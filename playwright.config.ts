// playwright.config.ts
import { defineConfig } from '@playwright/test';

export default defineConfig({
  // Run all tests in parallel.
  fullyParallel: process.env.CI ? false : true,
  // Glob patterns or regular expressions that match test files.
  testMatch: '**/*.e2e.ts',
  use: {
    headless: true,
    baseURL: process.env.URL || 'http://localhost:6006/',
    locale: 'fr-FR',
    timezoneId: 'Europe/Paris',
  },
  workers: process.env.CI ? 1 : '25%',
});
