const { readFile, readdir } = require('fs/promises');
const strip = require('strip-comments');

const { frCoreClassNames, frIconClassNames, riIconClassNames } = require('./classnames.js');
const { FALSE_POSITIVES } = require('./false-positives.js');

module.exports = { Analyzer };

/**
 * Parse et analyse les fichiers d'un répertoire.
 * Repère les classes 'fr-' qui sont utilisées dans le code mais qui n'existent pas (ou plus) côté DSFR.
 */
function Analyzer() {
  const dsfrClassNames = frCoreClassNames.concat(frIconClassNames).concat(riIconClassNames);

  /**
   * Analyse le répertoire
   * @param path
   * @return {Promise<void>}
   */
  this.analyzeDir = (path, verbose) => {
    return new Promise((resolve, reject) => {
      readdir(path, { withFileTypes: true }).then(
        (entries) => {
          const allPromises = [];
          entries.forEach((entry) => {
            const entryPath = path + '/' + entry.name;
            if (entry.isFile()) {
              const extension = getExtension(entryPath);
              if (extension === 'html' || entryPath.endsWith('component.ts') || entryPath.endsWith('stories.ts'))
                allPromises.push(analyzeFile(entryPath, verbose));
            } else if (entry.isDirectory()) {
              allPromises.push(this.analyzeDir(entryPath, verbose));
            }
          });
          Promise.all(allPromises).then(
            () => resolve(),
            (err) => reject(err),
          );
        },
        (err) => reject(err),
      );
    });
  };

  /**
   * Analyse un fichier
   * @param path
   * @return {Promise<unknown>}
   */
  function analyzeFile(path, verbose) {
    return new Promise((resolve, reject) => {
      readFile(path).then(
        (content) => {
          const classNames = analyzeContent(path, verbose, content);
          resolve(classNames);
        },
        (err) => reject(err),
      );
    });
  }

  /**
   * Analyse le contenu d'un fichier
   */
  function analyzeContent(path, verbose, content) {
    // remarque: strip supprime les commentaires des fichiers parsés
    const classNames = parseContent(strip(content.toString()));
    checkClassNames(path, verbose, classNames);
    return classNames;
  }

  /**
   * Parse le contenu HTML et isole le nom des classes du document dans classNames
   * @param content
   */
  function parseContent(content) {
    const classNames = new Set();
    const regExpr = /[ '"`](fr-)[^ '"`)]*/gs;
    const result = content.matchAll(regExpr);
    Array.from(result, (matchArr) => {
      const klass = matchArr[0].substring(1);
      if (/[\da-z]$/.test(klass)) {
        classNames.add(klass);
      }
    });
    return classNames;
  }

  /**
   * Vérifie que les classes font partie du DSFR
   */
  function checkClassNames(path, verbose, classNames) {
    let fileLogged = false;
    classNames.forEach((className) => {
      if (className?.startsWith('fr-')) {
        const index = dsfrClassNames.findIndex((e) => e === className);
        if (index === -1) {
          if (!fileLogged) {
            console.error(path);
            fileLogged = true;
          }
          if (FALSE_POSITIVES.includes(className)) {
            if (verbose) console.warn(` - warn  > anomalie connue : '${className}'`);
          } else {
            console.error(` - error > classe inconnue : '${className}'`);
          }
        }
      }
    });
  }

  // -- Tests -------------------------------------------------------------- */
  this.testAnalyzeFile = (path) => {
    return analyzeFile(path);
  };
  this.testParseContent = (content) => {
    return parseContent(content);
  };
}

function getExtension(fileName /*: string*/) {
  return fileName.split('.').pop();
}
