/* Public API Surface of ngx-dsfr */
export * from './lib/components';
export * from './lib/components/follow'; // Ici afin d'éviter une erreur de dépendances cycliques pendant la compilation
export * from './lib/forms';
export * from './lib/forms/form-email'; // Ici afin d'éviter une erreur de dépendances cycliques pendant la compilation
export * from './lib/forms/form-password'; // Ici afin d'éviter une erreur de dépendances cycliques pendant la compilation
export * from './lib/forms/form-select'; // Ici afin d'éviter une erreur de dépendances cycliques pendant la compilation
export * from './lib/pages';
export * from './lib/patterns';

// shared
export * from './lib/shared';

// Utilisé par les extensions (en plus des modèles)
export * from './lib/shared/components/default-control.component';
export * from './lib/shared/components/default-value-accessor.component';
export * from './lib/shared/components/input-group';

// utilitaires e2e (ne peut être exporté depuis utils/index, cela provoque des erreur de transpilation)
// export * from './lib/shared/utils/e2e.utils';
