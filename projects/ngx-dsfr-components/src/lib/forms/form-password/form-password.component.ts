import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { NavigationExtras } from '@angular/router';
import { DefaultControlComponent, DsfrLinkTarget, newUniqueId } from '../../shared';
import { DsfrFormPasswordValidationRule } from './form-password.model';

@Component({
  selector: 'dsfr-form-password',
  templateUrl: './form-password.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: DsfrFormPasswordComponent,
      multi: true,
    },
  ],
})
export class DsfrFormPasswordComponent extends DefaultControlComponent<string> implements OnInit {
  /** @since 1.6 'current-password' par défaut */
  @Input() autocomplete = 'current-password';

  /**
   * Cache le label visuellement en le laissant disponible aux lecteurs d'écran.
   */
  @Input() labelSrOnly = false;

  /**
   * Liste des règles de sécurisation du mot de passe, valides ou en erreur - Optionnel.
   * Sous la forme [{message: '', onError: false}]
   */
  @Input() validationRules: DsfrFormPasswordValidationRule[];

  /** Url de récupération du mot de passe (optionnel) */
  @Input() recoveryLink: string;

  /** Target du lien. Target par défaut de l'application si la propriété est non renseignée. */
  @Input() recoveryTargetLink: DsfrLinkTarget;

  /** Path angular géré en tant que directive routerLink. Exclusif avec link et route. Prioritaire sur route. */
  @Input() recoveryRouterLink: string | string[];

  /** RouterLink: classe utilisée pour la directive routerLink active. */
  @Input() recoveryRouterLinkActive: string | string[] | undefined;

  /** RouterLink: valeurs additionnelles de navigation pour le routerLink (queryParams, state etc.) */
  @Input() recoveryRouterLinkExtras: NavigationExtras;

  /** Path interne. Exclusif avec link et routerLink */
  @Input() recoveryRoute: string;

  /** Propage l'évènement Event du DOM à la sélection d'un lien. */
  @Output() recoveryRouteSelect = new EventEmitter<string>();

  /** @internal */ checkboxId: string;
  /** @internal */ messagesGroupId: string;

  /** @internal */
  constructor() {
    super();
  }

  /** @internal */
  ngOnInit() {
    super.ngOnInit();
    this.checkboxId = newUniqueId();
    this.messagesGroupId = newUniqueId();
  }

  /** @internal */
  getIdRule(rule: DsfrFormPasswordValidationRule, i: number): string {
    let status: string = 'info';
    if (rule.onError === false) {
      status = 'error';
    } else if (rule.onError === true) {
      status = 'error';
    }

    return `password-${this.inputId}-input-message-${status}-${i}`;
  }

  /** @internal */
  hasRecovery() {
    return this.recoveryLink || this.recoveryRoute || this.recoveryRouterLink;
  }

  /** @internal */
  onRecovery(route: string) {
    if (this.recoveryRoute) {
      this.recoveryRouteSelect.emit(this.recoveryRoute);
    }
  }

  /** @internal */
  hasMessages(): boolean {
    return this.validationRules && this.validationRules.length > 0;
  }
}
