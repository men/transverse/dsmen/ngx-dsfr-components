/**  Règle de sécurisation du mot de passe */
export interface DsfrFormPasswordValidationRule {
  /** Règle de validation */
  message: string;
  /** Statut de la validation
   * onError : true, validation en erreur (error)
   * onError: undefined, validation non effectuée (information)
   * onError: false : validation ok (valid)
   */
  onError?: boolean;
}
