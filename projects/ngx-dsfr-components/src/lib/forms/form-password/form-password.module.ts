import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrLinkModule } from '../../components';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrFormPasswordComponent } from './form-password.component';

@NgModule({
  declarations: [DsfrFormPasswordComponent],
  exports: [DsfrFormPasswordComponent],
  imports: [CommonModule, FormsModule, DsfrLinkModule, DsfrI18nPipe],
})
export class DsfrFormPasswordModule {}
