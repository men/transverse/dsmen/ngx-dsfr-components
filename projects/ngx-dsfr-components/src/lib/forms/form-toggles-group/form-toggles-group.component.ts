import {
  AfterViewInit,
  Component,
  ContentChildren,
  ElementRef,
  Input,
  OnDestroy,
  QueryList,
  ViewEncapsulation,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { DomUtils } from '../../shared';
import { BaseFieldsetComponent } from '../fieldset/base-fieldset.component';
import { DsfrFormToggleComponent } from '../form-toggle';

@Component({
  selector: 'dsfr-form-toggles-group',
  templateUrl: './form-toggles-group.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrFormTogglesGroupComponent extends BaseFieldsetComponent implements AfterViewInit, OnDestroy {
  /** @internal */
  @ContentChildren(DsfrFormToggleComponent) toggles: QueryList<DsfrFormToggleComponent>;

  private _showSeparators = true;

  private subscription = new Subscription();

  constructor(private _elementRef: ElementRef) {
    super();
  }

  get showSeparators(): boolean {
    return this._showSeparators;
  }

  /** Permet de masquer la bordure horizontale séparant les toggles dans le groupe. */
  @Input() set showSeparators(value: boolean) {
    this._showSeparators = value;
    this.updateTogglesBorderBottom();
  }

  ngAfterViewInit() {
    // Ajoute des tags <li> autour des toggles
    DomUtils.surroundChildWithli(this._elementRef, 'dsfr-form-toggle');

    // Gestion des bordures de séparation
    this.updateTogglesBorderBottom();

    const togglesChanges$ = this.toggles.changes.subscribe(() => {
      DomUtils.removeEmptyLi(this._elementRef);
      DomUtils.surroundChildWithli(this._elementRef, 'dsfr-form-toggle');

      this.updateTogglesBorderBottom();
    });

    this.subscription.add(togglesChanges$);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private updateTogglesBorderBottom() {
    if (this.toggles) {
      this.toggles.forEach((toggle) => (toggle.showSeparator = this.showSeparators));
    }
  }
}
