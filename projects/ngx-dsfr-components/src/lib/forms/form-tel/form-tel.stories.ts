import { argEventEmitter, dsfrDecorator } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrButtonModule } from '../../components';
import { InputGroupComponent } from '../../shared';
import { DsfrFormTelComponent } from './form-tel.component';

const meta: Meta = {
  title: 'FORMS/Phone',
  component: DsfrFormTelComponent,
  decorators: [moduleMetadata({ imports: [FormsModule, InputGroupComponent, DsfrButtonModule] })],
  argTypes: {
    buttonSelect: { control: argEventEmitter },
  },
};
export default meta;
type Story = StoryObj<DsfrFormTelComponent>;

export const Default: Story = {
  decorators: dsfrDecorator("Demande d'un numéro de téléphone"),
  args: {},
};

export const Error: Story = {
  decorators: dsfrDecorator("Demande d'un numéro de téléphone - Erreur"),
  args: {
    message: 'Le format de numéro de téléphone saisie n’est pas valide. Le format attendu est : (+33) 2 43 55 55 55',
    severity: 'error',
  },
};

export const WithNgModel: Story = {
  decorators: dsfrDecorator("Demande d'un numéro de téléphone - NgModel"),
  args: { value: '', label: 'Numéro de téléphone' },
  render: (args) => ({
    props: { ...args },
    template: `
    <dsfr-form-tel [label]="label" [(ngModel)]="value"></dsfr-form-tel>
    <br> <span>ngModel value : {{value}}</span>
    `,
  }),
};

export const Pattern: Story = {
  decorators: dsfrDecorator('Pattern de saisie'),
  args: {
    value: '0123456789',
    pattern: '^\\d{10}$',
    hint: 'Format attendu : une série de 10 chiffres sans séparateur',
  },
};
