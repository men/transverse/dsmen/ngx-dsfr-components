const REGEXP_PHONE_NUMBER = /^\(?(\+\d{2})?\)?\s?0?(\d(\s?\d{2}){4})$/;

/**
 * Teste si le numéro de téléphone correspond au pattern (soit celui de l'input, soit un pattern par défaut)
 */
export function isPhoneNumberValid(value: string | undefined, pattern?: string): boolean {
  if (!value) return false;

  const regexp = pattern ? new RegExp(pattern) : REGEXP_PHONE_NUMBER;
  const matchArr = regexp.exec(value);
  return matchArr ? true : false;
}

/**
 * Formatage du n° de téléphone.
 * Le formatage ne se fait que dans le cas où on utilise le pattern par défaut
 */
export function formatPhoneNumber(value: string | undefined, pattern?: string): string {
  if (!value) return '';
  if (pattern) return value;

  const regexp = REGEXP_PHONE_NUMBER;
  const matchArr = regexp.exec(value);
  if (!matchArr) return value;

  let num = value;
  let country = matchArr[1];
  num = matchArr[2].replace(/ /g, '');
  num = num.replace(/(\d)(\d{2})(\d{2})(\d{2})(\d{2})/, '$1 $2 $3 $4 $5');
  num = country ? `(${country}) ${num}` : `0${num}`;
  return num;
}
