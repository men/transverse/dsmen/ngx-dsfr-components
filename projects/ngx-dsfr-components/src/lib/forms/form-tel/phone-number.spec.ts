import { describe, test } from '@jest/globals';
import { formatPhoneNumber, isPhoneNumberValid } from './phone-number-utils';

describe('Phone Number test', () => {
  test('isPhoneNumberValid', () => {
    // Valide
    expect(isPhoneNumberValid('1 23 45 67 89')).toBe(true);
    expect(isPhoneNumberValid('123456789')).toBe(true);
    expect(isPhoneNumberValid('01 23 45 67 89')).toBe(true);
    expect(isPhoneNumberValid('0123456789')).toBe(true);
    expect(isPhoneNumberValid('+33 1 23 45 67 89')).toBe(true);
    expect(isPhoneNumberValid('+33123456789')).toBe(true);
    expect(isPhoneNumberValid('+33 01 23 45 67 89')).toBe(true);
    expect(isPhoneNumberValid('+330123456789')).toBe(true);
    expect(isPhoneNumberValid('(+33) 01 23 45 67 89')).toBe(true);
    expect(isPhoneNumberValid('(+33)0123456789')).toBe(true);

    // Non valdie
    expect(isPhoneNumberValid(undefined)).toBe(false);
    expect(isPhoneNumberValid('')).toBe(false);
    expect(isPhoneNumberValid('(+33)')).toBe(false);
    expect(isPhoneNumberValid('(+33) 1 22 33 44')).toBe(false);
    expect(isPhoneNumberValid('+33 01 22 33 44 55 66')).toBe(false);
    expect(isPhoneNumberValid('01 22 33 44 5')).toBe(false);
    expect(isPhoneNumberValid('11 22 33 44 55')).toBe(false);
  });

  test('Pattern', () => {
    expect(isPhoneNumberValid('0123456789', 'aa')).toBe(false);
    expect(formatPhoneNumber('0123456789', 'aa')).toBe('0123456789');
  });

  test('formatPhoneNumber', () => {
    // Valide
    expect(formatPhoneNumber('1 23 45 67 89')).toBe('01 23 45 67 89');
    expect(formatPhoneNumber('123456789')).toBe('01 23 45 67 89');
    expect(formatPhoneNumber('01 23 45 67 89')).toBe('01 23 45 67 89');
    expect(formatPhoneNumber('0123456789')).toBe('01 23 45 67 89');
    expect(formatPhoneNumber('+33 1 23 45 67 89')).toBe('(+33) 1 23 45 67 89');
    expect(formatPhoneNumber('+33123456789')).toBe('(+33) 1 23 45 67 89');
    expect(formatPhoneNumber('+33 01 23 45 67 89')).toBe('(+33) 1 23 45 67 89');
    expect(formatPhoneNumber('+330123456789')).toBe('(+33) 1 23 45 67 89');
    expect(formatPhoneNumber('(+33) 01 23 45 67 89')).toBe('(+33) 1 23 45 67 89');
    expect(formatPhoneNumber('(+33)0123456789')).toBe('(+33) 1 23 45 67 89');

    // Non valide (mais on formate quand même)
    expect(formatPhoneNumber(undefined)).toBe('');
    expect(formatPhoneNumber('')).toBe('');
    expect(formatPhoneNumber('(+33)')).toBe('(+33)');
    expect(formatPhoneNumber('(+33) 1 23 45 67')).toBe('(+33) 1 23 45 67');
    expect(formatPhoneNumber('+33 01 23 45 67 89 01')).toBe('+33 01 23 45 67 89 01');
    expect(formatPhoneNumber('11 23 45 67 89')).toBe('11 23 45 67 89');
  });
});
