import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DsfrFormTelComponent } from './form-tel.component';

describe('DsfrFormTelComponentTest', () => {
  let telComponent: DsfrFormTelComponent;
  let fixture: ComponentFixture<DsfrFormTelComponent>;

  function assertValueValidateWithSuccess(newValue: string, expextedValue: string) {
    telComponent.value = newValue;
    fixture.detectChanges();
    telComponent.onChange();
    expect(telComponent.error).toBe('');
    expect(telComponent.value).toBe(expextedValue);
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrFormTelComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DsfrFormTelComponent);
    telComponent = fixture.componentInstance;
    // Valeurs par défaut
    // Détection du changement
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(telComponent).not.toBeNull();
  });

  it('should be of type tel', () => {
    const inputElt = fixture.nativeElement.querySelector('.fr-input');
    expect(inputElt).not.toBeNull();
    const inpuType = inputElt.getAttribute('type');
    expect(inpuType).toEqual('tel');
  });

  it('should validate with empty error message', () => {
    expect(telComponent.error).toBeUndefined();
    assertValueValidateWithSuccess('1 23 45 67 89', '01 23 45 67 89');
    assertValueValidateWithSuccess('123456789', '01 23 45 67 89');
    assertValueValidateWithSuccess('0123456789', '01 23 45 67 89');
    assertValueValidateWithSuccess('+33 1 23 45 67 89', '(+33) 1 23 45 67 89');
    assertValueValidateWithSuccess('+33123456789', '(+33) 1 23 45 67 89');
    assertValueValidateWithSuccess('+33 01 23 45 67 89', '(+33) 1 23 45 67 89');
    assertValueValidateWithSuccess('+330123456789', '(+33) 1 23 45 67 89');
    assertValueValidateWithSuccess('(+33) 01 23 45 67 89', '(+33) 1 23 45 67 89');
    assertValueValidateWithSuccess('(+33)0123456789', '(+33) 1 23 45 67 89');
  });

  it('should validate with an error', () => {
    telComponent.value = '111AA11111';
    fixture.detectChanges();
    expect(telComponent.error).toBeUndefined();
    telComponent.onChange();
    expect(telComponent.error).toBe(telComponent.i18n.t('tel.error'));
  });

  it('should validate even if pattern defined', () => {
    telComponent.value = '1111111111';
    telComponent.pattern = '^\\d{10}$';
    fixture.detectChanges();
    expect(telComponent.error).toBeUndefined();
    telComponent.onChange();
    expect(telComponent.error).toBe('');
  });

  it('should put generic error message even if pattern defined', () => {
    telComponent.value = '111AA11111';
    telComponent.pattern = '^\\d{10}$';
    fixture.detectChanges();
    expect(telComponent.error).toBeUndefined();
    telComponent.onChange();
    expect(telComponent.error).toBe(telComponent.i18n.t('tel.error'));
  });
});
