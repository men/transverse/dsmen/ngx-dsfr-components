import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { DsfrButtonModule } from '../../../components';
import { DsfrI18nService } from '../../../shared';
import { DsfrFormTelComponent } from '../form-tel.component';

@Component({
  selector: 'demo-tel-reactive-form',
  templateUrl: './demo-tel-reactive-form.component.html',
  styles: [
    `
      dsfr-form-tel {
        display: block;
        margin-bottom: 1.25rem;

        .fr-input {
          max-width: 13rem;
        }
      }
    `,
  ],
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, DsfrFormTelComponent, DsfrButtonModule],
})
export class DemoTelReactiveFormComponent extends DsfrFormTelComponent {
  /** @internal */ formGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    i18n: DsfrI18nService,
  ) {
    super(i18n);
    this.formGroup = this.fb.group({
      phone: ['', Validators.required],
    });
  }

  /** @internal */
  onSubmit() {
    alert(`${this.label} : ${this.getControlValue('phone')}`);
  }

  private getFormControl(name: string): AbstractControl<any> {
    return this.formGroup.controls[name];
  }

  private getControlValue(name: string) {
    return this.getFormControl(name)?.value;
  }
}
