import { ReactiveFormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrButtonModule } from '../../../components';
import { DemoFormSelectComponent } from './demo-form-select.component';

const meta: Meta = {
  title: 'FORMS/Select',
  component: DemoFormSelectComponent,
  decorators: [moduleMetadata({ imports: [ReactiveFormsModule, DsfrButtonModule] })],
};
export default meta;
type Story = StoryObj<DemoFormSelectComponent>;

export const ReactiveForm: Story = {
  args: {},
};
