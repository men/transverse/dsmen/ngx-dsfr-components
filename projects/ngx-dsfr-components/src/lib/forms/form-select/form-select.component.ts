import {
  Component,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import {
  DefaultControlComponent,
  DsfrSelectOption,
  DsfrSeverity,
  DsfrSeverityConst,
  isStringEmptyOrNull,
} from '../../shared';

@Component({
  selector: 'dsfr-form-select',
  templateUrl: './form-select.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrFormSelectComponent),
      multi: true,
    },
  ],
})
export class DsfrFormSelectComponent extends DefaultControlComponent<any> implements OnInit, OnChanges {
  /**
   * Permet de personnaliser la première option (non sélectionnable). Cette propriété est positionnée à une valeur par
   * défaut (ex : Sélectionnez une option) internationalisée.
   */
  @Input() placeholder: string | undefined;

  /**
   * Indique que le champ est obligatoire, nécessaire du point de vue de l'accessibilité.
   */
  @Input() required = false;

  /**
   * Permet d'ajouter un attribut 'aria-label' sur le champ de formulaire en cas de besoin.
   */
  @Input() ariaLabel: string;

  /**
   * Message d'erreur, quand il est présent les couleurs du contrôle changent.
   *
   *  @deprecated (@since 1.13.0)
   */
  @Input() error: string;

  /**
   * Message de validation, quand il est présent les couleurs du contrôle changent.
   *
   *  @deprecated (@since 1.13.0)
   */
  @Input() valid: string;

  /**
   * Message d'information lié au composant
   */
  @Input() message: string | undefined = undefined;

  /**
   * Représente la sévérité du message.
   */
  @Input() messageSeverity: DsfrSeverity;

  /**
   * Cache le label visuellement en le laissant disponible aux lecteurs d'écran.
   */
  @Input() labelSrOnly = false;

  /**
   * Output équivalent de (ngModelChange) si celui n'est pas disponible.
   */
  @Output() selectChange = new EventEmitter<any>();

  /** @internal */ messagesGroupId: string;

  /**
   * @internal
   * https://github.com/angular/angular/issues/50233 : issue performance with ng-container/ng-template inside select for Firefox
   * Dans le cas ou il n'y a pas de groupe, supprimer le 'if' côté template
   * FIXME ticket toujours non résolu (v1.13)
   */
  noGroup: boolean = true;

  /** @internal fonction de comparaison */
  compareFn: (o1: any, o2: any) => boolean = Object.is;

  // Afin de pouvoir utiliser le type dans l'HTML
  /** @internal */
  protected readonly severityConst = DsfrSeverityConst;

  private _options: DsfrSelectOption[];

  /**
   * @deprecated since 1.2, utiliser `placeholder` à la place (sans H majuscule).
   */
  get placeHolder(): string | undefined {
    return this.placeholder;
  }

  get options(): DsfrSelectOption[] {
    return this._options;
  }

  /**
   * @deprecated since 1.2, utiliser `placeholder` à la place (sans H majuscule).
   */
  @Input() set placeHolder(value: string | undefined) {
    this.placeholder = value;
  }

  /**
   * Le modèle de présentation permettant de transmettre la liste des options.
   * fixme : remplacer par un signal input si https://github.com/compodoc/compodoc/pull/1491 fix
   */
  @Input() set options(value: DsfrSelectOption[]) {
    this._options = value;
    if (this._options && this._options.length && this._options.find((o) => o.options)) {
      this.noGroup = false;
    }
  }

  /** Personnalisation de la comparaison. CompareWith est une fonction qui a deux arguments: valeur option1 et valeur option2.
  Si compareWith est fournie, sélection de l'option en fonction du retour de la fonction. */
  @Input()
  set compareWith(fn: (o1: any, o2: any) => boolean) {
    if (typeof fn !== 'function') {
      throw Error('`compareWith` must be a function.');
    }

    this.compareFn = fn;
  }

  /** @internal */
  ngOnInit() {
    super.ngOnInit();
    this.messagesGroupId = `${this.inputId}-messages`;
  }

  ngOnChanges({ value }: SimpleChanges): void {
    if (value && this.value && this.options.length) {
      if (!this.findOption(this.value, this.options)) {
        this.value = undefined;
      }
    }
  }

  /** @internal */
  onChange() {
    // Comme on a le double binding, la valeur est déjà à jour
    this.selectChange.emit(this.value);
  }

  trackByIndex = (index: number): number => {
    return index;
  };

  /** @internal */
  protected hasMessage(severity: DsfrSeverity): boolean {
    return !isStringEmptyOrNull(this.message) && severity === this.messageSeverity;
  }

  private findOption(value: any, options: DsfrSelectOption[]): any {
    return options?.find(
      (option) =>
        this.compareFn(value, option.value) ||
        option.value === value ||
        (option.options && this.findOption(value, option.options)),
    );
  }
}
