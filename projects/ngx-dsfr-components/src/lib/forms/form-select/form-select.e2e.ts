import { expect, test } from '@playwright/test';
import { getSbFrame, goToFormsPage } from '../../shared/utils/e2e.utils';

test('select.default', async ({ page }) => {
  const frame = getSbFrame(page);
  const select = frame.locator('select');

  await goToFormsPage('select', page);
  await select.selectOption('Option 1');

  expect(await select.inputValue()).toBe('1: 1');
});

test('select.initial-value', async ({ page }) => {
  const frame = getSbFrame(page);
  const select = frame.locator('select');

  await goToFormsPage('select', page, 'initial-value');

  expect(await select.inputValue()).toBe('1: 2');
});
