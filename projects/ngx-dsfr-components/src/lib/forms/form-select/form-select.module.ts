import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrFormSelectComponent } from './form-select.component';

@NgModule({
  declarations: [DsfrFormSelectComponent],
  exports: [DsfrFormSelectComponent],
  imports: [CommonModule, FormsModule, DsfrI18nPipe],
})
export class DsfrFormSelectModule {}
