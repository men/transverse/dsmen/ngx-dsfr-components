import { dsfrDecorator } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFormCheckboxComponent } from './form-checkbox.component';

const meta: Meta = {
  title: 'FORMS/Checkbox',
  component: DsfrFormCheckboxComponent,
  decorators: [moduleMetadata({ imports: [FormsModule] })],
  argTypes: {
    value: { control: { type: 'boolean' } },
  },
};
export default meta;
type Story = StoryObj<DsfrFormCheckboxComponent>;

const label = 'Label checkbox';

const template = `
  <dsfr-form-checkbox [inputId]="'test'" label="Libellé case à cocher" ></dsfr-form-checkbox>
`;

/** Default */
export const Default: Story = {
  decorators: dsfrDecorator('Case à cocher seule'),
  args: {},
  render: (args) => ({
    props: args,
    template: template,
  }),
};

/** Hint */
export const Hint: Story = {
  decorators: dsfrDecorator('Case à cocher avec texte d‘aide'),
  args: {
    label: label,
    inputId: 'checkbox',
    hint: 'Texte de description additionnel',
  },
};

/** Hint */
export const LabelSrOnly: Story = {
  decorators: dsfrDecorator('Case à cocher avec label caché'),
  args: {
    label: label,
    inputId: 'checkbox',
    labelSrOnly: true,
  },
};

/** Valid */
export const Valid: Story = {
  decorators: dsfrDecorator('Case à cocher seule, validée'),
  args: {
    label: label,
    valid: 'Texte de validation',
  },
};

/** Error */
export const Error: Story = {
  decorators: dsfrDecorator('Case à cocher seule avec erreur'),
  args: {
    label: label,
    error: 'Texte d’erreur obligatoire',
  },
};

/** Checked */
export const Checked: Story = {
  decorators: dsfrDecorator('Case à cocher sélectionnée'),
  args: {
    label: label,
    value: true,
  },
};

/** Small */
export const Small: Story = {
  decorators: dsfrDecorator('Case à cocher, petite taille'),
  args: {
    label: label,
    small: true,
  },
};

/** Disabled */
export const Disabled: Story = {
  decorators: dsfrDecorator('Case à cocher désactivée'),
  args: {
    label: label,
    disabled: true,
  },
};

/** Indeterminate */
export const Indeterminate: Story = {
  decorators: dsfrDecorator('Case à cocher indéterminée'),
  args: {
    label: label,
    indeterminate: true,
  },
};

export const SlotLabel: Story = {
  decorators: dsfrDecorator('Case à cocher avec label en tant que slot'),
  args: {
    ...Default.args,
  },
  render: (args) => ({
    props: args,
    template: `
    <dsfr-form-checkbox [indeterminate]="indeterminate" [disabled]="disabled">
        <span label> <span aria-hidden="true" class="fr-icon-image-fill"></span> <span style="color: var(--info-425-625);font-weight: bold;"> Label </span> pour case à cocher</span>
    </dsfr-form-checkbox>`,
  }),
};

const myModel = { enabled: true };

export const WithNgModel: Story = {
  name: 'NgModel',
  decorators: dsfrDecorator('Case à cocher avec NgModel'),
  render: (args) => ({
    props: { ...args, myModel },
    template: `
         <dsfr-form-checkbox [(ngModel)]="myModel.enabled" label="Checkbox et son ngModel"></dsfr-form-checkbox>
         <span>myModel.enabled : {{ myModel.enabled}}</span>
       `,
  }),
};
