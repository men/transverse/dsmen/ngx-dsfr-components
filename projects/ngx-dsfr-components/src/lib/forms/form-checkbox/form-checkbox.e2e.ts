import { expect, test } from '@playwright/test';
import { getSbFrame, goToFormsPage } from '../../shared/utils/e2e.utils';

test('checkbox.default', async ({ page }) => {
  const frame = getSbFrame(page);
  const inputLocator = frame.locator('input[type=checkbox]');

  await goToFormsPage('checkbox', page);
  await expect(inputLocator).toBeVisible();
  expect(await inputLocator.getAttribute('checked')).toBeNull();
  await expect(inputLocator).toHaveAttribute('id');
});

test('checkbox.click', async ({ page }) => {
  const frame = getSbFrame(page);
  const input = frame.locator('dsfr-form-checkbox input[type=checkbox]');
  const label = frame.locator('dsfr-form-checkbox label');

  await goToFormsPage('checkbox', page);

  await expect(input).toBeVisible();
  await expect(input).not.toHaveAttribute('checked');
  await expect(input).not.toBeChecked();

  await label.click();

  await expect(input).not.toHaveAttribute('checked');
  await expect(input).toBeChecked();
});

test('checkbox.checked', async ({ page }) => {
  const frame = getSbFrame(page);
  const input = frame.locator('input[type=checkbox]');

  await goToFormsPage('checkbox', page, 'checked');

  await expect(input).toBeVisible();
  expect(await input.getAttribute('checked')).toBe('true');
});
