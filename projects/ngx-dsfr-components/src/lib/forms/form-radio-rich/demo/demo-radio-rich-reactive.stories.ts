import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrRadioRich } from '../form-radio-rich.model';
import { DsfrFormRadioRichModule } from '../form-radio-rich.module';
import { DemoRadioRichReactiveComponent } from './demo-radio-rich-reactive.component';

const meta: Meta = {
  title: 'FORMS/Radio Rich',
  component: DemoRadioRichReactiveComponent,
  decorators: [moduleMetadata({ imports: [DsfrFormRadioRichModule] })],
};
export default meta;
type Story = StoryObj<DemoRadioRichReactiveComponent>;

const legend = 'Légende pour l’ensemble des champs';
const labelRadio = 'Libellé bouton radio ';
const artworkFilePath = 'artwork/pictograms/buildings/city-hall.svg';
const options: DsfrRadioRich[] = [
  { label: labelRadio + 1, value: 1, artworkFilePath: artworkFilePath },
  { label: labelRadio + 2, value: 2, artworkFilePath: artworkFilePath },
  { label: labelRadio + 3, value: 3, artworkFilePath: artworkFilePath },
];

export const ReactiveForm: Story = {
  args: {
    legend: legend,
    options: options,
    value: 1,
  },
};
