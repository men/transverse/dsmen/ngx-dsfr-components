import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFormEmailModule } from '../form-email.module';
import { DemoEmailReactiveComponent } from './demo-email-reactive.component';

const meta: Meta = {
  title: 'FORMS/Email',
  component: DemoEmailReactiveComponent,
  decorators: [moduleMetadata({ imports: [DsfrFormEmailModule] })],
};
export default meta;
type Story = StoryObj<DemoEmailReactiveComponent>;

export const ReactiveForm: Story = {
  args: {},
};
