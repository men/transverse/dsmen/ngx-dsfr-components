import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrButtonModule } from '../../components';
import { InputGroupComponent } from '../../shared';
import { DsfrFormEmailComponent } from './form-email.component';

@NgModule({
  declarations: [DsfrFormEmailComponent],
  exports: [DsfrFormEmailComponent],
  imports: [CommonModule, FormsModule, InputGroupComponent, DsfrButtonModule],
})
export class DsfrFormEmailModule {}
