const PATTERN_EMAIL = '^[\\w\\d._%+-]+@[\\w\\d.-]+\\.[\\w]{2,4}$'; // https://www.abstractapi.com/guides/angular-email-validation + majuscules

export function isEmailValid(value: string | undefined, pattern?: string): boolean {
  if (!value) return false;

  const regexp = new RegExp(pattern ? pattern : PATTERN_EMAIL);
  return regexp.test(value);
}
