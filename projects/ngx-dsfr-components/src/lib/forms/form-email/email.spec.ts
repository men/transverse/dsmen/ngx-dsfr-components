import { describe, test } from '@jest/globals';
import { isEmailValid } from './email-utils';

describe('Email test', () => {
  test('isEmailValid', () => {
    expect(isEmailValid(undefined)).toBe(false);
    expect(isEmailValid('')).toBe(false);
    expect(isEmailValid('nom')).toBe(false);
    expect(isEmailValid('nom@domain.c')).toBe(false);
    expect(isEmailValid('nom@domain.com')).toBe(true);
    expect(isEmailValid('NOM@domain.com')).toBe(true);
    expect(isEmailValid('nom@domain.comix')).toBe(false);
  });
});
