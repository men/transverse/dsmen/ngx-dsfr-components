import {
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  OnInit,
  Output,
  Renderer2,
  SimpleChanges,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { DefaultControlComponent, DsfrI18nService, newUniqueId } from '../../shared';

@Component({
  selector: 'dsfr-form-upload, dsfr-upload',
  templateUrl: './upload.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrUploadComponent),
      multi: true,
    },
  ],
})
export class DsfrUploadComponent extends DefaultControlComponent<any> implements OnInit, OnChanges {
  /** @internal */
  @ViewChild('inputFile', { static: true })
  inputFile: ElementRef;

  /**
   * Cache le label visuellement en le laissant disponible aux lecteurs d'écran.
   */
  @Input() labelSrOnly = false;

  /** Texte d'erreur. */
  @Input() error: string;

  /** Permet l'ajout de plusieurs fichiers à la fois. */
  @Input() multiple = false;

  /** Spécifie un filtre pour les types de fichiers que l'utilisateur peut sélectionner (`@since 1.9.0`). */
  @Input() accept: string;

  /** Evénement émis à la sélection d'un ou plusieurs fichiers. */
  @Output()
  fileSelect = new EventEmitter<FileList>();

  /** Evénement émis lorsque l'utilisateur annule la sélection. */
  @Output()
  fileCancel = new EventEmitter<FileList>();

  /** @internal */
  messagesGroupId: string;

  constructor(
    private readonly _renderer: Renderer2,
    private readonly _elementRef: ElementRef,
    private readonly i18n: DsfrI18nService,
  ) {
    super();
  }

  /** @deprecated (@since 1.4.0) utiliser hint à la place. */
  get description(): string | undefined {
    return this.hint;
  }

  /**
   * Description pour l'upload (précise les contraintes au niveau du ou des fichiers attendus : format, poids attendus, nombre de fichiers possible…).
   *
   * @deprecated (@since 1.4.0) utiliser hint à la place.
   *
   * @internal
   */
  @Input() set description(value: string | undefined) {
    this.hint = value;
  }

  /**
   * Surcharge permettant de gérer la mise à jour du DOM.
   *
   * @internal
   */
  override writeValue(value: any | undefined): void {
    // afin de se prémunir des initialisations avec une chaîne de caractères (vide ou pas) il faut s'assurer du type
    // d'argument transmis en entrée et forcer la chaîne vide le cas échéant
    const isTypeFileList = Object.prototype.toString.call(value).includes(FileList.name);
    if (!isTypeFileList) {
      value = '';
    }
    super.writeValue(value);
    if (this.inputFile) {
      if (isTypeFileList && value?.length) {
        this._renderer.setProperty(this.inputFile.nativeElement, 'files', value);
      } else {
        this._renderer.setProperty(this.inputFile.nativeElement, 'value', '');
        this._renderer.setProperty(this.inputFile.nativeElement, 'files', null);
      }
    }
  }

  /** @internal */
  ngOnInit() {
    super.ngOnInit();
    this.name ??= 'file-upload'; // 'file-upload' est la valeur statique jusqu'en 1.3
    this.messagesGroupId = newUniqueId();
  }

  /** @internal */
  ngOnChanges(changes: SimpleChanges) {
    if ([changes['label']]) this.label ||= this.i18n.t('upload.label'); // default
    if ([changes['hint']]) this.hint ||= this.i18n.t('upload.hint'); // default
  }

  /**
   * Permet de programmatiquement réinitialiser le champ après une sélection de fichier(s).
   */
  reset() {
    this.inputFile.nativeElement.value = '';
  }

  /** @internal */
  onCancel() {
    this.fileCancel.emit();
  }

  /** @internal */
  onFileChange(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.value = target.files ?? undefined;
    if (this.value) this.fileSelect.emit(this.value);
  }

  /** @internal */
  hasMessages(): boolean {
    return !!this.error;
  }
}
