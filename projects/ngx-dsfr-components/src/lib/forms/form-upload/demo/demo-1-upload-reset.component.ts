
import { Component, Input, ViewChild } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { DsfrButtonModule } from '../../../components';
import { DsfrUploadComponent } from '../upload.component';
import { DsfrUploadModule } from '../upload.module';

@Component({
  selector: 'demo-reset-upload',
  template: `
    <dsfr-form-upload
      [label]="label"
      [hint]="hint"
      [error]="error"
      [multiple]="multiple"
      [accept]="accept"
      [disabled]="disabled"></dsfr-form-upload>

    <dsfr-button label="Reset" (click)="reset()"></dsfr-button>
  `,
  styles: [
    `
      dsfr-button {
        display: inline-block;
        margin-top: 30px;
      }
    `,
  ],
  standalone: true,
  imports: [ReactiveFormsModule, DsfrUploadModule, DsfrButtonModule],
})
export class DemoUploadResetComponent {
  @Input() label: string;
  @Input() hint: string | undefined;
  @Input() error: string;
  @Input() multiple: boolean;
  @Input() accept: string;
  @Input() disabled: boolean;
  @ViewChild(DsfrUploadComponent) uploadComponent: DsfrUploadComponent;

  constructor() {}

  /**
   * Perform reset action.
   */
  reset() {
    this.uploadComponent.reset();
  }
}
