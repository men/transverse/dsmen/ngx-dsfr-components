import { dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrButtonModule } from '../../../components';
import { DsfrUploadModule } from '../upload.module';
import { DemoUploadResetComponent } from './demo-1-upload-reset.component';

const meta: Meta = {
  title: 'FORMS/Upload',
  component: DemoUploadResetComponent,
  decorators: [moduleMetadata({ imports: [DsfrUploadModule, DsfrButtonModule] })],
};
export default meta;
type Story = StoryObj<DemoUploadResetComponent>;

export const Reset: Story = {
  decorators: dsfrDecorator(`Gestion d'un reset programmatique sur le champ`),
  args: {
    label: '', // label par défaut
    hint: 'Commencer par sélectionner un fichier puis actionner le bouton Reset pour réinitialiser le champ',
    error: '',
    multiple: false,
    accept: '',
    disabled: false,
  },
};
