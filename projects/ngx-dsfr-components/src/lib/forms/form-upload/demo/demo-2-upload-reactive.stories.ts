import { dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrButtonModule } from '../../../components';
import { DsfrUploadModule } from '../upload.module';
import { DemoUploadReactiveComponent } from './demo-2-upload-reactive.component';

const meta: Meta = {
  title: 'FORMS/Upload',
  component: DemoUploadReactiveComponent,
  decorators: [moduleMetadata({ imports: [DsfrUploadModule, DsfrButtonModule] })],
  argTypes: {
    withDefaultValue: {
      table: {
        disable: true,
      },
    },
    reactiveFormReset: {
      table: {
        disable: true,
      },
    },
    disabled: {
      table: {
        disable: true,
      },
    },
  },
};
export default meta;
type Story = StoryObj<DemoUploadReactiveComponent>;

export const ReactiveForm: Story = {
  decorators: dsfrDecorator('Bouton upload dans un ReactiveForm'),
  args: {
    label: '', // label par défaut
    hint: "Texte d'aide qui précise les contraintes au niveau du ou des fichiers attendus",
    error: '',
    multiple: false,
    accept: '',
    disabled: false,
  },
};

export const ReactiveFormInitValue: Story = {
  decorators: dsfrDecorator('Bouton upload dans un ReactiveForm pré-initialisé avec un fichier'),
  args: {
    label: '', // label par défaut
    hint: 'Fichiers de type txt',
    multiple: true,
    accept: '',
    withDefaultValue: true,
  },
};

export const ReactiveFormReset: Story = {
  decorators: dsfrDecorator(`Reset d'un composant upload piloté par un ReactiveForm`),
  args: {
    label: '', // label par défaut
    hint: 'Fichiers de type txt',
    multiple: true,
    accept: '',
    reactiveFormReset: true,
  },
};
