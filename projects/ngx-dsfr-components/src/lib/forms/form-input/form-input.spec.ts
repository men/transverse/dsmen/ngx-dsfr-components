import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { InputGroupComponent } from '../../shared';
import { DsfrFormInputComponent } from './form-input.component';

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-form-input [hint]="'texte description'" [(ngModel)]="val" [inputId]="'test'"></dsfr-form-input>`,
})
class TestHostComponent {
  @ViewChild(DsfrFormInputComponent)
  public formInputComponent: DsfrFormInputComponent;
  val: 'valeur';
}

describe('DsfrFormInputComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, InputGroupComponent],
      declarations: [DsfrFormInputComponent, TestHostComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should put id test ', () => {
    testHostComponent.formInputComponent.ngOnInit();
    fixture.detectChanges();

    const inputEl = fixture.nativeElement.querySelector('input');

    expect(inputEl.getAttribute('id')).toEqual('test');
    expect(inputEl.getAttribute('type')).toEqual('text');
    expect(inputEl.className).toContain('fr-input');
    expect(fixture.nativeElement.querySelector('.fr-hint-text').textContent).toEqual('texte description');
    expect(fixture.nativeElement.querySelector('fr-messages-group')).toBeDefined();
  });

  it('should show generate id and default aria attributes', () => {
    testHostComponent.formInputComponent.inputId = '';
    testHostComponent.formInputComponent.ngOnInit();

    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('input').getAttribute('id')).toBeDefined();
    expect(fixture.nativeElement.querySelector('input').getAttribute('aria-disabled')).toBeFalsy();
    expect(fixture.nativeElement.disabled).toBeFalsy();

    // Par défaut message group existante mais vide et aria-describedby non renseigné
    expect(fixture.nativeElement.querySelector('input').getAttribute('aria-describedby')).toBeFalsy();
    const messagesGroup = fixture.nativeElement.querySelector('.fr-messages-group');
    expect(messagesGroup.getAttribute('aria-live')).toEqual('polite');
    expect(messagesGroup.querySelector('p')).toBeFalsy();
  });

  it('should disabled component', () => {
    testHostComponent.formInputComponent.disabled = true;

    testHostComponent.formInputComponent.ngOnInit();
    fixture.detectChanges();

    const readonlyAttribute = fixture.nativeElement.querySelector('input').getAttribute('readonly');
    expect(fixture.nativeElement.querySelector('input').getAttribute('aria-disabled')).toBeTruthy();
    //expect(fixture.nativeElement.querySelector('input').getAttribute('disabled')).toBeTruthy();
    expect(fixture.nativeElement.querySelector('div').className).toContain('fr-input-group--disabled');
    expect(readonlyAttribute).toEqual(null);
  });

  it('should add required attribute', () => {
    testHostComponent.formInputComponent.required = true;
    testHostComponent.formInputComponent.type = 'textarea';

    testHostComponent.formInputComponent.ngOnInit();
    fixture.detectChanges();
    const requiredAttribute = fixture.nativeElement.querySelector('textarea').getAttribute('required');

    expect(requiredAttribute).toBeDefined();
    expect(requiredAttribute).toEqual('');
  });

  it('should add readonly attribute', () => {
    testHostComponent.formInputComponent.readonly = true;

    testHostComponent.formInputComponent.ngOnInit();
    fixture.detectChanges();
    const readonlyAttribute = fixture.nativeElement.querySelector('input').getAttribute('readonly');

    expect(readonlyAttribute).toBeDefined();
    expect(readonlyAttribute).toEqual('');
  });

  it('should display an error', () => {
    const message = 'Une erreur sur ce champ';
    testHostComponent.formInputComponent.error = message;

    testHostComponent.formInputComponent.ngOnInit();
    fixture.detectChanges();

    const messageId = fixture.nativeElement.querySelector('.fr-messages-group').id;
    expect(fixture.nativeElement.querySelector('input').getAttribute('aria-describedby')).toEqual(messageId);
    expect(fixture.nativeElement.querySelector('div').className).toContain('fr-input-group--error');
    expect(fixture.nativeElement.querySelector('.fr-message--error').textContent).toEqual(message);
  });

  it('should display a validation message', () => {
    const message = 'Text validation';
    testHostComponent.formInputComponent.valid = message;

    testHostComponent.formInputComponent.ngOnInit();
    fixture.detectChanges();

    const messageId = fixture.nativeElement.querySelector('.fr-messages-group').id;
    expect(fixture.nativeElement.querySelector('input').getAttribute('aria-describedby')).toEqual(messageId);
    expect(fixture.nativeElement.querySelector('div').className).toContain('fr-input-group--valid');
    expect(fixture.nativeElement.querySelector('.fr-message--valid').textContent).toEqual(message);
  });

  it('should add a custom class', () => {
    testHostComponent.formInputComponent.customClass = 'ma-classe';

    fixture.detectChanges();

    expect(fixture.nativeElement.querySelector('input').className).toContain('ma-classe');
  });

  it('should create an input type number with min and max', () => {
    testHostComponent.formInputComponent.type = 'number';
    testHostComponent.formInputComponent.min = '10';
    testHostComponent.formInputComponent.max = '100';

    testHostComponent.formInputComponent.ngOnInit();
    fixture.detectChanges();

    const inputEl = fixture.nativeElement.querySelector('input');
    expect(inputEl.getAttribute('type')).toEqual('number');
    expect(inputEl.getAttribute('min')).toEqual('10');
    expect(inputEl.getAttribute('max')).toEqual('100');
  });

  it('should have his label in SrOnly mode', () => {
    testHostComponent.formInputComponent.labelSrOnly = true;

    testHostComponent.formInputComponent.ngOnInit();
    fixture.detectChanges();

    const labelEl = fixture.nativeElement.querySelector('label');
    expect(labelEl.className).toContain('fr-sr-only');
  });
});
