import { argBoolean, argEventEmitter, buttonVariants, dsfrDecorator } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrButtonModule } from '../../components';
import { DsfrSeverityConst, InputGroupComponent } from '../../shared';
import { DsfrInputTypeConst } from './form-input.model';
import { DsfrFormInputComponent } from './index';

const meta: Meta = {
  title: 'FORMS/Input',
  component: DsfrFormInputComponent,
  decorators: [moduleMetadata({ imports: [FormsModule, DsfrButtonModule, InputGroupComponent] })],
  argTypes: {
    disabled: { control: argBoolean },
    inputWrapMode: { control: { type: 'inline-radio' } },
    required: { control: argBoolean },
    type: { control: { type: 'select' }, options: Object.values(DsfrInputTypeConst) },
    value: { control: { type: 'text' } },
    buttonVariant: { control: { type: 'inline-radio' }, options: buttonVariants },
    buttonSelect: { control: { control: argEventEmitter } },
    patternValueChange: { control: { control: argEventEmitter } },
    messageSeverity: { control: { type: 'inline-radio' }, options: ['info', 'error', 'success'] },
    severity: { table: { disable: true } },
  },
};
export default meta;
type Story = StoryObj<DsfrFormInputComponent>;

export const Default: Story = {
  decorators: dsfrDecorator("Input de type 'text'"),
  args: {
    label: 'Libellé champ de saisie',
    ariaExpanded: undefined,
    autoCorrect: true,
    disabled: false,
    required: false,
    inputWrapMode: 'action',
    buttonDisabled: false,
    buttonVariant: 'primary',
    type: DsfrInputTypeConst.TEXT,
    pattern: '',
  },
};

export const Number: Story = {
  decorators: dsfrDecorator("Input de type 'number'"),
  args: {
    ...Default.args,
    type: 'number',
    pattern: '[0-9]*',
  },
};

export const Search: Story = {
  decorators: dsfrDecorator("Input de type 'search'"),
  args: {
    ...Default.args,
    type: 'search',
  },
};

export const Time: Story = {
  decorators: dsfrDecorator("Input de type 'time'"),
  args: {
    ...Default.args,
    type: 'time',
  },
};

export const DateTimeLocal: Story = {
  decorators: dsfrDecorator("Input de type 'datetime-local'"),
  args: {
    ...Default.args,
    type: 'datetime-local',
  },
};

export const TextArea: Story = {
  decorators: dsfrDecorator("Input de type 'textarea'"),
  args: {
    ...Default.args,
    type: 'textarea',
  },
};

export const Password: Story = {
  decorators: dsfrDecorator(
    "Input de type 'password'",
    "Préférez l'utilisation du composant <a href='/?path=/story/forms-password--default'>Mot de passe</a>",
  ),
  args: {
    ...Default.args,
    type: 'password',
  },
};

export const Combo: Story = {
  decorators: dsfrDecorator('Combo champ + bouton'),
  args: {
    ...Default.args,
    buttonLabel: 'Envoyer',
    buttonIcon: 'fr-icon-arrow-down-s-line',
  },
};

export const Placeholder: Story = {
  decorators: dsfrDecorator('Champ avec placeholder'),
  args: {
    ...Default.args,
    placeholder: 'placeholder',
  },
};

export const InitialValue: Story = {
  decorators: dsfrDecorator('Champ avec valeur initiale'),
  args: {
    ...Default.args,
    value: 'value',
  },
};

export const CustomIcon: Story = {
  decorators: dsfrDecorator('Champ avec icône personnalisée'),
  args: {
    ...Default.args,
    icon: 'fr-icon-alert-line',
  },
};

export const Disabled: Story = {
  decorators: dsfrDecorator('Champ désactivé'),
  args: {
    ...Default.args,
    disabled: true,
  },
};

export const Hint: Story = {
  decorators: dsfrDecorator('Champ avec texte additionnel'),
  args: {
    ...Default.args,
    hint: 'Texte de description additionnel',
  },
};

export const Valid: Story = {
  decorators: dsfrDecorator('Champ valide avec texte de succès'),
  args: {
    ...Default.args,
    message: 'Texte de validation',
    messageSeverity: DsfrSeverityConst.SUCCESS,
  },
};

export const Error: Story = {
  decorators: dsfrDecorator("Champ en erreur avec texte d'erreur"),
  args: {
    ...Default.args,
    message: 'Texte d’erreur obligatoire',
    messageSeverity: DsfrSeverityConst.ERROR,
  },
};

export const Info: Story = {
  decorators: dsfrDecorator("Champ en erreur avec texte d'erreur"),
  args: {
    ...Default.args,
    message: "Message d'information",
    messageSeverity: DsfrSeverityConst.INFO,
  },
};

export const Warning: Story = {
  decorators: dsfrDecorator('Champ en warning avec texte en warning'),
  args: {
    ...Default.args,
    message: "Message d'avertissement",
    messageSeverity: DsfrSeverityConst.WARNING,
  },
};

export const SlotLabel: Story = {
  decorators: dsfrDecorator('Champ avec label en tant que slot'),
  args: {
    ...Default.args,
    message: "Message d'information",
    messageSeverity: DsfrSeverityConst.INFO,
  },
  render: (args) => ({
    props: args,
    template: `
    <dsfr-form-input>
        <span label><span aria-hidden="true" class="fr-icon-image-fill"></span> <span style="color: var(--info-425-625);font-weight: bold;"> Label </span> pour champ de saisie</span>
    </dsfr-form-input>`,
  }),
};
