
import { Component, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrButtonModule } from '../../../components';
import { DemoToolbarComponent } from '../../../shared/components/demo/demo-toolbar.component';
import { DsfrFormInputModule } from '../form-input.module';

@Component({
  selector: 'demo-input-pattern',
  templateUrl: './demo-input-pattern.component.html',
  standalone: true,
  imports: [FormsModule, DsfrFormInputModule, DsfrButtonModule, DemoToolbarComponent],
})
export class DemoInputPatternComponent {
  @Input() value: string;
  @Input() pattern: string;
  @Input() hint: string;

  /** @internal */
  valid = false;

  /** @internal */
  onInputPattern(valid: boolean) {
    this.valid = valid;
  }
}
