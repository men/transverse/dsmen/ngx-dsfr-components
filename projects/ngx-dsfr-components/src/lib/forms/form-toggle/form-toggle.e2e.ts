import { expect, test } from '@playwright/test';
import { getSbFrame, goToFormsPage } from '../../shared/utils/e2e.utils';

test('toggle.default', async ({ page }) => {
  const frame = getSbFrame(page);
  const input = frame.locator('input.fr-toggle__input');

  await goToFormsPage('toggle', page);
  await expect(input).toHaveAttribute('id');
});
