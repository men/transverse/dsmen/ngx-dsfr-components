import { Component, ContentChildren, Input, QueryList, ViewEncapsulation } from '@angular/core';
import { BaseFieldsetComponent } from './base-fieldset.component';
import { DsfrFormFieldsetElementDirective } from './form-fieldset-element.directive';

@Component({
  selector: 'dsfr-fieldset, dsfr-form-fieldset',
  templateUrl: './form-fieldset.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrFormFieldsetComponent extends BaseFieldsetComponent {
  /**
   * Ensemble des champs de formulaire sur une même ligne.
   */
  @Input() inline = false;

  /**
   * Les champs composant le formulaire.
   * @internal
   */
  @ContentChildren(DsfrFormFieldsetElementDirective) elements!: QueryList<DsfrFormFieldsetElementDirective>;

  /**
   * Retourne les erreurs sous forme de tableau même si on a juste un string
   * @return type compatible avec la propriété error, c.-à-d. undefined si non renseigné
   * @internal
   */
  get errors(): string[] | undefined {
    return this.error ? (Array.isArray(this.error) ? this.error : [this.error]) : undefined;
  }

  /** @internal */
  getFieldsetElementClass(elem: DsfrFormFieldsetElementDirective): string[] {
    const classes = ['fr-fieldset__element'];
    if (this.inline) {
      classes.push('fr-fieldset__element--inline');
    }
    if (elem.fieldsetElement) {
      const elemClasses: string[] = elem.fieldsetElement.split(/\s+/g);
      elemClasses.forEach((c) => classes.push(c));
    }

    return classes;
  }
}
