import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { InputGroupComponent } from '../../shared';
import { DsfrFormFieldsetComponent } from './form-fieldset.component';

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-form-fieldset
    [id]="id"
    [legend]="legend"
    [legendRegular]="legendRegular"
    [legendSrOnly]="legendSrOnly"
    [hint]="hint"
    [inline]="inline"
    [disabled]="disabled"
    [error]="error"
    [valid]="valid">
    <dsfr-form-input *fieldsetElement label="Libellé champ de saisie"></dsfr-form-input>
    <dsfr-form-input *fieldsetElement label="Libellé champ de saisie"></dsfr-form-input>
  </dsfr-form-fieldset>`,
})
class TestHostComponent {
  @ViewChild(DsfrFormFieldsetComponent)
  public formFieldsetComponent: DsfrFormFieldsetComponent;
  val: 'valeur';
}

describe('DsfrFormInputComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, InputGroupComponent],
      declarations: [DsfrFormFieldsetComponent, TestHostComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should have aria-labelledby', () => {
    testHostComponent.formFieldsetComponent.id = 'test';
    testHostComponent.formFieldsetComponent.ngOnInit();

    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('fieldset').getAttribute('aria-labelledby')).toEqual('test-legend');
  });

  it('should display a validation message', () => {
    const message = 'Text validation';
    testHostComponent.formFieldsetComponent.valid = message;
    testHostComponent.formFieldsetComponent.id = 'test';

    testHostComponent.formFieldsetComponent.ngOnInit();
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('fieldset').getAttribute('aria-labelledby')).toEqual(
      'test-legend test-messages',
    );
    expect(fixture.nativeElement.querySelector('.fr-message--valid').textContent).toEqual(message);
  });
});
