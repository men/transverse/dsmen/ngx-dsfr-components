/** Boutons radios simples */
export interface DsfrRadio {
  /** Valeur */
  value: any;
  /** Label  */
  label: string;
  /** Texte d'aide optionnel */
  hint?: string;
  /** Désactive l'input */
  disabled?: boolean;
}
