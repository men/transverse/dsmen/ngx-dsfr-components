import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrRadio } from '../form-radio.model';
import { DsfrFormRadioModule } from '../form-radio.module';
import { DemoRadioReactiveComponent } from './demo-radio-reactive.component';

const meta: Meta = {
  title: 'FORMS/Radio',
  component: DemoRadioReactiveComponent,
  decorators: [moduleMetadata({ imports: [DsfrFormRadioModule] })],
};
export default meta;
type Story = StoryObj<DemoRadioReactiveComponent>;

const legend = 'Légende pour l’ensemble des champs';
const options: DsfrRadio[] = [
  { label: 'Pissenlit', value: 'pissenlit' },
  { label: 'Marguerite', value: 'marguerite' },
  { label: 'Edelweiss', value: 'edelweiss' },
];

export const ReactiveForm: Story = {
  args: {
    legend: legend,
    options: options,
    value: 'marguerite',
  },
};

export const ReactiveFormDisabled: Story = {
  args: {
    legend: legend,
    options: options,
    value: 'marguerite',
    disabled: true,
  },
};
