import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { DsfrFormFieldsetComponent, DsfrFormFieldsetElementDirective, DsfrFormFieldsetModule } from '../fieldset';
import { DsfrFormRadioComponent } from './form-radio.component';

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-form-radio
    [hint]="'texte description'"
    [options]="[
      { label: 'label 1', value: '1' },
      { label: 'label 2', value: '2' },
      { label: 'label 3', value: '3' }
    ]"
    [(ngModel)]="val"
    [inputId]="'test'"></dsfr-form-radio>`,
})
class TestHostComponent {
  @ViewChild(DsfrFormRadioComponent)
  public formRadioComponent: DsfrFormRadioComponent;
  val: 'valeur';
}

describe('DsfrFormInputComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, DsfrFormFieldsetModule],
      declarations: [
        DsfrFormRadioComponent,
        DsfrFormFieldsetComponent,
        DsfrFormFieldsetElementDirective,
        TestHostComponent,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should disabled component one item', async () => {
    testHostComponent.formRadioComponent.options = [
      { label: 'label 1', value: '1' },
      { label: 'label 2', value: '2', disabled: true },
      { label: 'label 3', value: '3' },
    ];
    testHostComponent.formRadioComponent.ngOnInit();
    fixture.detectChanges();

    // await necessaire dans le cas de la propriété disabled (cycle angular supplémentaire ?)
    await fixture.whenStable();

    const firstRadio = fixture.nativeElement.querySelector('.fr-fieldset__element:nth-child(2)');
    const secondRadio = fixture.nativeElement.querySelector('.fr-fieldset__element:nth-child(3)');

    expect(secondRadio.querySelector('label').textContent).toEqual('label 2');
    expect(secondRadio.querySelector('input').hasAttribute('disabled')).toBe(true);
    expect(firstRadio.querySelector('input').hasAttribute('disabled')).toBe(false);
  });
});
