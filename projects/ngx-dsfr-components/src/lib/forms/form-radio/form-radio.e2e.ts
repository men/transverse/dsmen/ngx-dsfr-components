import { expect, test } from '@playwright/test';
import { getSbFrame, goToFormsPage } from '../../shared/utils/e2e.utils';

/** Inline */
test('radio.inline', async ({ page }) => {
  const frame = getSbFrame(page);
  const fieldsetElt = frame.locator('div.fr-fieldset__element').first();

  await goToFormsPage('radio', page, 'in-line');

  await expect(fieldsetElt).toBeVisible();
  await expect(fieldsetElt).toHaveClass('fr-fieldset__element fr-fieldset__element--inline');
});
