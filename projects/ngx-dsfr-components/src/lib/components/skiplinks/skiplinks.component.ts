import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { DsfrAnchorLink } from '../../shared';

@Component({
  selector: 'dsfr-skiplinks',
  templateUrl: './skiplinks.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrSkipLinksComponent {
  /**
   * Tableau des liens.
   */
  @Input() links: DsfrAnchorLink[];

  /**
   * Permet la gestion programmatique d'une navigation initiée au click sur le lien si l'input 'route' est valorisé.
   * La valeur de la propriété 'route' sera transmise.
   */
  @Output() linkSelect = new EventEmitter<string>();

  /**
   * Dans le cas d'une route, un événement `(routeSelect)` est émis avec la valeur de la route et
   * l'événement initial n'est pas propagé.
   *
   * @internal
   */
  onSkiplinkSelect(event: Event, route: string | undefined) {
    if (route) {
      event.preventDefault();
      this.linkSelect.emit(route);
    }
  }
}
