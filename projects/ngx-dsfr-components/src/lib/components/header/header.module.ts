import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DsfrDisableRouterLinkDirective } from '../../shared/directives/disable-router-link.directive';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrDisplayModule } from '../display';
import { DsfrLinkModule } from '../link';
import { ItemLinkComponent } from '../link/item-link.component';
import { DsfrSearchBarModule } from '../search-bar';
import { DsfrTranslateModule } from '../translate';
import { DsfrHeaderComponent } from './header.component';
import { DsfrMegaMenuComponent } from './mega-menu/mega-menu.component';

@NgModule({
  declarations: [DsfrHeaderComponent],
  exports: [DsfrHeaderComponent],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    DsfrMegaMenuComponent,
    DsfrSearchBarModule,
    ItemLinkComponent,
    DsfrTranslateModule,
    DsfrDisplayModule,
    DsfrLinkModule,
    DsfrDisableRouterLinkDirective,
    DsfrI18nPipe,
  ],
})
export class DsfrHeaderModule {}
