import { dsfrDecorator } from '.storybook/storybook-utils';
import { RouterTestingModule } from '@angular/router/testing';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrModalModule } from '../../modal';
import { DemoHeaderFocusComponent } from './demo-header-focus.component';

const meta: Meta = {
  title: 'COMPONENTS/Header',
  component: DemoHeaderFocusComponent,
  parameters: {
    docs: {
      inline: false,
    },
  },
  decorators: [moduleMetadata({ imports: [DsfrModalModule, RouterTestingModule] })],
};
export default meta;
type Story = StoryObj<DemoHeaderFocusComponent>;

export const CustomHeader: Story = {
  decorators: dsfrDecorator(
    'Custom Header',
    "Accessibilité : Tabuler pour accéder aux liens. Chacun ouvre une modale.  <br/> Redéfinition des liens après l'initialisation.  <br/>  Point d'injection : ajout slot headerTools",
  ),
  args: {
    serviceTitle: 'Nom du service',
    serviceTagline: "Description de l'organisation",
    headerToolsLinks: [
      {
        label: 'Un bouton',
        icon: 'fr-icon-account-line',
        ariaControls: 'accountModal',
        mode: 'button',
      },
      {
        label: 'Un lien',
        icon: 'fr-icon-logout-box-r-line',
        route: 'logout',
        ariaControls: 'logoutModal',
        ariaLabel: 'Se déconnecter de Scolarité services',
      },
    ],
  },
};
