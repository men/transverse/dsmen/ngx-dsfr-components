import { DsfrLink } from '../../../shared';

/** Catégorie d'un mega-menu */
export interface DsfrMegaMenuCategory {
  /* Label de la catégorie (optionnel) */
  label?: string;
  /* Entrées de la catégorie (liens directs). Eviter de dépasser 8 liens. */
  subItems: DsfrLink[];
  /* Lien de la catégorie (optionnel) */
  link?: string;
}

/** Leader d'un méga-menu (éléments de contexte) */
export interface DsfrMegaMenuLeader {
  /* Titre de la rubrique (optionnel) */
  title?: string;
  /* Texte de présentation (optionnel) */
  text?: string;
  /* Lien vers l'accueil de la rubrique (optionnel) */
  link?: DsfrLink;
}

/** Méga-menu : menu de navigation complexe */
export interface DsfrMegaMenu {
  /* Leader (contexte) du méga menu (optionnel) */
  leader?: DsfrMegaMenuLeader;
  /* Catégories */
  categories: DsfrMegaMenuCategory[];
}
