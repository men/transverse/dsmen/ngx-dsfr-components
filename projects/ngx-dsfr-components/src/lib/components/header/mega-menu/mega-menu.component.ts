import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DsfrLink } from '../../../shared';
import { DsfrI18nPipe } from '../../../shared/i18n/i18n.pipe';
import { DsfrLinkModule } from '../../link';
import { ItemLinkComponent } from '../../link/item-link.component';
import { DsfrMegaMenu } from './mega-menu.model';

@Component({
  selector: 'dsfr-mega-menu',
  templateUrl: './mega-menu.component.html',
  styleUrls: ['./mega-menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
  imports: [FormsModule, ItemLinkComponent, DsfrLinkModule, RouterModule, DsfrI18nPipe],
  standalone: true,
})
export class DsfrMegaMenuComponent {
  @Input() megaMenu: DsfrMegaMenu;
  @Input() idMenu: number;

  /**Emet l'évènement Event du DOM à la sélection d'un lien */
  @Output() linkSelect = new EventEmitter<DsfrLink>();

  /**Emet l'évènement Event du DOM à la fermeture du méga menu */
  @Output() closeSelect = new EventEmitter<Event>();

  onLink(event: DsfrLink) {
    this.linkSelect.emit(event);
  }

  onClose() {
    this.closeSelect.emit();
  }
}
