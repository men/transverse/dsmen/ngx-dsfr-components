import { argEventEmitter } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { componentWrapperDecorator, Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrLink } from '../../shared';
import { DsfrDisableRouterLinkDirective } from '../../shared/directives/disable-router-link.directive';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DISPLAY_MODAL_ID, DsfrDisplayModule } from '../display';
import { DsfrLinkModule } from '../link';
import { ItemLinkComponent } from '../link/item-link.component';
import { DsfrSearchBarModule } from '../search-bar';
import { DsfrTranslateModule } from '../translate';
import { DsfrHeaderComponent } from './header.component';
import { DsfrHeaderMenuItem } from './header.model';
import { DsfrMegaMenuComponent } from './mega-menu/mega-menu.component';

const meta: Meta = {
  title: 'COMPONENTS/Header',
  component: DsfrHeaderComponent,
  decorators: [
    moduleMetadata({
      imports: [
        DsfrDisableRouterLinkDirective,
        FormsModule,
        DsfrMegaMenuComponent,
        DsfrTranslateModule,
        DsfrSearchBarModule,
        RouterTestingModule,
        ItemLinkComponent,
        DsfrDisplayModule,
        DsfrLinkModule,
        DsfrI18nPipe,
      ],
    }),
  ],
  argTypes: {
    searchChange: { control: argEventEmitter },
    searchSelect: { control: argEventEmitter },
    linkSelect: { control: argEventEmitter },
    langChange: { control: argEventEmitter },
  },
};
export default meta;

type Story = StoryObj<DsfrHeaderComponent>;

const toolsLinks: DsfrLink[] = [
  { label: 'Contact', icon: 'fr-btn--team', link: '#[url - à modifier]' },
  { label: 'Espace recruteur', icon: 'fr-btn--briefcase', link: '#[url - à modifier]' },
  { label: 'Espace particulier', icon: 'fr-btn--account', routerLink: '/', routerLinkActive: 'class-active' },
  { label: 'Un lien de trop', route: '#' },
];

const toolsLinksDisplay: DsfrLink[] = [
  {
    mode: 'button',
    label: 'Créer un espace',
    icon: 'fr-icon-add-circle-line',
    ariaControls: DISPLAY_MODAL_ID,
  },
  {
    mode: 'button',
    label: 'Thème',
    customClass: 'fr-icon-theme-fill',
    ariaControls: DISPLAY_MODAL_ID,
  },
  { label: 'Se connecter', route: '#', icon: 'fr-icon-lock-line' },
];

const menuDeroulantHeader: DsfrHeaderMenuItem[] = [
  { label: 'Accès rapide', link: '.' },
  {
    label: 'Menu déroulant',
    subItems: [{ label: 'Accès routerLink', routerLink: '/' }, { label: 'Accès direct 3' }],
  },
  { label: 'Accès rapide actif', link: '#', active: true },
  { label: 'Menu déroulant 2', link: '#', subItems: [{ label: 'Accès routerLink' }, { label: 'Accès direct 3' }] },
  {
    label: 'Menu déroulant 3',
    link: '#',
    subItems: [{ label: 'Accès routerLink', target: '_self' }, { label: 'Accès direct 3' }],
  },
];

const megaMenuHeader: DsfrHeaderMenuItem[] = [
  { label: 'Accès direct', link: '#' },
  {
    label: 'Méga menu (actif)',
    active: true,
    megaMenu: {
      leader: {
        title: 'Titre éditorialisé',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua.',
        link: { label: 'Voir toute la rubrique', routerLink: '/' },
      },
      categories: [
        {
          label: 'Catégorie 1',
          link: '#',
          subItems: [
            { label: 'Lien 1', route: '#' },
            { label: 'Lien 3', route: '#' },
          ],
        },
        {
          label: 'Catégorie 2',
          subItems: [
            {
              label: 'Lien 1',
              route: '#',
              routerLinkActive: 'class-active',
              routerLinkActiveOptions: { exact: false },
            },
            {
              label: 'Lien actif',
              route: '#',
              active: true,
            },
            { label: 'Lien désactivé' },
          ],
        },
      ],
    },
  },
  {
    label: 'Méga menu 2',
    megaMenu: {
      categories: [
        {
          label: 'Catégorie 1',
          subItems: [{ label: 'Lien 1' }, { label: 'Lien 3' }],
        },
        {
          label: 'Catégorie 2',
          subItems: [
            { label: 'Lien 1' },
            { label: 'Lien avec router link', routerLink: '/link' },
            { label: 'Lien 3' },
            { label: 'Lien 4' },
          ],
        },
        {
          label: 'Catégorie 3',
          subItems: [{ label: 'Lien 1' }, { label: 'Lien 2', routerLink: '/dezfz' }, { label: 'Lien 3' }],
        },
      ],
    },
  },
  { label: 'Accès direct 3', link: '#' },
];

const DEFAULT_LOGO_TOOLTIP = 'Informations complémentaires sur le site ou service';

export const Default: Story = {
  args: {
    logoLabel: 'République<br/>Française',
    serviceTitle: '',
    serviceTagline: '',
    headerToolsLinks: [],
    searchBar: false,
    menu: [],
    translate: undefined,
    beta: false,
    display: false,
    artworkDirPath: '',
    operatorImagePath: '',
    operatorImageAlt: '',
    operatorImageVertical: false,
    logoLink: {
      routerLink: '/',
    },
  },
};

export const WithService: Story = {
  args: {
    ...Default.args,
    serviceTitle: 'Nom du site / service',
    serviceTagline: "tagline - précisions sur l'organisation",
  },
};

export const QuickAccess: Story = {
  args: {
    ...WithService.args,
    headerToolsLinks: toolsLinks,
  },
};

export const WithSearch: Story = {
  args: {
    ...QuickAccess.args,
    searchBar: true,
  },
};

export const WithTranslate: Story = {
  args: {
    translate: {
      languages: [
        { label: 'Français', value: 'fr' },
        { label: 'English', value: 'en' },
      ],
    },
  },
};

export const WithLogo: Story = {
  args: {
    ...Default.args,
    operatorImagePath: 'img/placeholder.16x9.png',
    logoLabel: 'Logo placeholder',
    logoLink: {
      routerLink: '/',
    },
  },
};

export const VerticalLogo: Story = {
  args: {
    ...WithLogo.args,
    operatorImageVertical: true,
    logoLabel: 'Logo placeholder',
    logoLink: '/',
    logoTooltipMessage: 'Logo placeholder',
  },
};

export const BadgeBeta: Story = {
  args: {
    ...WithSearch.args,
    beta: true,
  },
};

export const Full: Story = {
  args: {
    ...WithSearch.args,
    operatorImagePath: 'img/placeholder.16x9.png',
    menu: [
      { label: 'Accès direct', route: '#', active: true },
      { label: 'Accès direct 2', route: '#', target: '_blank' },
      { label: 'Accès direct 3', route: '#' },
      {
        label: 'Accès routerLink',
        routerLink: '/',
        routerLinkActive: 'class-active',
      },
    ],
  },
};

export const DropdownMenu: Story = {
  args: {
    ...WithService.args,
    logoTooltipMessage: DEFAULT_LOGO_TOOLTIP,
    operatorImagePath: 'img/placeholder.16x9.png',
    menu: menuDeroulantHeader,
  },
};

export const MegaMenu: Story = {
  args: {
    ...WithSearch.args,
    logoTooltipMessage: DEFAULT_LOGO_TOOLTIP,
    operatorImagePath: 'img/placeholder.16x9.png',
    menu: megaMenuHeader,
  },
};

export const Display: Story = {
  args: {
    ...Default.args,
    display: true,
    pictoPath: 'artwork/pictograms/environment/',
  },
  decorators: [
    componentWrapperDecorator((story) => `<div class="sb-title">Lien d'accès aux paramètres d'affichage</div>${story}`),
  ],
  render: (args) => ({
    props: args,
    template: `<dsfr-header [display]="display" [pictoPath]="pictoPath"></dsfr-header>`,
  }),
};

export const DisplayArtworkDirPath: Story = {
  name: 'Display (artworkDirPath)',
  args: {
    ...Default.args,
    display: true,
    artworkDirPath: 'artwork',
  },
  decorators: [
    componentWrapperDecorator(
      (story) => `<div class="sb-title">Lien d'accès aux paramètres d'affichage (custom artworkDirPath)</div>${story}`,
    ),
  ],
};

export const CustomDisplay: Story = {
  args: {
    ...Default.args,
    headerToolsLinks: toolsLinksDisplay,
    display: false,
  },
  decorators: [
    componentWrapperDecorator(
      (story) => `<div class="sb-title">Custom Display</div>${story} <dsfr-display></dsfr-display>`,
    ),
  ],
};
