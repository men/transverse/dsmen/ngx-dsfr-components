import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { DsfrLink } from '../../shared';
import { DsfrHeaderComponent } from './header.component';
import { DsfrHeaderMenuItem } from './header.model';
import { DsfrHeaderModule } from './header.module';
require('../../../../../../node_modules/@gouvfr/dsfr/dist/dsfr.module.js');

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-header [headerToolsLinks]="toolsLinks" [menu]="menu"></dsfr-header>`,
})
class TestHostComponent {
  @ViewChild(DsfrHeaderComponent)
  public headerComponent: DsfrHeaderComponent;

  toolsLinks: DsfrLink[] = [
    { label: 'Contact', link: '#' },
    { label: 'Espace recruteur', link: '#' },
    { label: 'Espace particulier', routerLink: '/' },
  ];

  menu: DsfrHeaderMenuItem[] = [
    { label: 'Home', routerLink: '/header/home' },
    {
      label: 'Méga menu 1',
      active: true,
      megaMenu: {
        leader: {
          title: 'Titre éditorialisé',
          text: 'Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua.',
          link: { label: 'Voir toute la rubrique' },
        },
        categories: [
          {
            label: 'Catégorie 1',
            subItems: [
              { label: 'Lien 1', link: '/header?page=mm1c1l1' },
              { label: 'Lien 3', routerLink: '#' },
            ],
          },
          {
            label: 'Catégorie 2',
            subItems: [
              { label: 'Lien 1', routerLink: '#' },
              {
                label: 'Lien actif',
                routerLink: 'accordion',
                active: true,
                routerLinkActive: 'class-active',
              },
              { label: 'Lien 3', routerLink: '#' },
            ],
          },
        ],
      },
    },
    {
      label: 'Dropdown Menu',
      routerLink: '/header',
      subItems: [
        {
          label: 'Lien 1',
          routerLink: '/header',
          routerLinkActiveOptions: { exact: false },
        },
        { label: 'Lien 2', routerLink: '#' },
        { label: 'Lien 3', routerLink: '#' },
      ],
    },
    { label: 'Contact', routerLink: '/header/contact' },
  ];
}

describe('DsfrHeaderComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, DsfrHeaderModule, RouterModule.forRoot([])],
      declarations: [TestHostComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    //@ts-ignore
    dsfr.start();
    fixture.detectChanges();
  });

  it('should init header with menu ', () => {
    testHostComponent.headerComponent.ngOnInit();
    fixture.detectChanges();

    const nav = fixture.nativeElement.querySelector('.fr-nav__list');

    // EXPECT
    expect(nav.querySelectorAll('.fr-nav__item')).toHaveLength(4); // niveau 1
    expect(nav.querySelector('.fr-menu__list').querySelectorAll('li')).toHaveLength(3); // méga menu
    expect(nav.querySelector('.fr-mega-menu__list').querySelectorAll('li')).toHaveLength(2); // menu déroulant
  });

  it('should add aria-current on button menu with active ', () => {
    testHostComponent.headerComponent.ngOnInit();
    fixture.detectChanges();

    const nav = fixture.nativeElement.querySelector('.fr-nav__list');
    const dropdownMenuButton = nav.querySelector('button[aria-controls="menu-2"]');
    const megaMenuButton = nav.querySelector('button[aria-controls="mega-menu-1"]');

    // EXPECT
    expect(dropdownMenuButton.getAttribute('aria-current')).toBeNull();
    expect(megaMenuButton.getAttribute('aria-current')).toEqual('true');
  });

  it('should open dropdown menu with button ', fakeAsync(() => {
    testHostComponent.headerComponent.ngOnInit();
    fixture.detectChanges();

    const nav = fixture.nativeElement.querySelector('.fr-nav__list');
    const dropdownMenuButton = nav.querySelector('button[aria-controls="menu-2"]');
    const containerDropdown = nav.querySelector('.fr-menu');

    // EXPECT
    expect(dropdownMenuButton.getAttribute('aria-expanded')).toEqual('false');
    expect(dropdownMenuButton.textContent).toContain('Dropdown Menu');

    // WHEN
    dropdownMenuButton.click();
    tick();
    fixture.detectChanges();

    // EXPECT
    expect(dropdownMenuButton.getAttribute('aria-expanded')).toEqual('true');
    expect(containerDropdown.classList).toContain('fr-collapsing');
    expect(containerDropdown.getAttribute('id')).toEqual('menu-2');
  }));

  it('should open mega menu with button ', fakeAsync(() => {
    testHostComponent.headerComponent.ngOnInit();
    fixture.detectChanges();

    const nav = fixture.nativeElement.querySelector('.fr-nav__list');
    const megaMenuButton = nav.querySelector('button[aria-controls="mega-menu-1"]');
    const containerMegaMenu = nav.querySelector('#mega-menu-1');

    // EXPECT
    expect(megaMenuButton.getAttribute('aria-expanded')).toEqual('false');
    expect(megaMenuButton.textContent).toContain('Méga menu 1');

    // WHEN
    megaMenuButton.click();
    tick();
    fixture.detectChanges();

    // EXPECT
    expect(megaMenuButton.getAttribute('aria-expanded')).toEqual('true');
    expect(containerMegaMenu.classList).toContain('fr-collapsing');
  }));

  it('should add toolsLinks and toolsLinks mobile ', fakeAsync(() => {
    testHostComponent.headerComponent.ngOnInit();
    tick();
    fixture.detectChanges();

    const headerToolsLinks = fixture.nativeElement.querySelector('.fr-header__tools-links');
    const headerToolsLinksMobile = fixture.nativeElement.querySelector('.fr-header__menu-links');

    const navLinks = headerToolsLinks.querySelectorAll('li');
    const navLinksMobile = headerToolsLinksMobile.querySelectorAll('li');

    expect(navLinks.length).toEqual(3);
    expect(navLinksMobile.length).toEqual(3);
  }));
});
