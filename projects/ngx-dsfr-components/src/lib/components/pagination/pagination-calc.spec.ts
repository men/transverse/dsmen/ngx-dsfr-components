import { describe, test } from '@jest/globals';
import { calcPages } from './pagination-calc';

/**
 * si n <= 5   : [[1,n]],                 n=4 -> (1 2 3 4)
 *                                        n=5 -> (1 2 3 4 5)
 * si p <= 2   : [[1,3], [n, n]],    p=2      -> (1 2 3 ... n)
 * si p >= n-3 : [min(p-1, n-2), n], p=3, n=6 -> (... 2 3 4 5 6)
 *                                   p=4, n=6 -> (... 3 4 5 6)
 *                                   p=5, n=6 -> (... 4 5 6)
 *                                   p=5, n=6 -> (... 4 5 6)
 * sinon [[p-1, p+1], [n, n]],       p=3, n=7 -> (... 2 3 4 ... 7)
 */
describe('Pagination', () => {
  test('calcul', () => {
    // n <= 5
    for (let n = 1; n <= 5; n++) {
      for (let p = 1; p <= n; p++) expect(calcPages(p, n)).toEqual([[1, n]]);
    }

    // p <= 2
    expect(calcPages(2, 6)).toEqual([
      [1, 3],
      [6, 6],
    ]);

    // p >= n-3
    expect(calcPages(3, 6)).toEqual([[2, 6]]);
    expect(calcPages(4, 6)).toEqual([[3, 6]]);
    expect(calcPages(5, 6)).toEqual([[4, 6]]);
    expect(calcPages(6, 6)).toEqual([[4, 6]]);

    // sinon
    expect(calcPages(3, 7)).toEqual([
      [2, 4],
      [7, 7],
    ]);
  });
});
