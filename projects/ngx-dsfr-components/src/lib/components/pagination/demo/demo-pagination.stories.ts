import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DemoPaginationComponent } from './demo-pagination.component';

const meta: Meta<DemoPaginationComponent> = {
  title: 'COMPONENTS/Pagination',
  component: DemoPaginationComponent,
  decorators: [moduleMetadata({ imports: [] })],
  parameters: { actions: false },
};
export default meta;
type Story = StoryObj<DemoPaginationComponent>;

export const Demo: Story = {
  args: {
    page: 1,
    count: 10,
  },
};
