
import { Component, Input } from '@angular/core';
import { DsfrPaginationModule } from '../pagination.module';

@Component({
  selector: 'edu-demo-pagination',
  templateUrl: './demo-pagination.component.html',
  styles: [
    `
      .page {
        width: 100%;
        height: 10rem;
        background: lightblue;
        border: 1px blue;
        display: flex;
        justify-content: center;
        align-items: center;
      }
    `,
  ],
  standalone: true,
  imports: [DsfrPaginationModule],
})
export class DemoPaginationComponent {
  @Input() page: number;
  @Input() count: number;
}
