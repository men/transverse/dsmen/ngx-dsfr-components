import { DSFR_HIGHLIGHT } from '.storybook/dsfr-themes';
import { dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, StoryObj } from '@storybook/angular';
import { DsfrSizeConst } from '../../shared';
import { DsfrHighlightComponent } from './highlight.component';

const meta: Meta = {
  title: 'COMPONENTS/Highlight',
  component: DsfrHighlightComponent,
  argTypes: {
    text: { if: { arg: 'text', truthy: true } },
    textSize: {
      control: { type: 'inline-radio' },
      options: Object.values(DsfrSizeConst),
    },
    customClass: { control: { type: 'select' }, options: [''].concat(Object.values(DSFR_HIGHLIGHT)) },
  },
};
export default meta;

type Story = StoryObj<DsfrHighlightComponent>;

export const Default: Story = {
  decorators: dsfrDecorator(`Contenu transmis via input 'text`),
  args: {
    textSize: DsfrSizeConst.MD,
    text: 'Les parents d’enfants de 11 à 14 ans n’ont aucune démarche à accomplir : les CAF versent automatiquement l’ARS aux familles déjà allocataires qui remplissent les conditions.',
  },
};

export const UsingSlot: Story = {
  decorators: dsfrDecorator('Contenu transmis via slot par défaut'),
  render: (args) => ({
    props: args,
    template: `
    <dsfr-highlight customClass="customClass">
      Les parents d’enfants de 11 à 14 ans n’ont aucune démarche à accomplir : les CAF versent automatiquement l’ARS aux familles déjà allocataires qui remplissent les conditions.
    </dsfr-highlight>
    `,
  }),
  args: {
    textSize: DsfrSizeConst.MD,
  },
};
