import { expect, test } from '@playwright/test';
import { getSbFrame, goToComponentPage } from '../../shared/utils/e2e.utils';

/** Default */
test('download.default', async ({ page }) => {
  const frame = getSbFrame(page);
  const div = frame.locator('div.fr-highlight');

  await goToComponentPage('highlight', page);

  await expect(div).toBeEnabled();
});
