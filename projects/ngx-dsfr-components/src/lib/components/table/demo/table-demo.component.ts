import { Component, OnInit, ViewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { delay, Observable, of } from 'rxjs';
import { DemoToolbarComponent } from '../../../shared/components/demo';
import { DsfrButtonModule } from '../../button';
import { DsfrButtonsGroupModule } from '../../buttons-group';
import { DsfrDataTableModule } from '../datatable.module';
import { DsfrTableComponent } from '../table.component';
import { DsfrColumn, DsfrRowOptions, DsfrSortColumn, DsfrTableOptions, DsfrTableState } from '../table.model';
import rowsMock from './mock-data.json';

@Component({
  selector: 'edu-table-demo',
  standalone: true,
  templateUrl: './table-demo.component.html',
  imports: [FormsModule, DsfrDataTableModule, DsfrButtonModule, DsfrButtonsGroupModule, DemoToolbarComponent],
})
export class EduTableDemoComponent implements OnInit {
  @ViewChild('datatable') table!: DsfrTableComponent;

  data: any[] = [];
  serverResultsLength: number;
  rows: any[] = [];
  state: DsfrTableState;
  rowsOptions: DsfrRowOptions[] = [{ id: '1', disableSelect: true }];
  initialSelection: any[];

  columns: DsfrColumn[] = [
    { label: 'Nom', field: 'name', sortable: true },
    { label: 'Prénom', field: 'firstName', sortable: false },
    { label: 'Date de naissance', field: 'birthDate', sortable: false },
    { label: 'Ville', field: 'city', textAlign: 'right' },
  ];

  columns2: DsfrColumn[] = [
    { label: 'fds', field: 'name', sortable: false },
    { label: 'fdsezez', field: 'firstName', sortable: false },
    { label: 'Daezte de naissance', field: 'birthDate', sortable: false },
    { label: 'Villeze', field: 'city' },
  ];

  data2: any[] = [];

  /** @internal */
  tableOptions: DsfrTableOptions = {
    selectable: true,
    bordered: true,
  };
  toggleSelect: boolean = true;
  loading: boolean;
  loadingServer: boolean;

  ngOnInit(): void {
    this.rows = rowsMock;
    const offset = 0;
    this.loading = true;
    this.initialSelection = ['4'];
    this.data = [...this.rows.slice(offset, offset + 10)];
    this.generateMockData();
  }

  /** @internal */
  public onviewSelect(view: string): void {
    console.log('change view' + view);
  }

  /** @internal */
  public onSearch(search: string): void {
    console.log('recherche' + search);
  }

  /** @internal */
  public onStateChange(state: DsfrTableState) {
    this.state = state;
    const offset = (state.page - 1) * state.rowsPerPage;

    this.paginate(offset, state.rowsPerPage);
  }

  /** @internal */
  public onSortChange(sort: DsfrSortColumn) {
    console.log('sort change');
    console.log(sort);
  }

  /** @internal */
  public goToPage() {
    // this.dataTableService.goToPage(4);
  }

  /** @internal */
  public paginate(offset: number, rowsPerPage: number) {
    this.data = [];
    this.data = [...this.rows.slice(offset, offset + rowsPerPage)];
    console.log(this.initialSelection);
  }

  /** @internal */
  generateMockData$(): Observable<any[]> {
    this.loading = true;
    const data: any[] = [];
    for (let i = 0; i < 1000; i++) {
      data.push(this.generateRandomData(i + 1));
    }

    return of(data).pipe(delay(2000));
  }

  /** @internal */
  generateMockData() {
    this.generateMockData$().subscribe((res) => {
      this.data2 = res;
      this.loading = false;
    });
  }

  /** @internal */
  addData() {
    this.loadingServer = true;

    setTimeout(() => {
      for (let i = 0; i < 1000; i++) {
        this.rows.push(this.generateRandomData(i + 1));
      }
      this.onStateChange(this.state);
      this.loadingServer = false;
    }, 1000);
  }

  /** @internal */
  updateTableOption() {
    this.table.getService().updateTableOptions('cellSize', 'SM');
  }

  /** @internal */
  updateRowsOptions() {
    this.toggleSelect = !this.toggleSelect;

    this.rowsOptions = [
      { id: '1', disableSelect: this.toggleSelect },
      { id: '2', disableSelect: !this.toggleSelect },
    ];

    this.rowsOptions = [...this.rowsOptions];
    this.table.dataTableService.setSelectedRows(['5']);
  }

  selectRows() {
    this.table.dataTableService.setSelectedRows(['5', '7']);
  }

  /** @internal */
  updateSingleData() {
    this.data[8].firstName = 'JACK';
    this.table.getService().refreshData(this.data);
  }

  /** @internal */
  onSelection(e: { row: any; selectedRows: any[] }) {
    this.initialSelection = [...this.initialSelection, ...e.selectedRows.map((r) => r.id)];
    this.initialSelection = [...new Set(this.initialSelection)];
  }

  private generateRandomData(id: number) {
    return {
      id: id.toString(),
      name: this.getRandomName(),
      firstName: this.getRandomFirstName(),
      birthDate: this.getRandomBirthDate(),
    };
  }

  private getRandomName(): string {
    const names = [
      'Smith',
      'Johnson',
      'Williams',
      'Brown',
      'Jones',
      'Garcia',
      'Martinez',
      'Rodriguez',
      'Davis',
      'Hernandez',
      'Lopez',
      'Gonzalez',
      'Wilson',
      'Anderson',
      'Thomas',
      'Taylor',
      'Moore',
      'Jackson',
      'Martin',
      'Lee',
      'Perez',
      'Thompson',
      'White',
      'Harris',
      'Sanchez',
      'Clark',
      'Ramirez',
      'Lewis',
      'Robinson',
      'Walker',
    ];
    return names[Math.floor(Math.random() * names.length)];
  }

  private getRandomFirstName(): string {
    const firstNames = [
      'John',
      'Emma',
      'Aaliyah',
      'Liam',
      'Olivia',
      'Mateo',
      'Isabella',
      'Sofia',
      'Ethan',
      'Mia',
      'Lucas',
      'Ava',
      'James',
      'Harper',
      'Alexander',
      'Charlotte',
      'Mason',
      'Amelia',
      'Benjamin',
      'Evelyn',
      'Henry',
      'Scarlett',
      'Jackson',
      'Grace',
      'David',
      'Zoey',
      'Samuel',
      'Nora',
      'Gabriel',
      'Aria',
    ];
    return firstNames[Math.floor(Math.random() * firstNames.length)];
  }

  private getRandomBirthDate(): string {
    const start = new Date(1970, 0, 1).getTime();
    const end = new Date(2000, 11, 31).getTime();
    const date = new Date(start + Math.random() * (end - start));
    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, '0');
    const day = date.getDate().toString().padStart(2, '0');
    return `${year}-${month}-${day}`;
  }
}
