import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrTableComponent } from '../table.component';
import { EduTableDemoComponent } from './table-demo.component';

const meta: Meta = {
  title: 'COMPONENTS/Table',
  component: EduTableDemoComponent,
  decorators: [moduleMetadata({ imports: [DsfrTableComponent] })],
};
export default meta;
type Story = StoryObj<EduTableDemoComponent>;

export const ServerSide: Story = {};
