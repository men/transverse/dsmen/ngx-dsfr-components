import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrCellCheckboxComponent } from './component/cell-checkbox.component';
import { SortButtonHeaderComponent } from './component/sort-button.component';
import { DsfrTableFooterComponent } from './component/table-footer.component';
import { EdutableHeaderComponent } from './component/table-header.component';
import { DsfrCellDirective } from './directive/cell.directive';
import { DsfrColumnDirective } from './directive/column.directive';
import { DsfrSelectAllDirective } from './directive/header-select.directive';
import { DsfrSelectRowDirective } from './directive/row-select.directive';
import { DsfrSortColumnDirective } from './directive/sort-column.directive';
import { DsfrTableComponent } from './table.component';
@NgModule({
  imports: [
    CommonModule,
    SortButtonHeaderComponent,
    DsfrColumnDirective,
    DsfrSortColumnDirective,
    DsfrTableComponent,
    DsfrSelectRowDirective,
    DsfrCellCheckboxComponent,
    DsfrCellDirective,
    DsfrSelectAllDirective,
    DsfrTableFooterComponent,
    EdutableHeaderComponent,
  ],
  exports: [
    DsfrColumnDirective,
    DsfrSortColumnDirective,
    DsfrCellDirective,
    DsfrSelectRowDirective,
    DsfrSelectAllDirective,
    DsfrTableFooterComponent,
    DsfrTableComponent,
  ],
})
export class DsfrDataTableModule {}
