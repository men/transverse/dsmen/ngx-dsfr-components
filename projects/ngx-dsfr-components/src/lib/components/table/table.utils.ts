/** ajouter ou supprimer une ligne d'une liste */
export function updateRowByField(row: any, list: any[], idField: string | null = null): void {
  // recherche de la ligne selon si dataKey est défini ou par l'index sinon
  const index: number = list.findIndex((r) => (idField ? r[idField] === row[idField] : r.index === row.index));

  if (index !== -1) {
    list.splice(index, 1); // suppression de la ligne
    list.push(row); // ajout de la ligne
  }
}

/**
 * Met à jour ou ajoute une propriété dans un objet.
 * @param obj - L'objet à modifier.
 * @param key - Le nom de la propriété à mettre à jour ou ajouter.
 * @param value - La nouvelle valeur à assigner à la propriété spécifiée.
 * @returns - La nouvelle référence de l'objet mis à jour avec la propriété modifiée ou ajoutée.
 */
export function updateOrAddProperty<T extends object>(obj: T, key: keyof T, value: T[keyof T]): T {
  obj[key] = value;
  return { ...obj };
}
