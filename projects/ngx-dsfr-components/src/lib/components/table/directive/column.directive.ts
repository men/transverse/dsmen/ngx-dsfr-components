import { Directive, HostBinding, inject, Input } from '@angular/core';
import { DsfrTableComponent } from '../table.component';
import { DsfrColumn } from '../table.model';

/**
 * Directive ajout de style pour une colonne (taille, alignements)
 */
@Directive({
  selector: '[dsfrCol]',
  standalone: true,
})
export class DsfrColumnDirective {
  @Input('dsfrCol') col: DsfrColumn;

  @HostBinding('class.fr-cell--multiline') @Input() allowWrap: boolean = false;

  protected table: DsfrTableComponent = inject(DsfrTableComponent);

  @HostBinding('class.fr-cell--fixed')
  get isFixed(): boolean {
    return !!this.col.fixed;
  }

  @HostBinding('class.fr-cell--multiline')
  get isMultiline(): boolean | undefined {
    return this.col?.allowWrap;
  }

  @HostBinding('class')
  get sizeClass(): string {
    switch (this.col.minWidth) {
      case 'XS':
        return 'fr-col--xs';
      case 'SM':
        return 'fr-col--sm';
      case 'MD':
        return 'fr-col--md';
      case 'LG':
        return 'fr-col--lg';
      default:
        return '';
    }
  }

  // Ajout de l'attribut scope si ce n'est pas un tableau complexe (pas de summary)
  @HostBinding('attr.scope')
  get scope(): string | null {
    return this.table.headerSummaryTemplate ? null : 'col';
  }

  @HostBinding('class.fr-cell--top')
  get isTopCell(): boolean {
    return this.col.verticalAlign === 'top';
  }

  @HostBinding('class.fr-cell--bottom')
  get isBottomCell(): boolean {
    return this.col.verticalAlign === 'bottom';
  }

  @HostBinding('class.fr-cell--center')
  get isCenterCell(): boolean {
    return this.col.textAlign === 'center';
  }

  @HostBinding('class.fr-cell--right')
  get isRightCell(): boolean {
    return this.col.textAlign === 'right';
  }
}
