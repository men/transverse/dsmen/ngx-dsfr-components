import {
  Directive,
  ElementRef,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
  Renderer2,
  ViewContainerRef,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { SortButtonHeaderComponent } from '../component/sort-button.component';
import { DsfrColumn } from '../table.model';
import { DsfrColumnDirective } from './column.directive';

/**
 * Directive ajout du tri sur l'en-tête de colonne (icône et logique)
 */
@Directive({
  selector: '[dsfrColumn]',
  standalone: true,
})
export class DsfrSortColumnDirective extends DsfrColumnDirective implements OnInit, OnDestroy {
  @Input('dsfrColumn') col: DsfrColumn;

  private column: DsfrColumn | undefined;
  private subscription: Subscription;

  constructor(
    private element: ElementRef,
    private viewContainer: ViewContainerRef,
    private renderer: Renderer2,
  ) {
    super();
  }

  @HostBinding('attr.aria-sort')
  get ariaSort(): string | undefined {
    return this.table.getAttributAriaSort(this.col.field);
  }

  public ngOnInit() {
    this.column = this.table.columns.find((col: DsfrColumn) => col.field === this.col.field);
    if (this.column && this.column.sortable) {
      const componentRef = this.viewContainer.createComponent<SortButtonHeaderComponent>(SortButtonHeaderComponent);
      componentRef.instance.field = this.column.field;

      this.subscription = this.table.activeSort$.subscribe((activeSort) => {
        componentRef.instance.activeSort = activeSort;
      });

      componentRef.instance.sortChanged.subscribe((event: string) => {
        this.table.sortTable(event);
      });

      this.addWrappingElement(this.element.nativeElement, componentRef.location.nativeElement);
    }
  }

  public ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  /**
   * Ajouter une div wrapper fr-cell--sort autour des éléments pour style DSFR
   * @param nativeElement composant <th>
   * @param componentElement composant sort-buton
   */
  private addWrappingElement(nativeElement: HTMLElement, componentElement: HTMLElement) {
    const wrapperDiv = this.renderer.createElement('div');
    this.renderer.addClass(wrapperDiv, 'fr-cell--sort');

    // Déplacer tous les enfants actuels du th dans le <div>
    while (nativeElement.firstChild) {
      this.renderer.appendChild(wrapperDiv, nativeElement.firstChild);
    }

    // Ajouter le <div> au <th>
    this.renderer.appendChild(nativeElement, wrapperDiv);

    // Ajouter le composant au <th>
    this.renderer.appendChild(wrapperDiv, componentElement);
  }
}
