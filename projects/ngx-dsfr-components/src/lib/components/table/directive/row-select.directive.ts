import {
  ComponentRef,
  Directive,
  ElementRef,
  HostBinding,
  Input,
  OnChanges,
  OnDestroy,
  ViewContainerRef,
  inject,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { DsfrCellCheckboxComponent } from '../component/cell-checkbox.component';
import { DsfrTableComponent } from '../table.component';

/**
 * Directive checkbox sur la première cellule (Selectionner/Déselectionner la ligne)
 */
@Directive({
  selector: '[dsfrSelectRow]',
  standalone: true,
})
export class DsfrSelectRowDirective implements OnChanges, OnDestroy {
  @Input('dsfrSelectRow') row: any;
  @Input() disableSelect: boolean;

  @HostBinding('class.fr-cell--fixed') fixedClass = true;

  @HostBinding('attr.aria-selected') checked: boolean;

  public componentRef: ComponentRef<DsfrCellCheckboxComponent> | undefined;

  private table: DsfrTableComponent = inject(DsfrTableComponent);
  private subscription: Subscription;

  constructor(
    private element: ElementRef,
    private viewContainer: ViewContainerRef,
  ) {}

  /** ajouter l'attribut scope si ce n'est pas un tableau complexe (pas de summary) */
  @HostBinding('attr.scope')
  get scope() {
    return this.table.headerSummaryTemplate ? undefined : 'row';
  }

  public ngOnChanges() {
    if (!this.disableSelect && !this.componentRef) {
      this.componentRef = this.viewContainer.createComponent<DsfrCellCheckboxComponent>(DsfrCellCheckboxComponent);
      this.componentRef.instance.row = this.row;
      this.componentRef.instance.checked = this.checked;

      this.componentRef.instance.tableId = this.table.tableId;

      this.subscription = this.addSubscriptionClickEvent();

      this.element.nativeElement?.insertBefore(
        this.componentRef.location.nativeElement,
        this.element.nativeElement.firstChild,
      );
    } else if (this.disableSelect && this.componentRef) {
      // detruire la checkbox et le composant si le disabledSelect a été ajouté
      this.simulateClick(false); // décocher la ligne
      this.componentRef.location.nativeElement.remove();
      this.componentRef.destroy();
      this.componentRef = undefined;
    }
  }

  /**
   * Apres désélection/selection de toutes les cases a cocher
   * Simuler le clic sur les cases à cocher pour lancer le script DSFR (appliquer le style sur la ligne)
   * Interrompre l'ecoute de l'evenement pour ne pas lancer selectRow()
   */
  public simulateClick(toSelect: boolean) {
    if (this.componentRef) {
      this.subscription.unsubscribe(); // interrompre select row
      const checkbox: HTMLInputElement = this.componentRef.location?.nativeElement?.getElementsByTagName('input')[0];
      if (checkbox && checkbox.checked !== toSelect) {
        checkbox.click(); // simulation du clic uniquement si la valeur doit changer
      }

      this.subscription = this.addSubscriptionClickEvent(); // replacer l'evenement selectRow
    }
  }

  public ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private addSubscriptionClickEvent(): Subscription {
    //@ts-ignore
    return this.componentRef.instance.selectRow.subscribe(() => {
      this.table.selectRow(this.row);
    });
  }
}
