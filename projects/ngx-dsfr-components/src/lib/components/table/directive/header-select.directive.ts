import {
  ComponentRef,
  Directive,
  ElementRef,
  HostBinding,
  OnDestroy,
  OnInit,
  ViewContainerRef,
  inject,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { DsfrCellCheckboxComponent } from '../component/cell-checkbox.component';
import { DsfrTableComponent } from '../table.component';

/**
 * Directive pour la checkbox d'en-tête de colonne (tout sélectionner/déselectionner)
 */
@Directive({
  selector: '[dsfrSelectAll]',
  standalone: true,
})
export class DsfrSelectAllDirective implements OnInit, OnDestroy {
  @HostBinding('class.fr-cell--fixed') fixedClass = true;

  public componentRef: ComponentRef<DsfrCellCheckboxComponent>;

  private table: DsfrTableComponent = inject(DsfrTableComponent);
  private subscription1$: Subscription;
  private subscription2$: Subscription;

  constructor(
    private element: ElementRef,
    private viewContainer: ViewContainerRef,
  ) {}

  // Ajout de l'attribut scope si ce n'est pas un tableau complexe (pas de summary)
  @HostBinding('attr.scope')
  get scope(): string | null {
    return this.table.headerSummaryTemplate ? null : 'col';
  }

  public ngOnInit() {
    this.componentRef = this.viewContainer.createComponent<DsfrCellCheckboxComponent>(DsfrCellCheckboxComponent);
    this.componentRef.instance.row = { index: 'all' };
    this.componentRef.instance.showSelectAll = true;

    this.componentRef.instance.tableId = this.table.tableId;
    this.subscription1$ = this.addSubscriptionClickEvent();

    /** Ecoute de l'evenement isAllChecked : toutes les cases sont cochées ou une seule case est décochée */
    this.subscription2$ = this.table.isAllChecked$.subscribe((isAllChecked) => {
      this.simulateClick(isAllChecked);
    });

    this.element.nativeElement?.appendChild(this.componentRef.location.nativeElement);
  }

  /**
   * Apres désélection/selection d'une case
   * Simuler le clic sur la case à cocher globale si la valeur doit changer
   * Interrompre l'ecoute de l'evenement pour ne pas lancer toggleshowSelectAll()
   */
  public simulateClick(checked: boolean) {
    this.subscription1$.unsubscribe(); // interrompre select row

    const checkbox: HTMLInputElement = this.componentRef.location?.nativeElement?.getElementsByTagName('input')[0];

    if (checkbox && checkbox.checked !== checked) {
      checkbox.click(); // simulation du clic uniquement si la valeur doit changer
    }

    this.subscription1$ = this.addSubscriptionClickEvent(); // replacer l'evenement selectRow
  }

  public ngOnDestroy() {
    if (this.subscription1$) {
      this.subscription1$.unsubscribe();
    }
    if (this.subscription2$) {
      this.subscription2$.unsubscribe();
    }
  }

  private addSubscriptionClickEvent(): Subscription {
    return this.componentRef.instance.selectRow.subscribe((checked) => {
      this.table.toggleshowSelectAll(checked);
    });
  }
}
