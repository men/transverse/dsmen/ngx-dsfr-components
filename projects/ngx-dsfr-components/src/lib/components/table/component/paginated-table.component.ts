import { Component, EventEmitter, inject, Input, Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { DsfrOption } from '../../../shared';
import { DsfrDataTableService } from '../service/datatable.service';
import { DsfrTablePaginationEvent, DsfrTableState } from '../table.model';

/**
 * Classe abstraite gestion de la pagination du tableau
 * selon le nombre de lignes à afficher et nombre de pages
 */
@Component({ template: '' })
export abstract class BasePaginatedTableComponent {
  /** Etat initial de la table à appliquer (tri, page et nombre de lignes affichées par page) */
  @Input() initialState: DsfrTableState;

  /** Afficher la pagination */
  @Input() pagination: boolean;

  /** Activer le mode serveur (pagination, tri et sélection côté serveur) */
  @Input() serverSide: boolean;

  /** Choix de taille de page (par défaut 10, 20, 50, 100). Pas d'affichage du select si non défini */
  @Input()
  rowsPerPageOptions: DsfrOption[] = [
    { label: '10 lignes par page', value: 10 },
    { label: '20 lignes par page', value: 20 },
    { label: '50 lignes par page', value: 50 },
    { label: '100 lignes par page', value: 100 },
  ];

  /** Changement de pagination (page affichée ou taille de page) */
  @Output() paginationChange: EventEmitter<DsfrTablePaginationEvent> = new EventEmitter<{
    currentPage: number;
    rowsPerPage: number;
  }>();

  /** Etat modifié */
  @Output() readonly stateChange: EventEmitter<DsfrTableState> = new EventEmitter<DsfrTableState>();

  /** @internal Total résultats */
  totalElements = 0;

  /** @internal nombre total d'éléments sélectionnables */
  selectableRows: any[];

  /** @internal  Page courante */
  currentPage: number = 1;

  /** @internal Total pages */
  totalPage = 0;

  /** @internal Page size */
  rowsPerPage = 10;

  /**@internal Service datatable pour rafraichissement des données */
  public dataTableService = inject(DsfrDataTableService);

  /** @internal Etat du tableau (tri, page courante, page size) */
  protected tableState$: BehaviorSubject<DsfrTableState>;

  private _serverResultsLength: number;

  /** @internal */
  get serverResultsLength(): number {
    return this._serverResultsLength;
  }

  /** Total du nombre de résultats (utilisé si serverSide) */
  @Input() set serverResultsLength(value: number) {
    this._serverResultsLength = value;
    if (this.serverSide) {
      this.totalElements = value;
    }
    this.totalPage = this.calculateTotalPage();
    if (this.currentPage > this.totalPage) {
      // si le nombre de pages est inférieur a la page courante, aller à la dernière page
      this.currentPage = this.totalPage;
    } else if (this.currentPage === 0 && this.totalElements > 0) {
      this.currentPage = 1;
    }
  }

  /** Accès aux méthodes de rafraichissement de données du tableau */
  public getService(): DsfrDataTableService {
    return this.dataTableService;
  }

  /**
   * Changement du nombre de lignes affichés par page
   * calcul des pages et changement des lignes affichées
   * @internal
   */
  public onRowsPerPageChange(rowsPerPage: number): void {
    this.rowsPerPage = rowsPerPage;
    this.totalPage = this.calculateTotalPage();
    if (this.currentPage > this.totalPage) {
      // si le nombre de pages est inférieur a la page courante, aller à la dernière page
      this.currentPage = this.totalPage;
    }

    this.onPageSelect(this.currentPage);
  }

  /**
   * Au changement de page
   * @param page numéro de page
   * @internal
   */
  public onPageSelect(page: number): void {
    this.currentPage = page;
    this.paginationChange.emit({ currentPage: this.currentPage, rowsPerPage: this.rowsPerPage });

    // mise a jour des données affichées
    this.tableState$.next({ ...this.tableState$.getValue(), page: page, rowsPerPage: this.rowsPerPage });
  }

  /** Calcul du total de page selon le nombre total de résultats et le nombre de lignes affichées
   * @internal
   */
  protected calculateTotalPage(): number {
    return Math.ceil(this.totalElements / this.rowsPerPage);
  }

  /**
   * Après changement de page
   * Mise a jour des données affichées selon l'offset et la taille de page
   * @internal
   * */
  protected paginateData(rows: any, state: DsfrTableState): any[] {
    if (!this.serverSide && this.totalElements !== rows.length) {
      this.totalElements = rows.length;
      this.totalPage = this.calculateTotalPage();
    }
    const offset = (state.page - 1) * state.rowsPerPage;

    return rows.slice(offset, offset + state.rowsPerPage);
  }
}
