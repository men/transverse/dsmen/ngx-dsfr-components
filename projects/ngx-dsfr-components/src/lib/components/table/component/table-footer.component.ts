import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewEncapsulation } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrFormSelectModule } from '../../../forms/form-select';
import { DsfrOption, newUniqueId } from '../../../shared';
import { DsfrI18nPipe } from '../../../shared/i18n/i18n.pipe';
import { DsfrPaginationModule } from '../../pagination';
import { DsfrTableState } from '../table.model';

/**
 * Composant pied de page du tableau
 * Contient trois éléments optionnel mais dans cet ordre :
 * - start: total de lignes, sélection du nombre de lignesn à afficher si pagination
 * - middle: composant de pagination commun dsfr-pagination
 * - end: template pour afficher des boutons d'actions agissant sur tout le tableau
 */
@Component({
  selector: 'dsfr-table-footer',
  templateUrl: './table-footer.component.html',
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule, FormsModule, DsfrFormSelectModule, DsfrPaginationModule, DsfrI18nPipe],
})
export class DsfrTableFooterComponent implements OnInit {
  /** Template optionnel afficher des boutons d'actions dans footer end */
  @Input() footerActionsTemplate!: TemplateRef<any>;

  /** Afficher la pagination */
  @Input() showPagination: boolean;

  /** Afficher le total de lignes */
  @Input() showFooterResult: boolean;

  /** Message personnalisé pour afficher le total de lignes */
  @Input() footerResultsTemplate: TemplateRef<any>;

  /** Etat initial pour la pagination */
  @Input() initialState: DsfrTableState;

  /** Nombre total d'éléments */
  @Input() totalElements: number;

  /** Nombre total de pages */
  @Input() totalPage: number;

  /** Page actuelle affichée (commence à 1) */
  @Input() currentPage: any;

  /** Options pour la sélection du nombre de lignes à afficher */
  @Input() rowsPerPageOptions: DsfrOption[];

  /** Message affiché en cas de tableau vide ('Aucun résultat' par défaut) */
  @Input() emptyResultsMessage: string;

  /** Désactiver la sélection du nombre de lignes à afficher */
  @Input() disabledRowsPerPage: boolean;

  @Output() rowsPerPageChange: EventEmitter<number> = new EventEmitter(); // changement du nombre de lignes à afficher
  @Output() pageSelect: EventEmitter<number> = new EventEmitter(); // changement de page

  rowsPerPage: number; // current page size selected
  selectId: string; // id du select de taille de page

  /** @internal */
  public ngOnInit(): void {
    this.rowsPerPage = this.initialState?.rowsPerPage ?? this.rowsPerPageOptions[0].value;
    this.currentPage = this.initialState?.page ?? 1;
    this.selectId = newUniqueId();
  }

  public onPageSelect(pageNumber: number): void {
    this.pageSelect.emit(pageNumber);
  }

  public onChangerowsPerPage(): void {
    this.rowsPerPageChange.emit(this.rowsPerPage);
  }
}
