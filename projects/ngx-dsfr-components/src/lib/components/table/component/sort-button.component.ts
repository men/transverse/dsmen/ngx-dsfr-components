import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { DsfrI18nPipe } from '../../../shared/i18n/i18n.pipe';
import { DsfrSortColumn } from '../table.model';

/**
 * Composant d'affichage du bouton de tri pour une colonne (icônes flêche)
 */
@Component({
  selector: 'edu-sort-button-header',
  standalone: true,
  encapsulation: ViewEncapsulation.None,
  imports: [DsfrI18nPipe],
  styles: ['.fr-btn--sort {margin-left: 1rem}'], // fix margin alignement sort icon and text
  template: `
    <button
      (click)="sortTable()"
      type="button"
      [attr.aria-sort]="activeSort && activeSort.field === field ? activeSort.order : undefined"
      class="fr-btn--sort fr-btn fr-btn--sm">
      {{ 'commons.sort' | dsfrI18n }}
    </button>
  `,
})
export class SortButtonHeaderComponent {
  @Input() field: string;
  @Output() sortChanged = new EventEmitter<string>();

  _activeSort: DsfrSortColumn | undefined;

  get activeSort(): DsfrSortColumn | undefined {
    return this._activeSort;
  }

  /** Tri actif actuel sur le tableau : désactiver le bouton si un autre tri est effectué (pas de multisort) */
  @Input() set activeSort(activeSort: DsfrSortColumn | undefined) {
    this._activeSort = activeSort;
  }

  sortTable() {
    this.sortChanged.emit(this.field);
  }
}
