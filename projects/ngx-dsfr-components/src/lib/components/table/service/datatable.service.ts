import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { DsfrTableOptions } from '../table.model';
import { updateOrAddProperty } from '../table.utils';

/**
 * Service pour la datatable
 * Gestion du rafraichissement des données
 */
@Injectable()
export class DsfrDataTableService {
  private readonly _dataSubject = new Subject<any[]>();
  // eslint-disable-next-line @typescript-eslint/member-ordering
  readonly data$: Observable<any[]> = this._dataSubject.asObservable();

  private readonly _tableOptionsSubject: BehaviorSubject<DsfrTableOptions> = new BehaviorSubject<DsfrTableOptions>({});

  // eslint-disable-next-line @typescript-eslint/member-ordering
  readonly tableOptions$: Observable<DsfrTableOptions> = this._tableOptionsSubject.asObservable();
  // eslint-disable-next-line @typescript-eslint/member-ordering
  readonly _selectionSubject$: Subject<any[]> = new Subject<any[]>();

  constructor() {}

  /**
   * Mise à jour des données avec rafraichissement du tableau
   * @param newData nouvelles données du tableau
   * @returns void
   */
  public refreshData(newData: any[]): void {
    this.setData(newData);
  }

  /**
   * Met à jour une propriété spécifique de l'objet `tableOptions`.
   * @param key Le nom de la propriété de l'interface `tableOptions` à mettre à jour.
   * @param newValue La nouvelle valeur de la propriété spécifiée par `key`
   * @returns void
   */
  public updateTableOptions(key: keyof DsfrTableOptions, newValue: any): void {
    const newTableOptions = updateOrAddProperty(this._tableOptionsSubject.getValue(), key, newValue);
    this.setTableOptions(newTableOptions);
  }

  /**
   * Sélectionner les lignes programmatiquement
   * @param selectedRows la liste des identifiants des lignes qui seront sélectionnées
   **/
  public setSelectedRows(selectedRows: any[]) {
    this._selectionSubject$.next(selectedRows);
  }

  /** @internal */
  public setTableOptions(tableOptions: DsfrTableOptions) {
    this._tableOptionsSubject.next(tableOptions);
  }

  private setData(data: any[]): void {
    this._dataSubject.next(data);
  }
}
