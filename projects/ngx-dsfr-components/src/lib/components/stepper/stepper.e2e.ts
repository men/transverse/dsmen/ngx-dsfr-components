import { expect, test } from '@playwright/test';
import { getSbFrame, goToComponentPage } from '../../shared/utils/e2e.utils';

test('stepper.heading-level', async ({ page }) => {
  const frame = getSbFrame(page);
  const stepperTitle = frame.locator('h3.fr-stepper__title');
  const translateBtn = frame.locator('.fr-translate__btn');
  const dropdown = frame.locator('.fr-collapse');
  const langLink = dropdown.locator(`a.fr-translate__language[lang="en"]`);
  const stepperStateElt = frame.locator('.fr-stepper__state');
  const stepperDetailsElt = frame.locator('.fr-stepper__details > span');

  await goToComponentPage('stepper', page, 'heading-level');

  await expect(dropdown).not.toBeVisible();
  await expect(stepperTitle).toBeVisible();

  await translateBtn.click();

  await expect(dropdown).toBeVisible();
  await expect(langLink).toBeAttached();
  await expect(langLink).toBeVisible();

  await langLink.click();

  await expect(stepperStateElt).toContainText('Step');
  await expect(stepperDetailsElt).toContainText('Next step:');
});
