import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HeadingModule } from '../../shared';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrStepperComponent } from './stepper.component';

@NgModule({
  declarations: [DsfrStepperComponent],
  exports: [DsfrStepperComponent],
  imports: [CommonModule, HeadingModule, DsfrI18nPipe],
})
export class DsfrStepperModule {}
