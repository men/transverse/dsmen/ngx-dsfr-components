import { dsfrDecorator } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrRangeComponent } from './range.component';

const meta: Meta = {
  title: 'COMPONENTS/Range',
  component: DsfrRangeComponent,
  decorators: [moduleMetadata({ imports: [FormsModule] })],
  argTypes: {},
  parameters: {
    controls: { exclude: ['name', 'ariaControls', 'id'] },
  },
};
export default meta;
type Story = StoryObj<DsfrRangeComponent>;

export const Default: Story = {
  decorators: dsfrDecorator('Range simple'),
  args: {
    label: 'Range',
    min: 0,
    max: 100,
    hint: 'Texte de description additionnel',
    value: 33,
  },
};

export const WithStep: Story = {
  args: {
    label: 'Range with step 10',
    min: 0,
    max: 50,
    step: 10,
  },
};

export const Small: Story = {
  args: {
    label: 'Range small',
    min: 0,
    max: 100,
    size: 'SM',
  },
};

export const WithSuffixPrefix: Story = {
  args: {
    label: 'Range with suffix/prefix',
    min: 0,
    max: 100,
    suffix: '%',
    prefix: '~',
  },
};

export const HideMinMax: Story = {
  args: {
    label: 'Range with Min & Max hidden',
    min: 0,
    max: 100,
    hideMinMax: true,
  },
};

export const Double: Story = {
  args: {
    label: 'Curseur double',
    min: 0,
    max: 100,
    size: 'SM',
    value: [25, 75],
  },
};

export const Disabled: Story = {
  args: {
    label: 'Curseur disabled',
    min: 0,
    max: 100,
    value: 75,
    disabled: true,
  },
};

export const Error: Story = {
  args: {
    label: 'Curseur avec erreur',
    min: 0,
    max: 100,
    value: 75,
    error: 'Ceci est une erreur',
  },
};

export const SlotLabel: Story = {
  decorators: dsfrDecorator('Range avec label en tant que slot'),
  args: {
    ...Default.args,
  },
  render: (args) => ({
    props: args,
    template: `
    <dsfr-range [min]="0" [max]="100" [value]="75">
        <span label>  <span aria-hidden="true" class="fr-icon-image-fill"></span> <span style="color: var(--info-425-625);font-weight: bold;"> Label </span> pour le range</span>
    </dsfr-range>`,
  }),
};

export const ngModel: Story = {
  args: {
    ...Default.args,
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-range [(ngModel)]="value" label="Range avec ngModel"></dsfr-range>
    <div class="sb-smaller">Valeur du modèle : {{ value }}</div>`,
  }),
};

export const ngModelDouble: Story = {
  args: {
    ...Double.args,
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-range [(ngModel)]="value" label="Range Double avec ngModel"></dsfr-range>
    <div class="sb-smaller">Valeur du modèle : {{ value[0] }} - {{ value[1] }}</div>`,
  }),
};
