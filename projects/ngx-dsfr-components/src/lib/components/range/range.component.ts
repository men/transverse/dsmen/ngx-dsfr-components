import { CommonModule } from '@angular/common';
import { Component, forwardRef, Input, ViewEncapsulation } from '@angular/core';
import { FormsModule, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DefaultControlComponent, DsfrSize, DsfrSizeConst } from '../../shared';

@Component({
  selector: 'dsfr-range',
  templateUrl: './range.component.html',
  encapsulation: ViewEncapsulation.None,
  imports: [CommonModule, FormsModule],
  standalone: true,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DsfrRangeComponent),
      multi: true,
    },
  ],
})
export class DsfrRangeComponent extends DefaultControlComponent<number | [number, number]> {
  /** Valeur minimale possible */
  @Input() min: number = 0;

  /** Valeur max possible */
  @Input() max: number = 100;

  /** Ne pas afficher le minimum et le maximum  */
  @Input() hideMinMax: boolean = false;

  /** Incrément que doit suivre la valeur (1 par défaut)  */
  @Input() step: number | undefined;

  /** Taille du curseur (SM et MD possibles, MD par défaut) */
  @Input() size: DsfrSize;

  /** Préfixe affiché avant les valeurs  */
  @Input() prefix: string | undefined;

  /** Suffixe affiché après les valeurs  */
  @Input() suffix: string | undefined;

  /** Désactiver le curseur */
  @Input() disabled: boolean = false;

  /** Affiche un message d'erreur  */
  @Input() error: string | undefined;

  /** @internal Curseur double  */
  double: boolean = false;

  constructor() {
    super();
  }

  get hintId() {
    return this.inputId + '-hint-text';
  }

  /**@internal */
  isDouble(): boolean {
    return Array.isArray(this.value);
  }

  /** @internal */
  isSmall(): boolean {
    return this.size == DsfrSizeConst.SM;
  }

  /** @internal */
  getValue(index: number) {
    return Array.isArray(this.value) ? this.value[index] : undefined;
  }

  /** @internal */
  onInputChange(index: number, value: number) {
    if (Array.isArray(this.value)) {
      this.value[index] = value;
    }
  }
}
