import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrBadgeModule } from '../badge';
import { DsfrBadgesGroupComponent } from './badges-group.component';

@NgModule({
  declarations: [DsfrBadgesGroupComponent],
  exports: [DsfrBadgesGroupComponent],
  imports: [CommonModule, DsfrBadgeModule],
})
export class DsfrBadgesGroupModule {}
