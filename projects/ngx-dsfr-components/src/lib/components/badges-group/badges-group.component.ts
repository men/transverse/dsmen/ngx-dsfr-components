import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrBadge } from '../badge';

@Component({
  selector: 'dsfr-badges-group',
  templateUrl: './badges-group.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrBadgesGroupComponent {
  /** Tableau de type `<Badge>`. */
  @Input() badges: DsfrBadge[] = [];

  /** @since 1.9.0 Taille des badges */
  @Input() small = false;
}
