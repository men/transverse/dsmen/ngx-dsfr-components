import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HeadingModule, WatchAttrDirective } from '../../shared';
import { DsfrAccordionComponent } from './accordion.component';

@NgModule({
  declarations: [DsfrAccordionComponent],
  exports: [DsfrAccordionComponent],
  imports: [CommonModule, HeadingModule, WatchAttrDirective],
})
export class DsfrAccordionModule {}
