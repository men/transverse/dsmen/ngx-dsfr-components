import { expect, test } from '@playwright/test';
import { getSbFrame, goToComponentPage } from '../../shared/utils/e2e.utils';

test('accordion.default', async ({ page }) => {
  const frame = getSbFrame(page);
  const heading = frame.getByRole('button');

  await goToComponentPage('accordion', page);
  await expect(heading).toHaveAttribute('aria-expanded', 'false');

  await heading.first().click();

  await expect(heading).toHaveAttribute('aria-expanded', 'true');
});
