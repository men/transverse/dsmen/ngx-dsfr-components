import { Component, Input } from '@angular/core';
import { DsfrButtonModule } from '../../button';
import { DsfrButtonsGroupModule } from '../../buttons-group';
import { DsfrModalModule } from '../modal.module';

@Component({
  selector: 'demo-modal-actions-template',
  template: `
    <dsfr-button
      label="Ouvrir la modale"
      ariaControls="myModalWithActionsTemplate"
      data-testid="open-modal-btn"></dsfr-button>

    <dsfr-modal dialogId="myModalWithActionsTemplate" titleModal="My Title">
      <div>Le footer de cette modale est construit à partir d'un templateRef</div>
      <ng-template #modalFooterTemplate>
        <dsfr-buttons-group inline="LG" alignment="reverse" iconPosition="left" groupMarkup="div">
          <dsfr-button label="Valider" [disabled]="actionsDisabled"></dsfr-button>
          <dsfr-button label="Annuler" variant="secondary" [disabled]="actionsDisabled"></dsfr-button>
          <dsfr-button label="Autre action" variant="secondary" [disabled]="actionsDisabled"></dsfr-button>
        </dsfr-buttons-group>
      </ng-template>
    </dsfr-modal>
  `,
  standalone: true,
  imports: [DsfrModalModule, DsfrButtonModule, DsfrButtonsGroupModule],
})
export class DemoModalActionsTemplateComponent {
  @Input() actionsDisabled = false;
}
