import { dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrButtonModule } from '../../button';
import { DsfrButtonsGroupModule } from '../../buttons-group';
import { DsfrModalModule } from '../modal.module';
import { DemoModalActionsTemplateComponent } from './demo-modal4-actions-template.component';

const meta: Meta = {
  title: 'COMPONENTS/Modal',
  component: DemoModalActionsTemplateComponent,
  decorators: [moduleMetadata({ imports: [DsfrModalModule, DsfrButtonModule, DsfrButtonsGroupModule] })],
  parameters: {
    actions: { argTypesRegex: '^on.*' },
  },
};
export default meta;
type Story = StoryObj<DemoModalActionsTemplateComponent>;

export const ActionsTemplate: Story = {
  decorators: dsfrDecorator('Modale avec actions via template'),
  args: { actionsDisabled: false },
};
