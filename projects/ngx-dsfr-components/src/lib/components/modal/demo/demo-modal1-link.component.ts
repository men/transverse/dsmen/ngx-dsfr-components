import { Component, ViewChild } from '@angular/core';
import { DsfrLinkModule } from '../../link';
import { DsfrModalAction } from '../modal-action.model';
import { DsfrModalComponent } from '../modal.component';
import { DsfrModalModule } from '../modal.module';

@Component({
  selector: 'demo-modal-link',
  template: `
    <dsfr-modal #accountModal dialogId="accountModal" titleModal="Mon compte" [actions]="actions">
      Description de mon compte.
    </dsfr-modal>

    <dsfr-modal #logoutModal dialogId="logoutModal" titleModal="Confirmation" [actions]="actions">
      Se déconnecter de Scolarité Services.
    </dsfr-modal>

    <dsfr-link
      icon="fr-icon-account-line"
      iconPosition="left"
      size="SM"
      route="account"
      ariaControls="accountModal"
      (linkSelect)="onLinkSelect($event)">
      Mon compte
    </dsfr-link>

    <dsfr-link
      icon="fr-icon-logout-box-r-line"
      iconPosition="left"
      size="SM"
      route="logout"
      ariaControls="logoutModal"
      (linkSelect)="onLinkSelect($event)">
      Se déconnecter
    </dsfr-link>
  `,
  styles: ['dsfr-link {margin-right: 1rem}'],
  standalone: true,
  imports: [DsfrModalModule, DsfrLinkModule],
})
export class DemoModalLinkComponent {
  /** @internal */ @ViewChild('accountModal') accountModal!: DsfrModalComponent;
  /** @internal */ @ViewChild('logoutModal') logoutModal!: DsfrModalComponent;
  /** @internal */ actions: DsfrModalAction[] = [
    { label: 'Valider', callback: () => {} },
    { label: 'Annuler', callback: () => {}, variant: 'secondary' },
  ];

  /** @internal */
  onLinkSelect(route: string) {
    if (route === 'account') {
      // this.accountModal?.open(); // inutile avec DSFR 1.10
    } else if (route === 'logout') {
      // this.logoutModal?.open(); // inutile avec DSFR 1.10
    }
  }
}
