import { dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrModalModule } from '../modal.module';
import { DemoModalLinkComponent } from './demo-modal1-link.component';

const meta: Meta = {
  title: 'COMPONENTS/Modal',
  component: DemoModalLinkComponent,
  decorators: [moduleMetadata({ imports: [DsfrModalModule] })],
};
export default meta;
type Story = StoryObj<DemoModalLinkComponent>;

export const OpenFromLink: Story = {
  name: 'Open from link',
  decorators: dsfrDecorator(
    'Ouverture avec un lien',
    "Les recommandations sur l'accessibilité préconisent l'utilisation d'un bouton à la place d'un lien",
  ),
  args: {},
};
