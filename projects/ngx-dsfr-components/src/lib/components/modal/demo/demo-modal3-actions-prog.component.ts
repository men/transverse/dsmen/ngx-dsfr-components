import { Component, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrFormRadioModule, DsfrRadio } from '../../../forms';
import { DsfrButtonModule } from '../../button';
import { DsfrButtonsGroupModule } from '../../buttons-group';
import { DsfrModalAction } from '../modal-action.model';
import { DsfrModalModule } from '../modal.module';

@Component({
  selector: 'demo-modal-actions',
  template: `
    <dsfr-button label="Ouvrir la modale" ariaControls="modal-actions" data-testid="open-modal-btn"></dsfr-button>

    <dsfr-modal dialogId="modal-actions" titleModal="Titre de la modale" [actions]="actions">
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua. Vitae sapien
        pellentesque habitant morbi tristique senectus et.
      </p>
      <dsfr-form-radio name="'test'" [options]="options2" [(ngModel)]="value"></dsfr-form-radio>
      <dsfr-button label="Bouton "></dsfr-button>
    </dsfr-modal>
  `,
  standalone: true,
  imports: [DsfrModalModule, DsfrButtonModule, FormsModule, DsfrButtonsGroupModule, DsfrFormRadioModule],
})
export class DemoModalActionsComponent {
  /**
   * @internal
   */
  public actions: DsfrModalAction[] = [
    { label: 'Callback bouton', icon: 'fr-icon-checkbox-circle-line', callback: () => alert('Action') },
    {
      label: 'Reset',
      callback: () => alert('Reset'),
      variant: 'secondary',
      tooltipMessage: 'My tooltip',
      type: 'reset',
    },
  ];

  options2: DsfrRadio[] = [
    { value: '1', label: 'Radio 1' },
    { value: '2', label: 'Radio 2' },
  ];

  private _actionsDisabled = false;

  public get actionsDisabled(): boolean {
    return this._actionsDisabled;
  }

  @Input()
  public set actionsDisabled(value: boolean) {
    this._actionsDisabled = value;
    this.actions.forEach((a) => (a.disabled = this._actionsDisabled));
  }
}
