import { dsfrDecorator } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFormRadioModule } from '../../../forms';
import { DsfrButtonModule } from '../../button';
import { DsfrModalModule } from '../modal.module';
import { DemoModalActionsComponent } from './demo-modal3-actions-prog.component';

const meta: Meta = {
  title: 'COMPONENTS/Modal',
  component: DemoModalActionsComponent,
  decorators: [moduleMetadata({ imports: [DsfrModalModule, DsfrButtonModule, DsfrFormRadioModule, FormsModule] })],
  parameters: {
    actions: { argTypesRegex: '^on.*' },
  },
};
export default meta;
type Story = StoryObj<DemoModalActionsComponent>;

export const Actions: Story = {
  decorators: dsfrDecorator('Modale avec actions'),
  args: { actionsDisabled: false },
};
