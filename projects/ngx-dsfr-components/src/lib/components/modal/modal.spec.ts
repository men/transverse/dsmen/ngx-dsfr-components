import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { Component, Input, ViewChild } from '@angular/core';
import { DsfrModalAction } from './modal-action.model';
import { DsfrModalComponent } from './modal.component';
import { DsfrModalModule } from './modal.module';

require('../../../../../../node_modules/@gouvfr/dsfr/dist/dsfr.module.js');
declare function dsfr(elem: HTMLElement): any;

@Component({
  selector: `edu-host-component`,
  template: `
    <button type="button" id="control" class="fr-btn" data-fr-opened="false" aria-controls="test">Open modal</button>
    <dsfr-modal dialogId="test" [actions]="actions"></dsfr-modal>
  `,
})
class TestHostComponent {
  @ViewChild(DsfrModalComponent)
  public modalComponent: DsfrModalComponent;
  @Input() actions: DsfrModalAction[] = [
    { label: 'Label bouton', icon: 'fr-icon-checkbox-circle-line', callback: () => console.log('Action') },
    {
      label: 'Reset',
      callback: () => console.log('Action'),
      variant: 'secondary',
      tooltipMessage: 'My tooltip',
      type: 'reset',
      disabled: true,
    },
  ];
}

describe('DsfrModalComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrModalModule],
      declarations: [TestHostComponent],
    }).compileComponents();

    window.scrollTo = jest.fn();
    //@ts-ignore
    dsfr.stop();
    //@ts-ignore
    dsfr.mode = 'manual';
    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should put id test ', () => {
    testHostComponent.modalComponent.ngOnInit();
    fixture.detectChanges();
    const el = fixture.nativeElement.querySelector('dialog');
    expect(testHostComponent.modalComponent.controlMode).toEqual('button');
    expect(el.getAttribute('id')).toEqual('test');
    expect(el.getAttribute('aria-labelledby')).toBeDefined();
    expect(el.querySelector('button.fr-btn--close').getAttribute('aria-controls')).toEqual('test');
  });

  it('should defined title default h1', () => {
    testHostComponent.modalComponent.titleModal = 'Titre';
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('h1').textContent).toContain('Titre');
  });

  it('should defined title h2', () => {
    testHostComponent.modalComponent.titleModal = 'Titre';
    testHostComponent.modalComponent.headingLevel = 'H2';
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('h2').textContent).toContain('Titre');
  });

  it('should open modal programmatically', fakeAsync(() => {
    //@ts-ignore
    dsfr.start();
    testHostComponent.modalComponent.controlMode = 'dynamic';
    testHostComponent.modalComponent.ngOnInit();
    testHostComponent.modalComponent.ngAfterViewInit();
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('dialog').className).toContain('fr-modal');
    testHostComponent.modalComponent.open();
    tick(100);
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelector('dialog').className).toContain('fr-modal--opened');
    const btnCloseFocused = fixture.nativeElement.querySelector('.fr-btn--close' /*:focus*/);
    // expect(btnCloseFocused).toBeTruthy();
    const eltFocus = fixture.nativeElement.querySelector(':focus');
    expect(eltFocus).toBe(btnCloseFocused);
    //@ts-ignore
    dsfr.stop();
  }));

  it('should add actions buttons', fakeAsync(() => {
    fixture.detectChanges();
    const onClickMock = jest.spyOn(testHostComponent.modalComponent, 'performAction');
    const buttons = fixture.nativeElement.querySelector('.fr-btns-group').children;
    expect(buttons.length).toEqual(2);
    buttons[1].click();
    tick();
    expect(onClickMock).toHaveBeenCalled();
  }));
});
