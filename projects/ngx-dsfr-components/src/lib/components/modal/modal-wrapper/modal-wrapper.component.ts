import { Component, ContentChild, ViewEncapsulation } from '@angular/core';
import { DsfrModalComponent } from '../modal.component';

@Component({
  selector: 'edu-modal-wrapper',
  templateUrl: './modal-wrapper.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class ModalWrapperComponent {
  @ContentChild('dialog5') dialog: DsfrModalComponent;

  performOpenModal() {
    this.dialog.open();
    setTimeout(() => {
      this.dialog.close();
    }, 3000);
  }
}
