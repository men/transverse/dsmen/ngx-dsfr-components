import { expect, FrameLocator, Locator, test } from '@playwright/test';
import { getSbFrame, goToComponentPage } from '../../shared/utils/e2e.utils';

test.describe('Modal', () => {
  test.describe('close button', () => {
    test('must receive focus as soon as the modal is displayed', async ({ page }) => {
      const frame = getSbFrame(page);
      const dialog = getDialog(frame);
      const openBtn = await getOpenModalButton(frame);

      await goToComponentPage('modal', page);
      await expect(dialog).toHaveClass('fr-modal');
      await expect(dialog).toBeHidden();
      await expect(openBtn).toHaveAttribute('aria-controls', 'modalDefault');
      await expect(openBtn).toHaveAttribute('data-fr-js-modal-button', 'true');
      await openBtn.press('Enter');
      await expect(dialog).toBeVisible();
      await expect(dialog).toHaveClass(/fr-modal--opened/);
      await expect(frame.locator('button.fr-btn--close')).toBeFocused();
    });

    test('must receive focus as soon as the modal is programmatically opened', async ({ page }) => {
      const frame = getSbFrame(page);
      const dialog = getDialog(frame);
      const openBtn = frame.locator('dsfr-button > button');

      await goToComponentPage('modal', page, 'programmatic');

      await expect(dialog).toHaveClass('fr-modal');
      await expect(dialog).toBeHidden();
      await expect(openBtn).toBeVisible();
      await expect(openBtn).not.toHaveAttribute('data-fr-js-modal-button');

      await openBtn.press('Enter');

      await expect(dialog).toBeVisible();
      await expect(dialog).toHaveClass(/fr-modal--opened/);
      await expect(frame.locator('button.fr-btn--close')).toBeFocused();
    });

    test('must be the only way to close the modal when in concealing backdrop mode', async ({ page }) => {
      const frame = getSbFrame(page);
      const dialog = getDialog(frame);
      const openBtn = await getOpenModalButton(frame);

      await goToComponentPage('modal', page, 'concealing-backdrop');
      await expect(dialog).toBeHidden();

      await openBtn.press('Enter');

      await expect(dialog).toBeVisible();

      await dialog.click();

      await expect(dialog).toBeVisible();
      await getCloseButton(frame).click();
      await expect(dialog).not.toBeVisible();
    });
  });
  test.describe('actions footer', () => {
    test('should not be present', async ({ page }) => {
      const frame = getSbFrame(page);
      const dialog = getDialog(frame);
      const openBtn = await getOpenModalButton(frame);
      const btnsGroup = dialog.locator('.fr-modal__footer');

      await goToComponentPage('modal', page);
      await expect(dialog).toBeHidden();

      await openBtn.press('Enter');

      await expect(dialog).toBeVisible();
      await expect(btnsGroup).not.toBeVisible();
    });
    test('must be defined programmatically', async ({ page }) => {
      const frame = getSbFrame(page);
      const dialog = getDialog(frame);
      const openBtn = frame.getByTestId('open-modal-btn').locator('button[aria-controls="modal-actions"]');
      const btnsGroup = dialog.locator('.fr-btns-group');

      await goToComponentPage('modal', page, 'actions');
      await expect(dialog).toBeHidden();

      await openBtn.press('Enter');

      await expect(dialog).toBeVisible();
      await expect(btnsGroup).toBeVisible();
      await expect(btnsGroup.locator('dsfr-button')).toHaveCount(2);
    });
    test('must be defined using template', async ({ page }) => {
      const frame = getSbFrame(page);
      const dialog = getDialog(frame);
      const openBtn = frame.getByTestId('open-modal-btn').locator('button[aria-controls="myModalWithActionsTemplate"]');
      const btnsGroup = dialog.locator('.fr-btns-group');

      await goToComponentPage('modal', page, 'actions-template');
      await expect(dialog).toBeHidden();

      await openBtn.press('Enter');

      await expect(dialog).toBeVisible();
      await expect(btnsGroup).toBeVisible();
      await expect(btnsGroup.locator('dsfr-button')).toHaveCount(3);
    });
  });
});

function getDialog(frame: FrameLocator): Locator {
  return frame.locator('dialog');
}

async function getOpenModalButton(frame: FrameLocator): Promise<Locator> {
  return frame.getByTestId('open-modal-btn').locator('> button');
}

function getCloseButton(frame: FrameLocator): Locator {
  return frame.getByTestId('modal-btn-close');
}
