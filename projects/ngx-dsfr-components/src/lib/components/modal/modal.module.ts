import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrButtonModule } from '../button';
import { DsfrModalComponent } from './modal.component';

@NgModule({
  declarations: [DsfrModalComponent],
  exports: [DsfrModalComponent],
  imports: [CommonModule, DsfrButtonModule, DsfrI18nPipe],
})
export class DsfrModalModule {}
