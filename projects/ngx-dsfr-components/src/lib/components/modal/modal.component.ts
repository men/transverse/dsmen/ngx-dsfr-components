import {
  AfterViewInit,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  TemplateRef,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { DsfrHeadingLevel, DsfrSize, DsfrSizeConst, newUniqueId } from '../../shared';
import { DsfrModalAction } from './modal-action.model';

declare function dsfr(elem: HTMLElement): any;

@Component({
  selector: 'dsfr-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DsfrModalComponent implements AfterViewInit, OnInit, OnDestroy {
  /** L'identifiant unique de l'élément <dialog>. */
  @Input() dialogId: string;

  /** Titre de la modal. */
  // TODO à renommer heading (en dépréciant)
  @Input() titleModal: string;

  /**
   * Le niveau de titre devant être utilisé (tag `h1` par défaut).
   * Cette balise ne produit pas de style, mais de la structure.
   */
  @Input() headingLevel: DsfrHeadingLevel | undefined;

  /** Le tableau des actions à positionner dans le dialogue modal. */
  // TODO Ne faudrait-il pas homogénéiser avec les tuiles et les cartes ?
  @Input() actions: DsfrModalAction[] = [];

  /** Permet de ne pas fermer la modale automatiquement lorsqu'une action est exécutée. */
  @Input() autoCloseOnAction = true;

  /** Autorise la fermeture de la modale au click sur le fond. */
  @Input() concealingBackdrop = true;

  /**
   * Le mode de contrôle vous permet, le cas échéant,  de maintenir la capacité de la modale à être pilotée
   * programmatiquement en absence de bouton de contrôle.
   *
   * @since 1.8.0
   */
  @Input() controlMode: 'button' | 'dynamic' = 'button';

  /** Signale l'ouverture de la modal. */
  @Output() disclose = new EventEmitter<void>();

  /** Signale la fermeture de la modal. */
  @Output() conceal = new EventEmitter<void>();

  /** @internal */
  @ViewChild('dsfrModal') dsfrModal: ElementRef;

  /** Template du pied de la modale (doit contenir des boutons d'actions) */
  @ContentChild('modalFooterTemplate', { static: true }) modalFooterTemplate: TemplateRef<any>;

  /**
   * L'identifiant unique de l'élément tenant titre descriptif du dialogue modal.
   *
   * @internal
   */
  titleElementId: string;

  /** @internal */
  sizeClasses: { [klass: string]: boolean };

  private _size: DsfrSize = DsfrSizeConst.MD;

  private _unlisten: { (): void }[] = [];

  /** @internal */
  constructor(private renderer2: Renderer2) {
    this.updateSizeClasses();
  }

  get size(): DsfrSize {
    return this._size;
  }

  /**
   * La taille de la modale.
   * Valeurs possibles : `SM` (pour small), `MD` (pour medium), `LG` (pour large).
   */
  @Input() set size(newSize: DsfrSize) {
    this._size = newSize;
    this.updateSizeClasses();
  }

  ngOnInit() {
    this.dialogId ??= newUniqueId();
    this.titleElementId = newUniqueId();
  }

  ngAfterViewInit() {
    this._unlisten.push(
      this.renderer2.listen(this.dsfrModal.nativeElement, 'dsfr.conceal', () => {
        const dialog: HTMLElement = this.dsfrModal.nativeElement;
        if (Boolean(dialog.getAttribute('open'))) {
          this.conceal.emit();
        }
      }),
      this.renderer2.listen(this.dsfrModal.nativeElement, 'dsfr.disclose', () => {
        this.disclose.emit();
      }),
    );
  }

  ngOnDestroy() {
    this._unlisten.forEach((unlistenFunc) => unlistenFunc());
  }

  /**
   * Permet de déclencher programmatiquement l'ouverture de la modale.
   */
  open() {
    dsfr(this.dsfrModal.nativeElement).modal.disclose();
  }

  /**
   * Permet de déclencher programmatiquement la fermeture de la modale.
   */
  close() {
    dsfr(this.dsfrModal.nativeElement).modal.conceal();
  }

  /** @internal */
  performAction(action: DsfrModalAction): void {
    if (action.callback) {
      action.callback();
    }
  }

  private updateSizeClasses() {
    this.sizeClasses = {
      'fr-col-12': true,
      'fr-col-md-4': this.size === DsfrSizeConst.SM,
      'fr-col-md-8 fr-col-lg-6': this.size === DsfrSizeConst.MD,
      'fr-col-md-8': this.size === DsfrSizeConst.LG,
    };
  }
}
