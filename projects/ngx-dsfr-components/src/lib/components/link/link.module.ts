import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DsfrLinkComponent } from './link.component';

@NgModule({
  declarations: [DsfrLinkComponent],
  exports: [DsfrLinkComponent],
  imports: [CommonModule, RouterModule],
})
export class DsfrLinkModule {}
