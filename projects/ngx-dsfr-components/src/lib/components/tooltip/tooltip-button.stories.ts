import { dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrTooltipButtonComponent } from './tooltip-button.component';

const meta: Meta = {
  title: 'COMPONENTS/Tooltip/Tooltip-Button',
  component: DsfrTooltipButtonComponent,
  decorators: [moduleMetadata({ imports: [] })],
};
export default meta;
type Story = StoryObj<DsfrTooltipButtonComponent>;

const loremIpsum = `Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua.`;

export const Default: Story = {
  decorators: dsfrDecorator('Ouverture du tooltip au clic'),
  args: {
    tooltip: loremIpsum,
  },
};
