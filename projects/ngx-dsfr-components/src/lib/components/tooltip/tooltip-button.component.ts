import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrI18nService, newUniqueId } from '../../shared';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';

@Component({
  selector: 'dsfr-tooltip-button',
  templateUrl: './tooltip-button.component.html',
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [DsfrI18nPipe],
})
export class DsfrTooltipButtonComponent {
  /**
   * La valeur du tooltip.
   */
  @Input() tooltip: string;

  /** @internal */
  tooltipId = newUniqueId();

  constructor(/** @internal */ public i18n: DsfrI18nService) {}
}
