import { Component, ElementRef, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { DsfrHeadingLevel } from '../../shared';
import { DsfrNoticeType, DsfrNoticeTypeConst } from './notice.model';

@Component({
  selector: 'dsfr-notice',
  templateUrl: './notice.component.html',
  styleUrls: ['./notice.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DsfrNoticeComponent {
  /**
   * Le message qui sera affiché dans le bandeau.
   * Renseigner le titre via l'input est prioritaire sur le slot.
   */
  @Input() heading: string;

  /**
   * Le niveau du titre, `<span>` par défaut.
   */
  @Input() headingLevel: DsfrHeadingLevel;

  /**
   * Permet d'afficher le bouton servant à fermer le bandeau d'information.<br>
   * La valeur 'controlled' permet d'afficher le bouton de fermeture, mais c'est vous qui devez contrôler l'action de
   * fermeture du bandeau.
   */
  @Input() closeable: boolean | 'controlled' = false;

  /**
   * @since 1.12 Permet d'enlever l'icône du bandeau d'information
   */
  @Input() noIcon = false;

  /**
   * @since 1.12 Permet de personnaliser l'icône du bandeau. Html possible.
   */
  @Input() icon: string;

  /**
   * @since 1.12 Texte complémentaire au titre
   */
  @Input() description: string;

  /**
   * @since 1.12 Sévérité du message
   */
  @Input() type: DsfrNoticeType = DsfrNoticeTypeConst.INFO;

  /**
   * Signale la fermeture manuelle du bandeau d'information.
   */
  @Output() readonly noticeClose = new EventEmitter<ElementRef>();

  constructor(private hostElement: ElementRef) {}

  get iconClass(): string {
    return this.icon ?? '';
  }

  /** @Deprecated since 1.12 use `closeable`instead. */
  get closable(): boolean | 'controlled' {
    return this.closeable;
  }

  /** @Deprecated since 1.12 use `heading` instead. */
  get message(): string {
    return this.heading;
  }

  /** @Deprecated since 1.12 use `closeable`instead. */
  @Input() set closable(value: boolean | 'controlled') {
    this.closeable = value;
  }

  /**
   * Le message qui sera affiché dans le bandeau.
   * Renseigner le message via l'input est prioritaire sur le slot.
   * @Deprecated since 1.12 use `heading` instead.
   */
  @Input() set message(value: string) {
    this.heading = value;
  }

  /** @internal */
  onClose() {
    if (this.closeable && this.closeable !== 'controlled') {
      this.hostElement.nativeElement.parentNode?.removeChild(this.hostElement.nativeElement);
    }
    this.noticeClose.emit(this.hostElement);
  }
}
