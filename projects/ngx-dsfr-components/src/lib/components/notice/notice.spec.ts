import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DsfrHeadingLevelConst } from '../../shared';
import { DsfrNoticeComponent } from './notice.component';
import { DsfrNoticeModule } from './notice.module';

describe('DsfrNoticeComponent', () => {
  let noticeComponent: DsfrNoticeComponent;
  let fixture: ComponentFixture<DsfrNoticeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrNoticeModule],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DsfrNoticeComponent);
    noticeComponent = fixture.componentInstance;
    // Valeurs par défaut
    noticeComponent.heading = 'Titre du bandeau';
    // Détection du changement
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(noticeComponent).not.toBeNull();
  });

  it('should have <span> title !', () => {
    const titleElt = fixture.nativeElement.querySelector('.fr-notice__title');
    expect(titleElt instanceof HTMLSpanElement).toBeTruthy();
  });

  it('should have <h3> title !', () => {
    noticeComponent.headingLevel = DsfrHeadingLevelConst.H3;
    fixture.detectChanges();
    const titleElt = fixture.nativeElement.querySelector('.fr-notice__title');
    expect(titleElt).not.toBeNull();
    expect(titleElt.tagName).toEqual('H3');
  });
});
