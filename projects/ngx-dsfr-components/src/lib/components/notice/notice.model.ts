/**
 * Types de notice sous forme énumérée
 */
export namespace DsfrNoticeTypeConst {
  export const INFO = 'info';
  export const WARNING = 'warning';
  export const ALERT = 'alert';
  export const WEATHER_ORANGE = 'weather-orange';
  export const WEATHER_RED = 'weather-red';
  export const WEATHER_PURPLE = 'weather-purple';
  export const ATTACK = 'attack';
  export const WITNESS = 'witness';
  export const CYBERATTACK = 'cyberattack';
}

type Type = typeof DsfrNoticeTypeConst;
export type DsfrNoticeType = Type[keyof Type];
