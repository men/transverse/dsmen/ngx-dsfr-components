import { argEventEmitter, dsfrDecorator, optionsHeadingLevel } from '.storybook/storybook-utils';
import { ElementRef } from '@angular/core';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrLinkModule } from '../link';
import { DsfrNoticeComponent } from './notice.component';
import { DsfrNoticeTypeConst } from './notice.model';

const meta: Meta = {
  title: 'COMPONENTS/Notice',
  component: DsfrNoticeComponent,
  decorators: [moduleMetadata({ imports: [DsfrLinkModule, DsfrI18nPipe] })],
  argTypes: {
    closeable: { control: 'inline-radio', options: [false, true, 'controlled'] },
    closable: { table: { disable: true } },
    headingLevel: { control: 'inline-radio', options: optionsHeadingLevel },
    type: { control: 'select', options: Object.values(DsfrNoticeTypeConst) },
    noticeClose: { control: { control: argEventEmitter } },
    message: { table: { disable: true } },
    myCloseHandler: {
      control: { type: 'object' },
      table: { disable: true },
    },
  },
};
export default meta;
type Story = StoryObj<DsfrNoticeComponent>;

// -- Contenu ----------------------------------------------------------------------------------------------------------

export const Default: Story = {
  decorators: dsfrDecorator("Bandeau d'information minimum"),
  args: {
    heading: 'Titre du bandeau',
    headingLevel: undefined,
    closeable: false,
    noIcon: false,
    icon: '',
    description: '',
    type: DsfrNoticeTypeConst.INFO,
  },
};

export const Closeable: Story = {
  decorators: dsfrDecorator("Bandeau d'information avec bouton fermer"),
  args: {
    ...Default.args,
    closeable: true,
  },
};

export const NoIcon: Story = {
  decorators: dsfrDecorator("Bandeau d'information sans icône"),
  args: {
    ...Closeable.args,
    noIcon: true,
  },
};

export const CustomIcon: Story = {
  decorators: dsfrDecorator("Bandeau d'information avec icône personnalisée"),
  args: {
    ...Closeable.args,
    icon: 'fr-icon-virus-fill',
  },
};

export const Description: Story = {
  decorators: dsfrDecorator("Bandeau d'information cas max"),
  args: {
    ...Closeable.args,
    heading: 'Titre du bandeau assez long',
    description: 'Texte de description plutot long lorem ipsum sit consectetur adipiscing elit. Sed',
  },
};

// -- Bandeaux génériques ----------------------------------------------------------------------------------------------

export const TypeWarning: Story = {
  decorators: dsfrDecorator("Bandeau d'avertissement"),
  args: {
    ...Closeable.args,
    heading: "Titre du bandeau d'avertissement",
    description: 'Texte de description',
    type: DsfrNoticeTypeConst.WARNING,
  },
};

export const TypeAlert: Story = {
  decorators: dsfrDecorator("Bandeau d'alerte"),
  args: {
    ...Closeable.args,
    heading: "Titre du bandeau d'avertissement",
    description: 'Texte de description',
    type: DsfrNoticeTypeConst.ALERT,
  },
};

// -- Bandeaux vigilance météo -----------------------------------------------------------------------------------------

export const WeatherOrange: Story = {
  decorators: dsfrDecorator('Vigilance météo orange'),
  args: {
    ...Closeable.args,
    heading: 'Vigilance météo orange',
    description: 'Texte de description',
    type: DsfrNoticeTypeConst.WEATHER_ORANGE,
  },
};

export const WeatherRed: Story = {
  decorators: dsfrDecorator('Vigilance météo rouge'),
  args: {
    ...Closeable.args,
    heading: 'Vigilance météo rouge',
    description: 'Texte de description',
    type: DsfrNoticeTypeConst.WEATHER_RED,
  },
};

export const WeatherPurple: Story = {
  decorators: dsfrDecorator('Vigilance météo violette'),
  args: {
    ...Closeable.args,
    heading: 'Vigilance météo violette',
    description: 'Texte de description',
    type: DsfrNoticeTypeConst.WEATHER_PURPLE,
  },
};

// -- Bandeaux d'alertes -----------------------------------------------------------------------------------------------

export const Attack: Story = {
  decorators: dsfrDecorator('Alerte attentat'),
  args: {
    ...Closeable.args,
    heading: 'Attentat en cours',
    description: 'Texte de description',
    type: DsfrNoticeTypeConst.ATTACK,
  },
};

export const Witness: Story = {
  decorators: dsfrDecorator('Appel à témoins'),
  args: {
    ...Closeable.args,
    heading: 'Appel à témoins',
    description: 'Texte de description',
    type: DsfrNoticeTypeConst.WITNESS,
  },
};

export const Cyberattack: Story = {
  decorators: dsfrDecorator('Alerte technologique'),
  args: {
    ...Closeable.args,
    heading: 'Cyber-attaque',
    description: 'Texte de description',
    type: DsfrNoticeTypeConst.CYBERATTACK,
  },
};

// -- Autres -----------------------------------------------------------------------------------------------------------

export const SlotTitle: Story = {
  decorators: dsfrDecorator("Bandeau d'information minimum", 'Titre projeté dans le slot par défaut'),
  args: {
    ...Default.args,
    heading: '',
  },
  render: (args) => ({
    props: args,
    template: ` 
    <dsfr-notice [heading]="heading" [closeable]="closeable" [noIcon]="[noIcon]" [icon]="icon" 
  [description]="description" [type]="type">Titre du bandeau</dsfr-notice>`,
  }),
};

export const SlotLink: Story = {
  decorators: dsfrDecorator("Bandeau d'information avec lien", "Lien projeté dans le slot 'link'"),
  args: {
    ...Default.args,
    description: 'Texte de description.',
    heading: 'Titre du bandeau',
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-notice [heading]="heading" [closeable]="closeable" [noIcon]="[noIcon]" [icon]="icon" 
  [description]="description" [type]="type">
    <dsfr-link #link link="#" customClass="fr-notice__link" label="Lien de consultation" target="_blank"></dsfr-link>
</dsfr-notice>`,
  }),
};

function myCloseHandler(comp: ElementRef) {
  const parent = comp.nativeElement.parentNode;
  parent.removeChild(comp.nativeElement);
  const p = document.createElement('p');
  p.innerHTML = '<i>Close action has been controlled by userdev</i>';
  parent.appendChild(p);
}

// fixme ?
export const CloseControlled: StoryObj<DsfrNoticeComponent> = {
  decorators: dsfrDecorator('Fermeture contrôlée par le développeur'),
  args: {
    ...Default.args,
    closeable: 'controlled',
  },
  render: (args) => ({
    props: {
      ...args,
      myCloseHandler: myCloseHandler,
    },
    template: `<dsfr-notice [heading]="heading" [closeable]="closeable" [noIcon]="[noIcon]" [icon]="icon" [description]="description" 
[type]="type" (noticeClose)="myCloseHandler($event)"></dsfr-notice>`,
  }),
};
