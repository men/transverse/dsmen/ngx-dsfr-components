import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrNoticeComponent } from './notice.component';

@NgModule({
  declarations: [DsfrNoticeComponent],
  exports: [DsfrNoticeComponent],
  imports: [CommonModule, DsfrI18nPipe],
})
export class DsfrNoticeModule {}
