import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrButtonComponent } from './button.component';

@NgModule({
  imports: [CommonModule, FormsModule],
  declarations: [DsfrButtonComponent],
  exports: [DsfrButtonComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class DsfrButtonModule {}
