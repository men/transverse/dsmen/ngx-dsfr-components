import { dsfrDecorator, optionsHeadingLevel } from '.storybook/storybook-utils';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrHeadingLevelConst } from '../../shared';
import { DsfrButtonModule } from '../button';
import { DsfrCalloutComponent } from './callout.component';

const meta: Meta = {
  title: 'COMPONENTS/Callout',
  component: DsfrCalloutComponent,
  decorators: [moduleMetadata({ imports: [DsfrButtonModule] })],
  argTypes: {
    headingLevel: { control: 'inline-radio', options: optionsHeadingLevel },
  },
};
export default meta;
type Story = StoryObj<DsfrCalloutComponent>;

const template = `
    <dsfr-callout
      [heading] = "heading"
      [headingLevel] = "headingLevel"
      [icon] = "icon"
      [customClass] = "customClass"
    >
      Les parents d’enfants de 11 à 14 ans n’ont aucune démarche à accomplir : les CAF versent automatiquement
      l’ARS aux familles déjà allocataires qui remplissent les conditions.
    </dsfr-callout>`;

const templateButton = `
    <dsfr-callout
      [heading] = "heading"
      [headingLevel] = "headingLevel"
      [icon] = "icon"
      [customClass] = "customClass"
    >
      <p> Les parents d’enfants de 11 à 14 ans n’ont aucune démarche à accomplir : les CAF versent automatiquement
      l’ARS aux familles déjà allocataires qui remplissent les conditions. </p>
      <dsfr-button label="Valider"></dsfr-button>
    </dsfr-callout>`;

export const Default: Story = {
  decorators: dsfrDecorator('Mise en avant avec texte seul'),
  render: (args) => ({
    props: args,
    template: template,
  }),
};

export const TitleAndText: Story = {
  decorators: dsfrDecorator('Mise en avant avec titre et texte'),
  args: {
    heading: 'Titre mise en avant',
  },
  render: (args) => ({
    props: args,
    template: template,
  }),
};

export const TitleButtonAndText: Story = {
  decorators: dsfrDecorator('Mise en avant avec titre, bouton, icône et texte'),
  args: {
    heading: 'Titre mise en avant',
  },
  render: (args) => ({
    props: args,
    template: templateButton,
  }),
};

export const TitleButtonIconAndText: Story = {
  args: {
    heading: 'Titre mise en avant',
    icon: 'fr-icon-information-line',
  },
  render: (args) => ({
    props: args,
    template: templateButton,
  }),
};

export const CustomTheme: Story = {
  decorators: dsfrDecorator('Mise en avant accentuée'),
  args: {
    heading: 'Titre mise en avant',
    customClass: 'fr-callout--green-emeraude',
  },
  render: (args) => ({
    props: args,
    template: template,
  }),
};

export const CustomHeadingLevel: Story = {
  decorators: dsfrDecorator('Mise en avant avec titre de niveau h2'),
  args: {
    heading: 'Titre mise en avant',
    headingLevel: DsfrHeadingLevelConst.H2,
  },
  render: (args) => ({
    props: args,
    template: template,
  }),
};
