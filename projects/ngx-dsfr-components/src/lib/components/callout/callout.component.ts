import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrHeadingLevel, DsfrHeadingLevelConst } from '../../shared';

@Component({
  selector: 'dsfr-callout',
  templateUrl: './callout.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrCalloutComponent {
  /**
   * Titre du call out. Si inexistant, pas de balise titre.
   */
  @Input() heading: string;

  /**
   * Le niveau de titre devant être utilisé.
   * Valeurs supportées : H2, H3, H4, H5, H6 ou P si vous ne souhaitez pas positionner un titre de section.
   */
  @Input() headingLevel: DsfrHeadingLevel = DsfrHeadingLevelConst.H3;

  /**
   * Indique la classe d'icône à utiliser en haut à gauche. Ex. `fr-icon-information-line`.
   */
  @Input() icon: string;

  /**
   * Permet de personnaliser la couleur du tag. Il faut donner la class exact (ex : fr-callout--green-emeraude) que vous trouverez
   * [ici](https://gouvfr.atlassian.net/wiki/spaces/DB/pages/222331196/Mise+en+avant+-+Call+out#Personnalisation).
   */
  @Input() customClass: string;

  /**
   * Détermine la liste des classes CSS à positioner sur le parent.
   * @returns un tableau de classes CSS
   *
   * @internal
   */
  getClasses(): string[] {
    let results: string[] = ['fr-callout'];
    if (this.icon) {
      results = results.concat(this.icon.trim().split(/\s+/));
    }
    if (this.customClass) {
      results = results.concat(this.customClass.trim().split(/\s+/));
    }

    return results;
  }
}
