import {
  argEventEmitter,
  bgDecorator,
  gridDecoratorLG,
  optionsHeadingLevel,
  titleDecorator,
} from '.storybook/storybook-utils';
import { componentWrapperDecorator, Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFileSizeUnitConst, DsfrLinkTargetConst, SvgIconModule } from '../../shared';
import { DsfrBadge } from '../badge';
import { DsfrBadgesGroupModule } from '../badges-group';
import { DsfrButtonModule } from '../button';
import { DsfrButtonsGroupModule } from '../buttons-group';
import { DsfrLinkModule } from '../link';
import { DsfrTag } from '../tag';
import { DsfrTagsGroupModule } from '../tags-group';
import { DEFAULT_HEADING_LEVEL } from './base-panel.component';
import { DsfrPanelBackgroundConst, DsfrPanelBorderConst } from './base-panel.model';
import { DsfrCardComponent } from './card.component';
import { DsfrImageFitConst, DsfrImageRatioConst, DsfrImageTypeConst } from './card.model';

const meta: Meta = {
  title: 'COMPONENTS/Card',
  component: DsfrCardComponent,
  decorators: [
    moduleMetadata({
      imports: [
        DsfrBadgesGroupModule,
        DsfrTagsGroupModule,
        SvgIconModule,
        DsfrLinkModule,
        DsfrButtonsGroupModule,
        DsfrButtonModule,
      ],
    }),
  ],
  argTypes: {
    customBackground: { control: { type: 'inline-radio' }, options: Object.values(DsfrPanelBackgroundConst) },
    customBorder: { control: { type: 'inline-radio' }, options: Object.values(DsfrPanelBorderConst) },
    downloadAssessFile: { control: { type: 'inline-radio' }, options: [false, true, 'bytes'] },
    downloadLangCode: { if: { arg: 'downloadAssessFile', neq: true } },
    downloadMimeType: { if: { arg: 'downloadAssessFile', neq: true } },
    downloadSizeBytes: { if: { arg: 'downloadAssessFile', neq: true } },
    downloadSizeUnit: { control: { type: 'inline-radio' }, options: Object.values(DsfrFileSizeUnitConst) },
    headingLevel: { control: 'inline-radio', options: optionsHeadingLevel },
    imageFit: { control: { type: 'inline-radio' }, options: Object.values(DsfrImageFitConst) },
    imageRatio: { control: { type: 'inline-radio' }, options: Object.values(DsfrImageRatioConst) },
    imageType: { control: { type: 'inline-radio' }, options: Object.values(DsfrImageTypeConst) },
    linkTarget: { control: { type: 'inline-radio' }, options: Object.values(DsfrLinkTargetConst) },
    size: { table: { disable: true } },
    cardSelect: { control: { control: argEventEmitter } },
  },
};
export default meta;
type Story = StoryObj<DsfrCardComponent>;

const heading = 'Intitulé de la carte (sur lequel se trouve le lien)';
const loremIpsum = `Lorem ipsum dolor sit amet, consectetur, incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et`;
const unBadge: DsfrBadge[] = [{ label: 'Libellé Badge' }];
const deuxBadges: DsfrBadge[] = [
  { label: 'Libellé Badge', customClass: 'fr-badge--purple-glycine' },
  { label: 'Libellé Badge', customClass: 'fr-badge--purple-glycine' },
];
const tags: DsfrTag[] = [{ label: 'Libellé tag' }, { label: 'Libellé tag' }];
const detail = 'detail (optionnel)';
const detailIcon = 'fr-icon-warning-fill';

const gridDecorator = componentWrapperDecorator(
  (story) =>
    `<div class="fr-mb-6v">
  <div class="fr-grid-row fr-grid-row--gutters">
      <div class="fr-col-12 fr-col-md-6 fr-col-lg-4">
      ${story}
    </div>
  </div>
</div>`,
);

// -- Tailles ----------------------------------------------------------------------------------------------------------

export const Default: Story = {
  decorators: [gridDecorator, titleDecorator('Carte verticale')],
  args: {
    // Valeurs par défaut
    badgesOnMedia: false,
    customBackground: 'default',
    customBorder: 'default',
    disabled: false,
    download: false,
    downloadDirect: true,
    downloadAssessFile: true,
    enlargeLink: true, // identique à la tuile
    hasFooter: false,
    headingLevel: DEFAULT_HEADING_LEVEL,
    horizontal: false,
    imageType: 'img',
    size: 'MD',
    noIcon: false,
    // Obligatoires
    heading: heading,
    // Pour la story
    link: '#',
    imagePath: 'img/placeholder.16x9.png',
  },
};

export const Small: Story = {
  decorators: [gridDecorator, titleDecorator('Carte taille SM')],
  args: {
    ...Default.args,
    size: 'SM',
  },
};

export const Large: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Carte taille LG')],
  args: {
    ...Default.args,
    size: 'LG',
  },
};

// -- Icônes -----------------------------------------------------------------------------------------------------------

export const NoIcon: Story = {
  decorators: [gridDecorator, titleDecorator('Carte sans icône')],
  args: {
    ...Default.args,
    noIcon: true,
  },
};

export const ExternalLink: Story = {
  decorators: [gridDecorator, titleDecorator('Carte avec icône lien externe')],
  args: {
    ...Default.args,
    linkTarget: '_blank',
    description: loremIpsum,
  },
};

// -- Variantes --------------------------------------------------------------------------------------------------------

export const GreyBackground: Story = {
  decorators: [gridDecorator, titleDecorator('Carte verticale variation accentuée en contrast grey')],
  args: {
    ...Default.args,
    customBackground: DsfrPanelBackgroundConst.GREY,
  },
};

export const Borderless: Story = {
  decorators: [gridDecorator, bgDecorator('lightyellow'), titleDecorator('Carte verticale variation sans bordure')],
  args: {
    ...Default.args,
    customBorder: DsfrPanelBorderConst.NO_BORDER,
  },
};

export const Transparent: Story = {
  decorators: [gridDecorator, bgDecorator('lightyellow'), titleDecorator('Carte verticale variation sans fond')],
  args: {
    ...Default.args,
    customBackground: DsfrPanelBackgroundConst.TRANSPARENT,
  },
};

export const Shadow: Story = {
  decorators: [gridDecorator, titleDecorator('Carte verticale variation ombre portée')],
  args: {
    ...Default.args,
    customBorder: DsfrPanelBorderConst.SHADOW,
  },
};

// -- Markup button ----------------------------------------------------------------------------------------------------

export const MarkupButton: Story = {
  decorators: [gridDecorator, titleDecorator('Carte avec bouton')],
  args: {
    ...Default.args,
    enlargeButton: true,
  },
};

// -- Sans image -------------------------------------------------------------------------------------------------------

export const NoImage: Story = {
  decorators: [gridDecorator, titleDecorator('Carte sans image')],
  args: {
    ...Default.args,
    imagePath: '',
  },
};

// -- Image et ratio ---------------------------------------------------------------------------------------------------

export const ImageRatio: Story = {
  decorators: [gridDecorator, titleDecorator("Carte verticale avec image au ratio d'aspect 32x9")],
  args: {
    ...Default.args,
    imageRatio: '16:9/2',
  },
};

export const ImageSVG: Story = {
  decorators: [gridDecorator, titleDecorator('Carte avec image svg')],
  args: {
    ...Default.args,
    imagePath: 'icon/sprites.svg#userLine',
    imageType: 'svg',
    imageRatio: '16:9/2',
  },
};

// -- En-tête ----------------------------------------------------------------------------------------------------------

export const Header: Story = {
  decorators: [gridDecorator, titleDecorator('Carte verticale avec badge sur le média')],
  args: {
    ...Default.args,
    badges: unBadge,
    badgesOnMedia: true,
  },
};

// -- Contenu ----------------------------------------------------------------------------------------------------------

export const Badges: Story = {
  decorators: [gridDecorator, titleDecorator('Carte verticale avec badges dans le contenu')],
  args: {
    ...Default.args,
    description: loremIpsum,
    badges: deuxBadges,
  },
};

export const Tags: Story = {
  decorators: [gridDecorator, titleDecorator('Carte verticale avec tags dans le contenu')],
  args: {
    ...Default.args,
    description: loremIpsum,
    tags: tags,
  },
};

export const Detail: Story = {
  decorators: [gridDecorator, titleDecorator('Carte verticale avec détail et icon')],
  args: {
    ...Default.args,
    description: loremIpsum,
    detail: detail,
    detailIcon: detailIcon,
  },
};

export const DetailBottom: Story = {
  decorators: [gridDecorator, titleDecorator('Carte verticale avec détails en haut et en bas')],
  args: {
    ...Default.args,
    description: loremIpsum,
    detail: detail,
    detailIcon: detailIcon,
    detailBottom: detail,
    detailBottomIcon: detailIcon,
  },
};

export const DetailBadges: Story = {
  name: 'Details & Badges',
  decorators: [gridDecorator, titleDecorator('Carte verticale avec détails et badges')],
  args: {
    ...Default.args,
    description: loremIpsum,
    detail: detail,
    detailIcon: detailIcon,
    detailBottom: detail,
    detailBottomIcon: detailIcon,
    badges: deuxBadges,
  },
};

export const DetailTags: Story = {
  name: 'Details & Tags',
  decorators: [gridDecoratorLG, titleDecorator('Carte verticale LG avec détails et tags')],
  args: {
    ...Default.args,
    description: loremIpsum,
    detail: detail,
    detailIcon: detailIcon,
    detailBottom: detail,
    detailBottomIcon: detailIcon,
    size: 'LG',
    tags: tags,
  },
};

// -- Sans lien --------------------------------------------------------------------------------------------------------

export const NoLink: Story = {
  decorators: [gridDecorator, titleDecorator('Carte sans lien')],
  args: {
    ...Default.args,
    link: '',
  },
};

// -- Sans lien étendu -------------------------------------------------------------------------------------------------

export const NoEnlargeLink: Story = {
  decorators: [gridDecorator, titleDecorator('Carte verticale sans lien étendu à la carte')],
  args: {
    ...Default.args,
    enlargeLink: false,
  },
};

export const NoEnlargeExtLink: Story = {
  decorators: [gridDecorator, titleDecorator('Carte avec lien externe, sans lien étendu')],
  args: {
    ...NoEnlargeLink.args,
    linkTarget: '_blank',
  },
};

// -- Désactivée -------------------------------------------------------------------------------------------------------

export const LinkDisabled: Story = {
  decorators: [gridDecorator, titleDecorator('Carte avec lien désactivé (a sans href)')],
  args: {
    ...Default.args,
    disabled: true,
  },
};

export const ButtonDisabled: Story = {
  decorators: [gridDecorator, titleDecorator('Carte avec bouton désactivé (disabled)')],
  args: {
    ...Default.args,
    enlargeButton: true,
    disabled: true,
  },
};

// -- Zone d'action -------------------------------------------------------------------------------------------------------

const baseTemplate = `
<dsfr-card
  [badges] = "badges"
  [badgesOnMedia] = "badgesOnMedia"
  [customBackground] = "customBackground"
  [customBorder] = "customBorder"
  [description] = "description"
  [detail] = "detail"
  [detailBottom] = "detailBottom"
  [detailBottomIcon] = "detailBottomIcon"
  [detailIcon] = "detailIcon"
  [download] = "download"
  [downloadAssessFile] = "downloadAssessFile"
  [downloadLangCode]="downloadLangCode"
  [downloadMimeType] = "downloadMimeType"
  [downloadSizeBytes] = "downloadSizeBytes"
  [disabled] = "disabled"
  [enlargeLink] = "enlargeLink"
  [hasFooter] = "hasFooter"
  [heading] = "heading"
  [headingLevel] = "headingLevel"
  [horizontal] = "horizontal"
  [imageFit] = "imageFit"
  [imagePath] = "imagePath"
  [imageRatio] = "imageRatio"
  [imageType] = "imageType"
  [link] = "link"
  [linkTarget] = "linkTarget"
  [route] = "route"
  [routePath] = "routePath"
  [size] = "size" 
  [tags] = "tags"
  [noIcon]="noIcon"
  [enlargeButton]="enlargeButton"
>$ACTIONS</dsfr-card>`;

export const ActionButtons: Story = {
  decorators: [gridDecorator, titleDecorator("Carte verticale avec zone d'action boutons")],
  args: {
    ...Default.args,
    hasFooter: true,
  },
  render: (args) => ({
    props: args,
    template: baseTemplate.replace(
      '$ACTIONS',
      `
      <dsfr-buttons-group footer inline="always">
          <dsfr-button label="Label" variant="secondary"></dsfr-button>
          <dsfr-button label="Label"></dsfr-button>
      </dsfr-buttons-group>
`,
    ),
  }),
};

export const ActionLinks: Story = {
  decorators: [gridDecorator, titleDecorator("Carte verticale avec zone d'action liens")],
  args: {
    ...Default.args,
    hasFooter: true,
  },
  render: (args) => ({
    props: args,
    template: baseTemplate.replace(
      '$ACTIONS',
      `
      <ul footer class="fr-links-group fr-links-group--inline">
          <li><a class="fr-link fr-icon-arrow-right-line fr-link--icon-right" id="link-1686" href="#">label</a></li>
          <li><a class="fr-link fr-icon-external-link-line fr-link--icon-right" id="link-1687" href="/iframe.html?viewMode=story&id=components-card--action-links" target="_blank">label</a></li>
      </ul>
`,
    ),
  }),
};

// -- Carte horizontale ------------------------------------------------------------------------------------------------

export const Horizontal: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Carte horizontale avec détails et tags')],
  args: {
    ...Default.args,
    description: loremIpsum,
    detail: detail,
    detailIcon: detailIcon,
    detailBottom: detail,
    detailBottomIcon: detailIcon,
    tags: tags,
    horizontal: true,
  },
};
