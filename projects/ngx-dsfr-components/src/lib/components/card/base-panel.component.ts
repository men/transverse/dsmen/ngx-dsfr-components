import { Component, inject, Input } from '@angular/core';
import { NavigationExtras } from '@angular/router';
import {
  downloadDetail,
  DsfrFileSizeUnit,
  DsfrHeadingLevel,
  DsfrLink,
  DsfrLinkTarget,
  DsfrNavigation,
  DsfrSize,
  LangService,
} from '../../shared';
import { DsfrBadge } from '../badge';
import { DsfrTag } from '../tag';
import { DsfrPanelBackground, DsfrPanelBorder } from './base-panel.model';
// Ne peut pas être déplacé dans 'shared' à cause de la dépendance sur DsfrBadge et DsfrTag

export const DEFAULT_HEADING_LEVEL: DsfrHeadingLevel = 'H3';

@Component({ template: '' })
export abstract class BasePanelComponent implements DsfrNavigation {
  /** @since 1.7 La propriété ariaLabel définit une valeur de chaîne qui étiquette un élément interactif. */
  @Input() ariaLabel?: string;

  /**
   * Permet de positionner des badges (jusqu'à 4 badges maxi) en haut du composant, optionnel.
   * @see DsfrBadge
   */
  @Input() badges: DsfrBadge[] = [];

  /**
   * Permet d'avoir un fond gris ou transparent.
   */
  @Input() customBackground: DsfrPanelBackground = 'default';

  /**
   * Permet d'afficher une ombre ou ne pas mettre de bordure.
   */
  @Input() customBorder: DsfrPanelBorder = 'default';

  /**
   * Une description optionnelle plein texte.
   */
  @Input() description: string;

  /**
   * Permet de désactiver le composant.
   */
  @Input() disabled = false;

  /**
   * Equivalent à l'attribut html natif 'download'.
   * - true : la fenêtre de téléchargement s'affiche directement,
   * - false : le fichier est ouvert avant d'être téléchargé,
   * - string : nouveau nom de fichier proposé au téléchargement.
   */
  @Input() downloadDirect: boolean | string = true;

  /**
   * Si 'true' (défaut) : le détail du téléchargement est automatiquement renseigné par le script DSFR.
   */
  @Input() downloadAssessFile = true;

  /**
   * Obligatoire, dans le cas d'un téléchargement si le document n'est pas du même langage que la page courante.
   * Ex : downloadLangCode="en".
   */
  @Input() downloadLangCode: string;

  /**
   * Format du fichier, à renseigner si downloadAssessFile est faux.
   */
  @Input() downloadMimeType: string;

  /**
   * Poids du fichier en octets, à renseigner si downloadAssessFile est faux.
   * */
  @Input() downloadSizeBytes: number;

  /**
   * Permet de désactiver le lien étendu de la carte.
   */
  @Input() enlargeLink = true;

  /**
   * Titre du composant.
   * Le fait de valoriser cette propriété est prioritaire sur le ng-content [heading].
   * Attention fonctionnellement la présence d'un titre est obligatoire.
   */
  @Input() heading: string;

  /**
   * Le niveau de titre devant être utilisé (tag `h4` par défaut).
   * Cette balise ne produit pas de style, mais de la structure.
   */
  @Input() headingLevel: string = DEFAULT_HEADING_LEVEL;

  /**
   * Passe le composant en mode horizontal.
   */
  @Input() horizontal = false;

  /**
   * Permet de positionner un lien de type href.
   * Sert aussi de lien de téléchargement.
   */
  @Input() link: string;

  /**
   * Permet de positionner une route Angular qui sera récupérée via 'output tileSelect.
   */
  @Input() route: string;

  /**
   * Path angular géré en tant que directive routerLink. Prioritaire et exclusif avec link et route.
   */
  @Input() routePath: string | string[];

  /**
   * Classe CSS utilisée pour la directive routerLink active.
   */
  @Input() routerLinkActive: string | string[] | undefined;

  /**
   * RouterLink : valeurs additionnelles de navigation pour le routerLink (queryParams, state, etc.)
   */
  @Input() routerLinkExtras: NavigationExtras;

  /**
   * Dimension du composant, 'MD' par défaut ('SM, 'MD' uniquement pour la tuile).
   */
  @Input() size: DsfrSize = 'MD';

  /**
   * Permet d'avoir un bouton à la place d'un lien @since 1.12
   */
  @Input() enlargeButton = false;

  /**
   * Attribut target du lien.
   */
  @Input() linkTarget: DsfrLinkTarget;

  private _download = false;
  private _downloadSizeUnit: DsfrFileSizeUnit;
  private _tags: DsfrTag[] = [];
  private _detailBottom: string;
  private langService = inject(LangService);

  protected constructor() {}

  get downloadSizeUnit(): DsfrFileSizeUnit {
    return this._downloadSizeUnit || this.langService.lang ? 'octets' : 'bytes';
  }

  /*  get linkTarget(): DsfrLinkTarget {
    return this._linkTarget;
  }*/

  get tags(): DsfrTag[] {
    return this._tags;
  }

  get itemLink(): DsfrLink {
    const label = this.heading ? this.heading : '';
    const itemLink: DsfrLink = { label: label, ariaLabel: this.ariaLabel }; // ariaLabel @since 1.7
    if (this.link) {
      itemLink.link = this.link;
    } else if (this.route) {
      itemLink.route = this.route;
    }
    return itemLink;
  }

  get download(): boolean {
    return this._download;
  }

  get detailBottom(): string {
    return !this.download ? this._detailBottom : this.downloadDetail;
  }

  /**
   * Dans le cas d'un téléchargement sans l'option 'downloadAssessFile',
   * on crée le détail à l'aide des propriétés 'downloadSizeBytes', 'downloadMimeType'.
   * @internal
   */
  protected get downloadDetail(): string {
    return downloadDetail(this.downloadMimeType, this.downloadSizeBytes, this.downloadSizeUnit);
  }

  /**
   * Transforme le composant classique en composant de téléchargement.
   */
  @Input() set download(download: boolean) {
    this._download = download;
    if (this._download) this.enlargeLink = true;
  }

  /** Zone de détail se trouvant visuellement sous la description et à côté de l’icône, optionnel.
   * Pour la tuile, on peut passer un slot de sélecteur "detail".
   * En cas de téléchargement le détail est remplacé par les informations du fichier calculées selon downloadAssessFile.
   */
  @Input() set detailBottom(value: string) {
    this._detailBottom = value;
  }

  /**
   * Force l'unité par défaut qui dépend de la langue du site, en octets (ko, Mo, ...) pour 'fr', en 'bytes' (KB, MB...) pour les autres langues.
   */
  @Input() set downloadSizeUnit(value: DsfrFileSizeUnit) {
    this._downloadSizeUnit = value;
  }

  /**
   * Permet de positionner des tags en haut de la carte (cliquables ou non), optionnel.
   * voir DsfrTag
   */
  @Input() set tags(tags: DsfrTag[]) {
    this._tags = tags;

    // Vérifie si un tag est cliquable pour désactiver enlargeLink.
    if (tags?.find((tag) => tag.link)) this.enlargeLink = false;
  }

  /**
   * @deprecated (@since 1.11.5) utiliser `routePath` à la place.
   * routerLink provoque un bug accessibilité sur la navigation au clavier (ajout d'un tabindex=0)
   **/
  @Input() set routerLink(value: string | string[] | undefined) {
    if (value) this.routePath = value;
  }

  /** @internal */
  hasLink(): boolean {
    return !!(this.link || this.route || this.routePath);
  }
}
