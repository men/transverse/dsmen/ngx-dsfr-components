
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrBadge } from '../../badge';
import { DsfrButtonModule } from '../../button';
import { DsfrButtonsGroupModule } from '../../buttons-group';
import { DsfrTag } from '../../tag';
import { DsfrCardModule } from '../card.module';

@Component({
  selector: 'demo-card-grid',
  templateUrl: './demo-card-grid.component.html',
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [DsfrCardModule, DsfrButtonsGroupModule, DsfrButtonModule],
})
export class DemoCardGridComponent {
  @Input() horizontal = false;
  @Input() size: 'SM' | 'MD' = 'MD';

  /** @internal */
  readonly heading = 'Intitulé de la carte (sur lequel se trouve le lien)';
  /** @internal */
  readonly image16x9 = 'img/placeholder.16x9.png';
  /** @internal */
  readonly route = '/maroute';
  /** @internal */
  readonly loremIpsum = `Lorem ipsum dolor sit amet, consectetur, incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et`;
  /** @internal */
  readonly badges: DsfrBadge[] = [
    { label: 'Label Badge', customClass: 'fr-badge--purple-glycine' },
    { label: 'Label Badge', customClass: 'fr-badge--purple-glycine' },
  ];
  /** @internal */
  readonly tags: DsfrTag[] = [{ label: 'label tag' }, { label: 'label tag' }];
  /** @internal */
  readonly detail = 'detail (optionnel)';
  /** @internal */
  readonly detailIcon = 'fr-icon-warning-fill';

  get cardGridClass(): string {
    return '<div class="fr-col-12 fr-col-md-6 fr-col-lg-4">';
  }
}
