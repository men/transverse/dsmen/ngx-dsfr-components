import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LinkDownloadComponent, SvgIconModule } from '../../shared';
import { DsfrBadgesGroupModule } from '../badges-group';
import { DsfrLinkModule } from '../link';
import { DsfrTagsGroupModule } from '../tags-group';
import { DsfrCardComponent } from './card.component';

@NgModule({
  declarations: [DsfrCardComponent],
  exports: [DsfrCardComponent],
  imports: [
    CommonModule,
    DsfrBadgesGroupModule,
    DsfrTagsGroupModule,
    SvgIconModule,
    DsfrLinkModule,
    LinkDownloadComponent,
  ],
})
export class DsfrCardModule {}
