import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { DsfrI18nService, DsfrSizeConst } from '../../shared';
import { BasePanelComponent } from './base-panel.component';
import { DsfrPanelBackgroundConst, DsfrPanelBorderConst } from './base-panel.model';
import { DsfrImageFit, DsfrImageRatio, DsfrImageType } from './card.model';

@Component({
  selector: 'dsfr-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DsfrCardComponent extends BasePanelComponent {
  /** Si true, les badges seront affichés sur la zone media. */
  @Input() badgesOnMedia: boolean;

  /**
   * Le détail, optionnel.
   */
  @Input() detail: string;

  /** Icône de la zone de détail, optionnel. */
  @Input() detailIcon: string;

  /** Icône devant le détail situé au bas de la carte, optionnel. */
  @Input() detailBottomIcon: string;

  /**
   * Zone d'actions, composée de bouton ou de liens, optionnelle (mais incompatible avec la
   * deuxième zone de détail). Si activée elle affiche le slot avec sélecteur `footer`
   */
  @Input() hasFooter = false;

  /** Texte alternatif d'une image à utiliser uniquement si l'image à une information à passer. */
  @Input() imageAlt: string = '';

  /**
   * Selon la valeur utilisée pour 'imageFit', l'élément peut être rogné, mis à l'échelle ou étiré, afin de remplir
   * la boîte qui le contient.
   */
  @Input() imageFit: DsfrImageFit;

  /** Url de l'image de l'entête, optionnel. */
  @Input() imagePath: string;

  /** Ratio de l'image. */
  @Input() imageRatio: DsfrImageRatio;

  /** Supprime l'icône du lien @since 1.12 */
  @Input() noIcon = false;

  /**
   * Type d'illustration
   * - `img` : pour l'utilisation d'une balise '<img>'.
   * - `svg` : pour l'utilisation de '<edu-svg-icon>'.
   * Si c'est le cas, `@ImagePath` devient le path du fichier `sprite.svg` et la concaténation de l'id du svg à afficher.
   */
  @Input() imageType: DsfrImageType = 'img';

  /** Remplace le lien du bouton de download par un markup de bouton */
  @Input() isDownloadButton = false;

  /** Signale quand la carte est sélectionnée. */
  @Output() readonly cardSelect: EventEmitter<string> = new EventEmitter();

  /** @internal */
  protected readonly borderConst = DsfrPanelBorderConst;
  /** @internal */
  protected readonly backgroundConst = DsfrPanelBackgroundConst;
  /** @internal */
  protected readonly sizeConst = DsfrSizeConst;

  /**@internal */
  constructor(public i18n: DsfrI18nService) {
    super();
  }

  /** @internal */
  onLinkSelect(): void {
    // on propage l'output, pas besoin de gérer ici le preventDefault, c'est géré en amont
    if (this.route) {
      this.cardSelect.emit(this.route);
    }
  }
}
