import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NavigationExtras } from '@angular/router';
import { DsfrLinkTarget, DsfrNavigation, DsfrSize, DsfrSizeConst } from '../../shared';
import { LoggerService } from '../../shared/services/logger.service';
import { DsfrMedia } from './content.model';

// https://www.systeme-de-design.gouv.fr/elements-d-interface/fondamentaux-techniques/medias/
type RatioImage = '16:9/2' | '16:9' | '3:2' | '4:3' | '1:1' | '3:4' | '2:3';
type RatioVideo = '16:9' | '4:3' | '1:1';

@Component({
  selector: 'dsfr-content',
  templateUrl: './content.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrContentComponent implements DsfrNavigation, OnInit {
  // -- Commun images / vidéos -----------------------------------------------------------------------------------------

  /** Type du média 'image' | 'video', 'image' par défaut. */
  @Input() type: DsfrMedia = 'image';

  /** Légende, optionnelle */
  @Input() legend: string | undefined;

  /** Le format conseillé par défaut est le 16:9. */
  @Input() ratio: RatioImage | RatioVideo = '16:9';

  /** @requires media, image, vidéo ou audio, obligatoire */
  @Input() source: string;

  // -- Lien de transcription sous le media (optionnel) ----------------------------------------------------------------

  /**
   * @since 1.6 remplace 'transcriptionLabel'.
   * Label du lien (de transcription), optionnel.
   */
  @Input() linkLabel: string;

  /** Lien de transcription (ou autre), optionnel. */
  @Input() link: string;

  /** Target du lien. Target par défaut de l'application si la propriété est non renseignée. */
  @Input() linkTarget: DsfrLinkTarget;

  /** Path interne. Exclusif avec link et routePath */
  @Input() route: string;

  /** Path angular géré en tant que directive routerLink. Exclusif avec link et route. */
  @Input() routePath: string | string[];

  /** RouterLink : classe utilisée pour la directive routerLink active. */
  @Input() routerLinkActive: string | string[] | undefined;

  /** RouterLink : valeurs additionnelles de navigation pour le routerLink (queryParams, state, etc.) */
  @Input() routerLinkExtras: NavigationExtras;

  /** Message du tooltip du média. */
  @Input() tooltipMessage: string;

  // -- Modal de transcription (optionnel) -----------------------------------------------------------------------------

  /**
   * @since 1.6 remplace 'transcription'.
   * Contenu, simple, optionnel de la modale de transcription.
   */
  @Input() transcriptionContent: string;

  /**
   * @since 1.6 Titre du média (image ou vidéo), de niveau h1.
   */
  @Input() transcriptionHeading: string;

  // -- Images ---------------------------------------------------------------------------------------------------------

  /**
   * Spécifique aux images.
   * Alternative d'une image (attribut alt), doit toujours être présente, sa valeur peut être vide (image n’apportant
   * pas de sens supplémentaire au contexte) ou non (porteuse de texte ou apportant du sens) selon votre contexte.
   */
  @Input() alternate: string;

  // -------------------------------------------------------------------------------------------------------------------

  /**
   * Si une route est spécifiée, émet un événement avec la route comme valeur, sinon n'émet rien.
   */
  @Output() linkSelect = new EventEmitter<string>();

  private _size: DsfrSize = DsfrSizeConst.MD;

  constructor(
    private sanitizer: DomSanitizer,
    private loggerService: LoggerService,
  ) {}

  get size(): DsfrSize {
    return this._size;
  }

  /** @deprecated (@since 1.6) utiliser `transcriptionContent` à la place */
  get transcription(): string {
    return this.transcriptionContent;
  }

  /** @deprecated (@since 1.6) utiliser `transcriptionLinkLabel` à la place  */
  get transcriptionLabel(): string {
    return this.linkLabel;
  }

  /** @deprecated (@since 1.6) utiliser `transcriptionContent` à la place  */
  @Input() set transcription(value: string) {
    this.transcriptionContent = value;
  }

  /** @deprecated (@since 1.6) utiliser `transcriptionLinkLabel` à la place  */
  @Input() set transcriptionLabel(value: string) {
    this.linkLabel = value;
  }

  /** Redimensionnement du composant : `LG` : 125%, `MD` : 100% (défault), `SM` : 75%. */
  @Input() set size(value: DsfrSize) {
    this._size = <DsfrSize>value?.toUpperCase();
  }

  /**
   * @deprecated (@since 1.11.5) utiliser `routePath` à la place.
   * routerLink provoque un bug accessibilité sur la navigation au clavier (ajout d'un tabindex=0)
   **/
  @Input() set routerLink(value: string | string[] | undefined) {
    if (value) this.routePath = value;
  }

  ngOnInit(): void {
    this.verifyAccessibility();
  }

  /** @internal */
  onLinkSelect() {
    if (this.route) {
      this.linkSelect.emit(this.route);
    }
  }

  /**
   * @internal
   */
  sanitizedSource() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.source);
  }

  private verifyAccessibility(): void {
    const missingVideoTitle = this.type === 'video' && !this.tooltipMessage;

    if (missingVideoTitle) {
      this.loggerService.warn('dsfr-content : Renseigner le titre du contenu est obligatoire (tooltipMessage).');
    }
  }
}
