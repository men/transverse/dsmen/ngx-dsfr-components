import { CommonModule, NgOptimizedImage } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrLinkModule } from '../link';
import { DsfrTranscriptionComponent } from '../transcription';
import { DsfrContentComponent } from './content.component';

@NgModule({
  declarations: [DsfrContentComponent],
  exports: [DsfrContentComponent],
  imports: [CommonModule, DsfrTranscriptionComponent, DsfrLinkModule, NgOptimizedImage],
})
export class DsfrContentModule {}
