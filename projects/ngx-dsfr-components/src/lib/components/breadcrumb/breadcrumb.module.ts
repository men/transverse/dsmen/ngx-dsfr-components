import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { ItemLinkComponent } from '../link/item-link.component';
import { DsfrBreadcrumbComponent } from './breadcrumb.component';

@NgModule({
  declarations: [DsfrBreadcrumbComponent],
  exports: [DsfrBreadcrumbComponent],
  imports: [CommonModule, ItemLinkComponent, DsfrI18nPipe],
})
export class DsfrBreadcrumbModule {}
