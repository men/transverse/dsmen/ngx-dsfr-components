import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { DsfrLink } from '../../shared';

@Component({
  selector: 'dsfr-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrBreadcrumbComponent {
  /** Tableau d'item du fil d'Ariane. */
  @Input() items: DsfrLink[] = [];

  /** Attribut aria-label de la balise `nav` du fil d'Ariane. */
  @Input() ariaLabel: string;

  /** Confirmation du clic sur le bouton. */
  @Output() linkSelect = new EventEmitter<DsfrLink>();

  /** @internal */
  constructor() {}

  /** @internal */
  onItem(item: DsfrLink): void {
    this.linkSelect.emit(item);
  }
}
