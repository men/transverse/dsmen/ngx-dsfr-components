import { DsfrNavigation } from '../../shared';

export interface DsfrSummaryLink extends DsfrNavigation {
  /** Nom du lien */
  label: string;

  /** Sous-entrées optionnelles */
  subEntries?: DsfrSummary;
}

/**
 * Un sommaire est une liste de liens
 */
export type DsfrSummary = DsfrSummaryLink[];
