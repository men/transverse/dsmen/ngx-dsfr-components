import { expect, test } from '@playwright/test';
import { getSbFrame, goToComponentPage } from '../../shared/utils/e2e.utils';

test.describe('Alerte', () => {
  test('renders fr-tile when heading is defined', async ({ page }) => {
    const frame = page.frameLocator('#storybook-preview-iframe');
    const div = frame.locator('div.fr-alert');
    const titleElement = div.locator('p.fr-alert__title');

    await goToComponentPage('alert', page);

    await expect(div).toBeVisible();
    await expect(titleElement).toBeVisible();
  });

  test('does not render fr-tile if heading is not defined', async ({ page }) => {
    const frame = getSbFrame(page);
    const div = frame.locator('div.fr-alert--sm');
    const titleElement = div.locator('.fr-alert__title');

    await goToComponentPage('alert', page, 'small');

    await expect(div).toBeVisible();
    await expect(titleElement).not.toBeVisible();
  });
});
