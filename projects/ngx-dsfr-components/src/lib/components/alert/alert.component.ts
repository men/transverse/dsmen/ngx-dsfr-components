import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  Renderer2,
  signal,
  viewChild,
  ViewEncapsulation,
} from '@angular/core';
import { DsfrHeadingLevel, DsfrSeverity, DsfrSeverityConst, DsfrSizeConst } from '../../shared';
import { DsfrAlertSize, DsfrAlertSizeConst } from './alert.size';

@Component({
  selector: 'dsfr-alert',
  templateUrl: './alert.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrAlertComponent implements AfterViewInit {
  /** Le titre de l'alerte. Peut être remplacé par le slot [heading] */
  @Input() heading: string;

  /** Le niveau de titre devant être utilisé, une balise <p> sera utilisée si la propriété n'est pas renseignée. */
  @Input() headingLevel: DsfrHeadingLevel;

  /** Le corps du message de l'alerte (texte simple). Peut être remplacé par le slot message */
  @Input() message: string;

  /** Le niveau d'alerte. 'info' par défaut. */
  @Input() severity: DsfrSeverity;

  /** La valeur à utiliser pour le rôle ARIA. 'alert' par défaut. */
  @Input() ariaRoleValue: 'alert' | 'status' = 'alert';

  /** La taille de l'alerte. 'MD' par défaut. */
  @Input() size: DsfrAlertSize;

  /** Le libellé associé au bouton de fermeture de l'alerte. */
  @Input() closeControlLabel: string | undefined;

  /** Doit être vrai si l'alerte apparait dynamiquement en cours de navigation. (ajout de role=alert pour l'accessibilité),
   *  false par défaut */
  // https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/alerte
  // Pour les développeurs.
  // Nous avons retiré l’attribut role="alert" des exemples de code pour les alertes présentes au chargement de la page.
  // En effet, l'élément avec un role="alert" sont les premières choses lues par les technologies d'assistance.
  // De ce fait, le role="alert" doit être réservé aux alertes ajoutées (/injectées) dynamiquement au cours de la navigation.
  // Ex : alertes de notification suite à une action utilisateur ou mise à jour d’un statut.
  @Input() hasAriaRole = false;

  /** L'alerte peut être masquée */
  @Input() closeable = false;

  /** Signale la fermeture de l'alerte. */
  @Output() conceal = new EventEmitter<void>();

  protected containerSlotHeading = viewChild('containerSlotHeading', { read: ElementRef });
  protected isNgContentEmpty = signal(false);

  constructor(
    private el: ElementRef,
    private renderer: Renderer2,
  ) {
    this.severity = DsfrSeverityConst.INFO;
    this.size = DsfrAlertSizeConst.MD;
  }

  /** @deprecated @since 1.7.0 use 'closeable' instead (avec un 'e') */
  get closable(): boolean {
    return this.closeable;
  }

  /** @deprecated (@since 1.7.0) utiliser `closeable` à la place (avec un 'e') */
  @Input() set closable(value: boolean) {
    this.closeable = value;
  }
  /**
   * Supprimer le container <p> du titre si le contenu projeté heading est vide
   * ok a l'initialisation la balise sera bien supprimé.
   * mais si du contenu est ajouté dynamiquement dans le slot du heading alors qu'il est vide a l'init, le heading ne
   * sera pas projeté. Solution temporaire pour ce cas précis : initialiser avec une valeur vide type <span></span>
   *  */
  ngAfterViewInit(): void {
    if (this.isSmall() && !this.heading) {
      const hasContent = this.hasContent(this.containerSlotHeading()?.nativeElement);
      this.isNgContentEmpty.set(!hasContent);
    }
  }

  /** @internal */
  getClasses(): string[] {
    const classes = ['fr-alert'];
    if (this.severity === DsfrSeverityConst.ERROR) classes.push('fr-alert--error');
    if (this.severity === DsfrSeverityConst.SUCCESS) classes.push('fr-alert--success');
    if (this.severity === DsfrSeverityConst.INFO) classes.push('fr-alert--info');
    if (this.severity === DsfrSeverityConst.WARNING) classes.push('fr-alert--warning');
    if (this.size === DsfrSizeConst.SM) classes.push('fr-alert--sm');
    return classes;
  }

  /** @internal */
  isSmall(): boolean {
    return this.size == DsfrSizeConst.SM;
  }

  /** @internal */
  onClose(event: Event): void {
    const parentNode = this.el.nativeElement?.parentNode;
    if (parentNode) {
      this.renderer.removeChild(parentNode, this.el.nativeElement, true);
    }

    this.conceal.emit();
  }

  /**
   * Test si le container doit être affiché (contenu visible)
   * @returns true si le container possède au moins un noeuf enfant ou du contenu de texte (exclure les commentaires et chaine vide)
   */
  private hasContent(container: HTMLElement): boolean {
    return !!(container?.children?.length > 0 || (container?.textContent && container.textContent !== ' '));
  }
}
