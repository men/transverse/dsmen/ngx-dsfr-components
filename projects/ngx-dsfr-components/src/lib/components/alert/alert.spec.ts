import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DsfrAlertComponent } from './alert.component';
import { DsfrAlertModule } from './alert.module';

const TITLE = "Titre de l'alerte (slot par défaut)";
const MESSAGE = "<p message>Message de l'alerte <b>(slot par défaut)</b></p>";

@Component({
  selector: `edu-host-component`,
  template: ` <dsfr-alert>${TITLE}${MESSAGE}</dsfr-alert>`,
})
class TestHostComponent {
  @ViewChild(DsfrAlertComponent)
  public alertComponent: DsfrAlertComponent;
}

describe('DsfrAlertComponentTest', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;
  let alertComponent: DsfrAlertComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrAlertModule],
      declarations: [TestHostComponent],
    }).compileComponents();
    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
    alertComponent = testHostComponent.alertComponent;
  });

  it('should create', () => {
    expect(testHostComponent).not.toBeNull();
    expect(alertComponent).not.toBeNull();
  });

  it('should have title', () => {
    const titleElt = fixture.nativeElement.querySelector('.fr-alert__title');
    expect(titleElt).not.toBeNull();
    expect(titleElt.textContent).toEqual(TITLE);
  });

  it('should not have title', () => {
    const titleElt = fixture.nativeElement.querySelector('.fr-alert__title');
    expect(titleElt).not.toBeNull();
    expect(titleElt.textContent).toEqual(TITLE);
  });

  it('should have message', () => {
    const messageElt = fixture.nativeElement.querySelector('[message]');
    expect(messageElt).not.toBeNull();
    expect(messageElt.textContent).toEqual("Message de l'alerte (slot par défaut)");
  });

  it('property should be given priority', () => {
    const title = 'Titre prioritaire sur la projection';
    alertComponent.heading = title;
    fixture.detectChanges();
    const titleElt = fixture.nativeElement.querySelector('.fr-alert__title');
    expect(titleElt).not.toBeNull();
    expect(titleElt.textContent).toEqual(title);
  });
});
