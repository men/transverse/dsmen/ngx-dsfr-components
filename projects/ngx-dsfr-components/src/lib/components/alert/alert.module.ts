import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrAlertComponent } from './alert.component';

@NgModule({
  declarations: [DsfrAlertComponent],
  exports: [DsfrAlertComponent],
  imports: [CommonModule, DsfrI18nPipe],
})
export class DsfrAlertModule {}
