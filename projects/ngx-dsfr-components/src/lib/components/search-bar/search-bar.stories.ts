import { argEventEmitter } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrSearchBarComponent } from './search-bar.component';

const meta: Meta = {
  title: 'COMPONENTS/SearchBar',
  component: DsfrSearchBarComponent,
  decorators: [moduleMetadata({ imports: [FormsModule, DsfrI18nPipe] })],
  argTypes: {
    searchChange: { control: argEventEmitter },
    searchSelect: { control: argEventEmitter },
  },
};
export default meta;

type Story = StoryObj<DsfrSearchBarComponent>;

export const Default: Story = {
  args: {},
};

export const WithInitialValue: Story = {
  args: {
    initialValue: 'Test 1',
  },
};

export const Large: Story = {
  args: {
    large: true,
  },
};
