import { AfterContentInit, Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { DsfrI18nService, newUniqueId } from '../../shared';

@Component({
  selector: 'dsfr-search-bar',
  templateUrl: './search-bar.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrSearchBarComponent implements OnInit, AfterContentInit {
  /**
   * Valeur initiale du champ de recherche.
   */
  @Input() initialValue: string | undefined;

  /**
   * Attribut 'id' du champ de recherche, généré automatiquement par défaut.
   */
  @Input() inputId: string | undefined;

  /** Texte du bouton  */
  @Input() buttonText: string;

  /** Label (pour les lecteurs d'écrans). */
  @Input() label: string;

  /** Bascule le composant dans sa version large. */
  @Input() large: boolean = false;

  /** Placeholder de l'input du composant */
  @Input() placeholder: string | undefined;

  /** Attribut title du bouton */
  @Input() buttonTitle: string | undefined;

  /** Emet le texte lors de l'évènement keyup sur l'input de recherche.  */
  @Output() searchChange = new EventEmitter<string>();

  /** Emet le texte lors du clic sur le bouton "rechercher".  */
  @Output() searchSelect: EventEmitter<string> = new EventEmitter<string>();

  /** Le texte à rechercher. */
  text: string;

  protected defaultButtonTitle: string | null;

  private _id: string | undefined;

  constructor(protected i18n: DsfrI18nService) {}

  get formId() {
    return 'search-form-' + this.inputId;
  }

  /**
   * @deprecated utiliser inputId
   * @since 1.5
   * * Cet attribut doit être utilisé en tant que propriété et non en attribut, ex. [id]="'monid'"
   */
  @Input() set id(value: string | undefined) {
    if (value) {
      this._id = value;
      this.inputId ??= this._id;
    }
  }

  ngOnInit(): void {
    this.text = this.initialValue ?? '';
  }

  ngAfterContentInit() {
    if (!this.inputId) this.inputId = newUniqueId();
  }

  /** @internal */
  onSearch() {
    this.searchChange.emit(this.text);
  }

  /** @internal */
  onSearchClick() {
    this.searchSelect.emit(this.text);
  }

  /**
   * Si le libellé du bouton est identique au libellé potentiel du tooltip, il ne faut pas positionner de tooltip.
   * @internal
   */
  getButtonTitleValue(): string | null {
    const defaultTitle = this.i18n.t('commons.search');

    return this.buttonText === defaultTitle ? null : defaultTitle;
  }
}
