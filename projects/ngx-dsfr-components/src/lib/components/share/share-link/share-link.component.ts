import { Component, Input } from '@angular/core';
import { DsfrLinkTarget } from '../../../shared';
import { DsfrShareName, DsfrShareNameConst } from '../share.model';

@Component({
  selector: 'dsfr-share-link',
  templateUrl: './share-link.component.html',
})
export class DsfrShareLinkComponent {
  /** Indique si le composant est disable ou non (le mail est toujours enable). */
  @Input() disabled: boolean;

  /** la propriété 'id' est optionnelle, à renseigner si nécessaire par le développeur utilisant la librairie.*/
  @Input() id: string;

  /** 'Partager <name>' par défaut, peut être personnalisé. */
  @Input() label: string;

  /** Lien obligatoire pour les réseaux sociaux et mailto. Optionnel pour le presse-papier. */
  @Input() link: string;

  /** L'attribut rel définit la relation entre une ressource liée et le document actuel.
   * Cf. https://developer.mozilla.org/fr/docs/Web/HTML/Attributes/rel
   */
  @Input() rel: string;

  /** Target optionnelle, target html par défaut si non renseigné. */
  @Input() linkTarget: DsfrLinkTarget;

  private _name: DsfrShareName;

  /** @deprecated (@since 1.8.0) use linkTarget instead. */
  get target(): DsfrLinkTarget {
    return this.linkTarget;
  }

  get name(): DsfrShareName {
    return this._name;
  }

  /** @deprecated (@since 1.8.0) use linkTarget instead. */
  @Input() set target(value: DsfrLinkTarget) {
    this.linkTarget = value;
  }

  /** La propriété 'name' est obligatoire et doit être sélectionnée dans une liste énumérée. */
  @Input() set name(value: DsfrShareName) {
    if (value === DsfrShareNameConst.TWITTER) {
      this._name = DsfrShareNameConst.X;
    } else {
      this._name = value;
    }
  }

  /** @internal */
  getClasses() {
    return `fr-btn--${this.name} fr-btn`;
  }
}
