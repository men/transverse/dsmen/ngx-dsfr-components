import { argEventEmitter, dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrLinkTargetConst } from '../../shared';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrLinkModule } from '../link';
import { DsfrShareComponent, DsfrShareLinkComponent } from './index';

const meta: Meta = {
  title: 'COMPONENTS/Share',
  component: DsfrShareComponent,
  decorators: [moduleMetadata({ declarations: [DsfrShareLinkComponent], imports: [DsfrLinkModule, DsfrI18nPipe] })],
  parameters: {},
  argTypes: {
    consentManagerSelect: { control: { control: argEventEmitter } },
    consentManagerLinkTarget: { control: 'inline-radio', options: Object.values(DsfrLinkTargetConst) },
  },
};
export default meta;
type Story = StoryObj<DsfrShareComponent>;

const template = `<dsfr-share 
  [consentManagerLink]="consentManagerLink" 
  [consentManagerLinkTarget]="consentManagerLinkTarget" 
  [consentManagerRoute]="consentManagerRoute" 
  [hasCookiePermissions]="hasCookiePermissions">
    <dsfr-share-link name="facebook" link="https://fr-fr.facebook.com/"></dsfr-share-link>
    <dsfr-share-link name="linkedin" link="https://www.linkedin.com/"></dsfr-share-link>
    <dsfr-share-link name="mastodon" link="https://mastodon.social/"></dsfr-share-link>
    <dsfr-share-link name="twitter-x" link="https://twitter.com/"></dsfr-share-link>
    <dsfr-share-link name="mail" link="mailto:dsmen-core@ldiff.forge.education.gouv.fr"></dsfr-share-link>
</dsfr-share>`;

export const Default: Story = {
  decorators: dsfrDecorator('Boutons de partage par défaut'),
  args: {
    consentManagerLink: '',
    consentManagerLinkTarget: undefined,
    consentManagerRoute: '',
    hasCookiePermissions: true,
  },
  render: (args) => ({
    props: args,
    template: template,
  }),
};

export const ConsentLink: Story = {
  decorators: dsfrDecorator('Boutons de partage version inactive', 'Lien de consentement'),
  args: {
    ...Default.args,
    hasCookiePermissions: false,
    consentManagerLink: '?path=/story/components-consent-banner--default',
  },
  render: (args) => ({
    props: args,
    template: template,
  }),
};
