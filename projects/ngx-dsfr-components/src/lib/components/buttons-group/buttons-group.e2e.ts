import { expect, test } from '@playwright/test';
import { getSbFrame, goToComponentPage } from '../../shared/utils/e2e.utils';

test.describe('ButtonsGroup', () => {
  test('should produce expected dsfr markup', async ({ page }) => {
    const frame = getSbFrame(page);
    const buttons = frame.locator('ul.fr-btns-group > li > :only-child > button');

    await goToComponentPage('button-group', page);
    await expect(buttons).toHaveCount(3);
  });

  test('should produce div as parent tag instead of ul', async ({ page }) => {
    const frame = getSbFrame(page);
    const buttons = frame.locator('div.fr-btns-group > :not(li) > button');

    await goToComponentPage('button-group', page, 'group-markup-div');
    await expect(buttons).toHaveCount(3);
  });
});
