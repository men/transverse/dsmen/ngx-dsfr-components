import { dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrTextSizeConst } from '../../shared';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrQuoteComponent } from './index';

const meta: Meta = {
  title: 'COMPONENTS/Quote',
  component: DsfrQuoteComponent,
  decorators: [moduleMetadata({ imports: [DsfrI18nPipe] })],
  argTypes: {
    textSize: {
      control: { type: 'inline-radio' },
      options: Object.values(DsfrTextSizeConst),
    },
  },
};
export default meta;

type Story = StoryObj<DsfrQuoteComponent>;

export const Default: Story = {
  decorators: dsfrDecorator('Citation par défaut (texte taille xl)'),
  args: {
    text: `« Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua. Vitae 
    sapien pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan lacus vel facilisis 
    volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet consectetur adipiscing elit ut. »`,
    sourceUrl: 'https://en.wikipedia.org/wiki/Lorem_ipsum',
    author: 'Cicero',
    details: [
      { text: 'Titre sans source', isTitle: true },
      { text: 'Titre avec source', isTitle: true, sourceUrl: 'https://en.wikipedia.org/wiki/Lorem_ipsum' },
      { text: 'Détail sans source' },
      { text: 'Détail avec source', sourceUrl: 'https://en.wikipedia.org/wiki/Lorem_ipsum' },
    ],
  },
};

export const TextSizeLg: Story = {
  decorators: dsfrDecorator('Citation en taille lg'),
  args: {
    ...Default.args,
    textSize: DsfrTextSizeConst.LG,
  },
};

export const Illustration: Story = {
  decorators: dsfrDecorator('Citation avec illustration'),
  args: {
    ...Default.args,
    imagePath: 'img/placeholder.1x1.png',
    imageAlt: 'texte alternatif',
  },
};

export const CustomClass: Story = {
  decorators: dsfrDecorator('Citation accentuée'),
  args: {
    ...Default.args,
    customClass: 'fr-quote--green-emeraude',
  },
};
