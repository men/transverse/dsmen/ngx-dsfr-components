import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrQuoteComponent } from './quote.component';

@NgModule({
  declarations: [DsfrQuoteComponent],
  exports: [DsfrQuoteComponent],
  imports: [CommonModule, DsfrI18nPipe],
})
export class DsfrQuoteModule {}
