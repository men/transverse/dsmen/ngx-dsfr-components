import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { DsfrSize, DsfrSizeConst, newUniqueId } from '../../shared';
import { DsfrSegmentedControl } from './segmented-control.model';

@Component({
  selector: 'dsfr-segmented-control',
  templateUrl: './segmented-control.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrSegmentedControlComponent {
  /**
   * Liste des choix du contrôle segmenté.
   */
  @Input() segments: DsfrSegmentedControl[];

  /**
   * Légende du composant (obligatoire).
   */
  @Input() legend: string;

  /**
   * Cache la légende visuellement.
   */
  @Input() legendSrOnly = false;

  /**
   * Texte de description additionnel.
   */
  @Input() hint: string;

  /** Taille du contrôle segmenté, existe en `SM` et `MD` uniquement. `MD` par défaut. */
  @Input() size: DsfrSize = DsfrSizeConst.MD;

  /**
   * Contrôle segmenté avec légende en ligne
   */
  @Input() inline = false;

  /**
   * Renvoie la value du choix selectionné.
   */
  @Output() segmentedControlSelect = new EventEmitter<string>();

  private _name: string;

  get name() {
    return this._name;
  }

  /**
   * Valeur de l'attribut name des inputs du contrôle segmenté.
   */
  @Input() set name(value: string) {
    if (!value.length) {
      this._name = 'segmented-' + newUniqueId();
    } else {
      this._name = value;
    }
  }

  /** @internal */
  getClasses(): {} {
    return {
      'fr-segmented': true,
      'fr-segmented--sm': this.size === DsfrSizeConst.SM,
      'fr-segmented--no-legend': this.legendSrOnly,
    };
  }

  /** @internal */
  getSegmentedControlId(index: number): string {
    return `${this._name}_${index + 1}`;
  }

  /**
   * @internal
   */
  onSegmentSelect(value: string) {
    this.segmentedControlSelect.emit(value);
  }
}
