import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrSegmentedControlComponent } from './segmented-control.component';

@NgModule({
  declarations: [DsfrSegmentedControlComponent],
  exports: [DsfrSegmentedControlComponent],
  imports: [CommonModule],
})
export class DsfrSegmentedControlModule {}
