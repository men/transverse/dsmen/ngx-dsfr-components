export interface DsfrSegmentedControl {
  /** Valeur */
  value: any;
  /** Label  */
  label: string;
  /** Icone */
  icon?: string;
  /** Disabled */
  disabled?: boolean;
  /** Checked - Affiche l'item actif à son premier affichage */
  checked?: boolean;
}
