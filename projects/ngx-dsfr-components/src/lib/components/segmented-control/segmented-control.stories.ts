import { dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrSizeConst, HeadingModule } from '../../shared';
import { DsfrSegmentedControlComponent } from './index';
import { DsfrSegmentedControl } from './segmented-control.model';

const meta: Meta = {
  title: 'COMPONENTS/Segmented control',
  component: DsfrSegmentedControlComponent,
  decorators: [moduleMetadata({ imports: [HeadingModule] })],
  argTypes: {
    size: { control: { type: 'inline-radio' }, options: Object.values(DsfrSizeConst) },
  },
};
export default meta;
type Story = StoryObj<DsfrSegmentedControlComponent>;

const segments: DsfrSegmentedControl[] = [
  {
    label: 'Libelle 1',
    value: '1',
  },
  {
    label: 'Libelle 2',
    value: '2',
    checked: true,
  },
  {
    label: 'Libelle 3',
    value: '3',
  },
];
const segmentsIcons: DsfrSegmentedControl[] = [
  {
    label: 'Libelle 1',
    value: '1',
    icon: 'fr-icon-road-map-line',
  },
  {
    label: 'Libelle 2',
    value: '2',
    icon: 'fr-icon-road-map-line',
    checked: true,
  },
  {
    label: 'Libelle 3',
    value: '3',
    icon: 'fr-icon-road-map-line',
    disabled: true,
  },
];
const segmentsDisabled: DsfrSegmentedControl[] = [
  {
    label: 'Libelle 1',
    value: '1',
  },
  {
    label: 'Libelle 2',
    value: '2',
    checked: true,
  },
  {
    label: 'Libelle 3',
    value: '3',
    disabled: true,
  },
];

export const Default: Story = {
  decorators: dsfrDecorator('Défaut'),
  args: {
    segments: segments,
    legend: 'Légende',
    name: 'mySegmentedControl',
    size: DsfrSizeConst.MD,
    legendSrOnly: false,
    inline: false,
    hint: '',
  },
};

export const Small: Story = {
  decorators: dsfrDecorator('Taille small'),
  args: {
    ...Default.args,
    size: DsfrSizeConst.SM,
  },
};

export const Inline: Story = {
  decorators: dsfrDecorator('Légende en ligne'),
  args: {
    ...Default.args,
    inline: true,
  },
};

export const Hint: Story = {
  decorators: dsfrDecorator('Légende avec aide'),
  args: {
    ...Default.args,
    hint: 'Texte descriptif',
  },
};

export const Icons: Story = {
  decorators: dsfrDecorator('Avec icônes'),
  args: {
    ...Default.args,
    segments: segmentsIcons,
  },
};

export const HideLegend: Story = {
  decorators: dsfrDecorator('Légende invisible'),
  args: {
    ...Default.args,
    legendSrOnly: true,
  },
};

export const Disabled: Story = {
  decorators: dsfrDecorator('Disabled'),
  args: {
    ...Default.args,
    segments: segmentsDisabled,
    name: 'Disabled',
  },
};
