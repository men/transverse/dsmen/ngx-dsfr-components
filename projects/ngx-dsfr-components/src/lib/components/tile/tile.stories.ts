import { argEventEmitter, bgDecorator, optionsHeadingLevel, titleDecorator } from '.storybook/storybook-utils';
import { componentWrapperDecorator, Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFileSizeUnitConst, DsfrLinkTargetConst, LinkDownloadComponent, PictogramModule } from '../../shared';
import { DsfrBadge } from '../badge';
import { DsfrBadgesGroupModule } from '../badges-group';
import { DsfrPanelBackgroundConst, DsfrPanelBorderConst } from '../card';
import { DEFAULT_HEADING_LEVEL } from '../card/base-panel.component';
import { DsfrMimeTypeConst } from '../download';
import { DsfrLinkModule } from '../link';
import { DsfrTag } from '../tag';
import { DsfrTagsGroupModule } from '../tags-group';
import { DsfrTileComponent } from './tile.component';

const meta: Meta = {
  title: 'COMPONENTS/Tile',
  component: DsfrTileComponent,
  decorators: [
    moduleMetadata({
      imports: [DsfrBadgesGroupModule, DsfrTagsGroupModule, DsfrLinkModule, PictogramModule, LinkDownloadComponent],
    }),
  ],
  argTypes: {
    customBackground: { control: { type: 'inline-radio' }, options: Object.values(DsfrPanelBackgroundConst) },
    customBorder: { control: { type: 'inline-radio' }, options: Object.values(DsfrPanelBorderConst) },
    downloadSizeUnit: { control: { type: 'inline-radio' }, options: Object.values(DsfrFileSizeUnitConst) },
    headingLevel: { control: 'inline-radio', options: optionsHeadingLevel },
    size: { control: 'inline-radio', options: ['SM', 'MD'] }, // "LG" non pris en compte
    linkTarget: { control: { type: 'inline-radio' }, options: Object.values(DsfrLinkTargetConst) },
    useGreyBackground: { table: { disable: true } },
    tileSelect: { control: { control: argEventEmitter } },
  },
};
export default meta;
type Story = StoryObj<DsfrTileComponent>;

const artworkCityHall = 'artwork/pictograms/buildings/city-hall.svg';
const description = 'Lorem ipsum dolor sit amet, consectetur adipiscing, incididu';
const detail = 'Détail (optionnel)';
const route = '/maroute';
const badges: DsfrBadge[] = [{ label: 'Libellé Badge', customClass: 'fr-badge--purple-glycine' }];
const tags: DsfrTag[] = [{ label: 'Libellé tag' }];

const gridDecorator = componentWrapperDecorator(
  (story) =>
    `<div class="fr-mb-6v">
  <div class="fr-grid-row fr-grid-row--gutters">
    <div class="fr-col-12 fr-col-md-4 fr-col-lg-3">
      ${story}
    </div>
  </div>
</div>`,
);

// -- Tailles ----------------------------------------------------------------------------------------------------------

export const Default: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile verticale MD, taille par défaut')],
  args: {
    // Valeurs par défaut
    artworkDirPath: 'artwork',
    customBackground: 'default',
    customBorder: 'default',
    disabled: false,
    download: false,
    downloadDirect: true,
    downloadAssessFile: false,
    enlargeLink: true,
    headingLevel: DEFAULT_HEADING_LEVEL,
    horizontal: false,
    size: 'MD',
    // Obligatoires
    heading: 'Intitulé de la tuile',
    // autres
    artworkFilePath: artworkCityHall,
    description: description,
    detailBottom: '',
    link: '#',
  },
};

export const Small: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile taille SM')],
  args: {
    ...Default.args,
    size: 'SM',
  },
};

// -- Sans image -------------------------------------------------------------------------------------------------------

export const NoImage: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile sans image cas max')],
  args: {
    ...Default.args,
    artworkFilePath: '',
  },
};

// -- Contenu ----------------------------------------------------------------------------------------------------------

export const Badges: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile verticale avec badge dans le contenu')],
  args: {
    ...Default.args,
    badges: badges,
  },
};

export const Tags: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile verticale avec tag dans le contenu')],
  args: {
    ...Default.args,
    tags: tags,
  },
};

export const Detail: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile verticale avec détail')],
  args: {
    ...Default.args,
    detailBottom: 'Détail (optionnel)',
  },
};

// -- Variantes --------------------------------------------------------------------------------------------------------

export const GreyBackground: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile verticale variation accentuée en contrast grey')],
  args: {
    ...Default.args,
    customBackground: DsfrPanelBackgroundConst.GREY,
  },
};

export const Borderless: Story = {
  decorators: [gridDecorator, bgDecorator('lightyellow'), titleDecorator('Tuile verticale variation sans bordure')],
  args: {
    ...Default.args,
    customBorder: DsfrPanelBorderConst.NO_BORDER,
  },
};

export const Transparent: Story = {
  decorators: [gridDecorator, bgDecorator('lightyellow'), titleDecorator('Tuile verticale variation sans fond')],
  args: {
    ...Default.args,
    customBackground: DsfrPanelBackgroundConst.TRANSPARENT,
  },
};

export const Shadow: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile verticale variation ombre portée')],
  args: {
    ...Default.args,
    customBorder: DsfrPanelBorderConst.SHADOW,
  },
};

// -- Markup button ----------------------------------------------------------------------------------------------------

export const MarkupButton: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile avec bouton')],
  args: {
    ...Default.args,
    enlargeButton: true,
  },
};

// -- Sans lien --------------------------------------------------------------------------------------------------------

export const NoLink: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile sans lien')],
  args: {
    ...Default.args,
    link: '',
    description: '',
  },
};

// -- Sans lien étendu -------------------------------------------------------------------------------------------------

export const NoEnlargeLink: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile verticale sans lien étendu à la tuile')],
  args: {
    ...Default.args,
    enlargeLink: false,
    description: '',
  },
};

export const NoEnlargeExtLink: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile verticale sans lien étendu à la tuile, nouvelle fenêtre')],
  args: {
    ...NoEnlargeLink.args,
    linkTarget: '_blank',
  },
};

// -- Désactivée -------------------------------------------------------------------------------------------------------

export const Disabled: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile verticale sans lien étendu à la tuile, nouvelle fenêtre')],
  args: {
    ...Default.args,
    disabled: true,
    description: '',
  },
};

// -- Icône ------------------------------------------------------------------------------------------------------------

export const NoIcon: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile sans icône')],
  args: {
    ...Default.args,
    noIcon: true,
    description: '',
  },
};

export const ExternalLink: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile avec icône lien externe')],
  args: {
    ...Default.args,
    linkTarget: '_blank',
    description: '',
  },
};

// -- Tuile horizontale ------------------------------------------------------------------------------------------------

export const Horizontal: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile horizontale avec détails et tag')],
  args: {
    ...Default.args,
    description: '',
    horizontal: true,
    detailBottom: 'Détail (optionnel)',
    tags: tags,
  },
};

export const HorizontalSM: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile SM horizontale avec détails et badge')],
  args: {
    ...Default.args,
    description: '',
    horizontal: true,
    detailBottom: 'Détail (optionnel)',
    badges: badges,
    size: 'SM',
  },
};

export const BreakpointMD: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile horizontale puis vertical à partir du breakpoint md')],
  args: {
    ...Default.args,
    description: '',
    horizontal: true,
    detailBottom: 'Détail (optionnel)',
    rotateOn: 'MD',
  },
};

// -- Download ---------------------------------------------------------------------------------------------------------

const artworkDownload = 'artwork/pictograms/document/document-download.svg';
const downloadHeading = 'Télécharger le document XX';
const downloadDescription = 'Description (optionnelle)';
const downloadPath = 'files/lorem-ipsum.pdf';

export const Download: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile de téléchargement')],
  args: {
    ...Default.args,
    heading: downloadHeading,
    description: downloadDescription,
    link: downloadPath,
    download: true,
    downloadAssessFile: false,
    downloadSizeBytes: 76 * 1024 + 100,
    downloadMimeType: DsfrMimeTypeConst.APPLICATION_PDF,
  },
};

export const DownloadAuto: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile de téléchargement avec détail renseigné automatiquement')],
  args: {
    ...Default.args,
    artworkFilePath: artworkDownload,
    heading: downloadHeading,
    description: downloadDescription,
    link: downloadPath,
    download: true,
    downloadAssessFile: true,
  },
};

export const DownloadDocLang: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile de téléchargement avec fichier en langue étrangère')],
  args: {
    ...Default.args,
    artworkFilePath: artworkDownload,
    heading: downloadHeading,
    description: downloadDescription,
    link: downloadPath,
    download: true,
    downloadAssessFile: true,
    downloadLangCode: 'la',
  },
};

export const DownloadButton: Story = {
  decorators: [gridDecorator, titleDecorator('Tuile de téléchargement avec bouton à la place du lien')],
  args: {
    ...Default.args,
    artworkFilePath: artworkDownload,
    heading: downloadHeading,
    description: downloadDescription,
    link: downloadPath,
    download: true,
    downloadAssessFile: false,
    downloadSizeBytes: 76 * 1024 + 100,
    downloadMimeType: DsfrMimeTypeConst.APPLICATION_PDF,
    isDownloadButton: true,
  },
};

// -- Spécifique -------------------------------------------------------------------------------------------------------

export const Slots: Story = {
  decorators: [gridDecorator, titleDecorator('Utilisation des slots du composant pour le titre et la description')],
  args: {
    ...Default.args,
    artworkFilePath: artworkCityHall,
    description: description,
    headingLevel: 'H2',
    detail: detail,
    route: route,
  },
  render: (args) => ({
    props: args,
    template: slotTemplate,
  }),
};

//  [description] + [heading] dans les slots
const slotTemplate = `
<dsfr-tile
  [artworkDirPath] = "artworkDirPath"
  [artworkFilePath] = "artworkFilePath"
  [badges] = "badges"
  [detailBottom]="detail"
  [disabled] = "disabled"
  [enlargeLink] = "enlargeLink"
  [headingLevel] = "headingLevel"
  [horizontal] = "horizontal"
  [link] = "link"
  [linkTarget] = "linkTarget"
  [route] = "route"
  [routePath] = "routerLink"
  [routerLinkExtras] = "routerLinkExtras"
  [size] = "size"
  [tags] = "tags"
  >
    <ng-container heading>Slot heading</ng-container>
    <ng-container desc>
      <div><em>Slot description</em></div>
    </ng-container>
</dsfr-tile>
`;
