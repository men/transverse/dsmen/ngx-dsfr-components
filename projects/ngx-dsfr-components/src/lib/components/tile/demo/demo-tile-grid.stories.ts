import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrTileComponent } from '../tile.component';
import { DsfrTileModule } from '../tile.module';
import { DemoTileGridComponent } from './demo-tile-grid.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'COMPONENTS/Tile',
  component: DemoTileGridComponent,
  subcomponents: [DsfrTileComponent],
  decorators: [moduleMetadata({ imports: [DsfrTileModule] })],
  argTypes: {
    size: { control: { type: 'inline-radio' }, options: ['SM', 'MD'] },
  },
};
export default meta;
type Story = StoryObj<DemoTileGridComponent>;

// -- Grille de tuile --------------------------------------------------------------------------------------------------

export const Grid: Story = {
  decorators: dsfrDecorator('Grilles de tuiles'),
  args: {
    size: 'MD',
    horizontal: false,
  },
  parameters: {
    docs: {
      source: {
        code: `<div class="fr-mb-6v">
  <div class="fr-grid-row fr-grid-row--gutters">
    <div class="fr-col-12 fr-col-sm-6 fr-col-md-3 fr-col-lg-2">
      <dsfr-tile ...></dsfr-tile>
    </div>
    <div class="fr-col-12 fr-col-sm-6 fr-col-md-3 fr-col-lg-2">
      <dsfr-tile ...></dsfr-tile>
    </div>
    ...
  </div>
</div>`,
      },
    },
  },
};
