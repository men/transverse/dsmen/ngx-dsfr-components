import { expect, test } from '@playwright/test';
import { getSbFrame, goToComponentPage } from '../../shared/utils/e2e.utils';

test.describe('Tile', () => {
  test('contains fr-tile', async ({ page }) => {
    const frame = getSbFrame(page);
    const div = frame.locator('div.fr-tile');

    await goToComponentPage('tile', page, 'default');

    await expect(div).toBeEnabled();
  });

  test('have proper classes in horizontal mode', async ({ page }) => {
    const frame = getSbFrame(page);
    const div = frame.locator('div.fr-tile');

    await goToComponentPage('tile', page, 'horizontal');

    await expect(div).toBeEnabled();
    await expect(div).toHaveClass('fr-tile fr-enlarge-link fr-tile--horizontal');
  });

  test('perform navigation using enlargeLink', async ({ page }) => {
    const frame = getSbFrame(page);
    const dsfrTileDescription = frame.getByText('Lorem ipsum');
    const dsfrTileLink = frame.getByText('Intitulé de la tuile');

    await goToComponentPage('tile', page, 'default');

    try {
      await dsfrTileDescription.click({ timeout: 2000 });
      throw new Error('invalid test');
    } catch (error: any) {
      expect(error.message).toContain('locator.click: Timeout');
    }

    await dsfrTileLink.click();
    await expect(page).toHaveURL(/.*iframe.html\?viewMode=story&id=components-tile--default&globals=/);
  });
});
