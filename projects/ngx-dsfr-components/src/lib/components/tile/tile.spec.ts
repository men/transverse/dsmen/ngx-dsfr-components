import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DsfrTileComponent } from './tile.component';
import { DsfrTileModule } from './tile.module';

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-tile [link]="'#'"> </dsfr-tile>`,
})
class TestHostWithoutSlotComponent {
  @ViewChild(DsfrTileComponent)
  public tileComponent: DsfrTileComponent;
}

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-tile [link]="'#'">
    <ng-container heading>Slot heading</ng-container>
    <ng-container desc>
      <div><em>Slot description</em></div>
    </ng-container>
    <ng-container detail> Slot détail</ng-container>
  </dsfr-tile>`,
})
class TestHostComponent {
  @ViewChild(DsfrTileComponent)
  public tileComponent: DsfrTileComponent;
}

describe('DsfrTileComponentTest', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrTileModule],
      declarations: [TestHostComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(testHostComponent.tileComponent).toBeDefined();
  });

  it("should have 'fr-enlarge-link' !", () => {
    const tileElt = fixture.nativeElement.querySelector('.fr-tile');
    expect(tileElt).toBeDefined();
    expect(tileElt.classList).toContain('fr-enlarge-link');
    expect(tileElt.classList).not.toContain('fr-enlarge-button');
  });

  it("should have 'fr-enlarge-button' !", () => {
    const tileElt = fixture.nativeElement.querySelector('.fr-tile');
    testHostComponent.tileComponent.enlargeButton = true;
    fixture.detectChanges();
    expect(tileElt.classList).toContain('fr-enlarge-button');
  });

  it('should have heading as slot without link', () => {
    const tileElt = fixture.nativeElement.querySelector('.fr-tile__title');
    testHostComponent.tileComponent.link = '';
    fixture.detectChanges();
    expect(tileElt.querySelector('a')).toBeNull();
    expect(tileElt.textContent).toContain('Slot heading');
  });

  it('should have heading as slot and link', () => {
    const tileElt = fixture.nativeElement.querySelector('.fr-tile__title');
    fixture.detectChanges();
    expect(tileElt.querySelector('a').textContent).toContain('Slot heading');
  });

  it('should set heading as input', () => {
    const tileElt = fixture.nativeElement.querySelector('.fr-tile');
    testHostComponent.tileComponent.heading = 'Mon heading';
    fixture.detectChanges();
    expect(tileElt.textContent).toContain('Mon heading');
  });

  it('should set description as input', () => {
    const tileElt = fixture.nativeElement.querySelector('.fr-tile__desc');
    testHostComponent.tileComponent.description = 'Ma description';
    fixture.detectChanges();
    expect(tileElt.textContent).toContain('Ma description');
  });

  it('should have description as slot', () => {
    const tileElt = fixture.nativeElement.querySelector('.fr-tile__desc');
    fixture.detectChanges();
    expect(tileElt.textContent).toContain('Slot description');
  });

  it('should set detailBottom as input', () => {
    const tileElt = fixture.nativeElement.querySelector('.fr-tile__detail');
    testHostComponent.tileComponent.detailBottom = 'Mon détail';
    fixture.detectChanges();
    expect(tileElt.textContent).toContain('Mon détail');
  });

  it('should have detailBottom as slot', () => {
    const tileElt = fixture.nativeElement.querySelector('.fr-tile__detail');
    fixture.detectChanges();
    expect(tileElt.textContent).toContain('Slot détail');
  });
});

describe('DsfrTileComponentTest', () => {
  let testHostComponent: TestHostWithoutSlotComponent;
  let fixture: ComponentFixture<TestHostWithoutSlotComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrTileModule],
      declarations: [TestHostWithoutSlotComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestHostWithoutSlotComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should remove empty detail and description paragraphs', () => {
    fixture.detectChanges();

    const descElt = fixture.nativeElement.querySelector('.fr-tile__desc');
    const detailElt = fixture.nativeElement.querySelector('.fr-tile__detail');
    expect(descElt).toEqual(null);
    expect(detailElt).toEqual(null);
  });
});
