import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DsfrMimeTypeConst } from '../download';
import { DsfrTileComponent } from './tile.component';
import { DsfrTileModule } from './tile.module';

describe('Download DsfrTileComponentTest', () => {
  const TITRE = 'Télécharger le document XX';

  let fixture: ComponentFixture<DsfrTileComponent>;
  let tileComponent: DsfrTileComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrTileModule],
    }).compileComponents();
    fixture = TestBed.createComponent(DsfrTileComponent);
    tileComponent = fixture.componentInstance;
    tileComponent.heading = TITRE;
    tileComponent.download = true;
    tileComponent.link = 'files/lorem-ipsum.pdf';
    tileComponent.downloadSizeBytes = 77824;
    tileComponent.downloadMimeType = DsfrMimeTypeConst.APPLICATION_PDF;
    tileComponent.downloadAssessFile = false;
    tileComponent.downloadDirect = true; // Indique que le clic sur le lien déclenche le téléchargement
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(tileComponent).toBeDefined();
  });

  it('default', () => {
    const anchorElt = fixture.nativeElement.querySelector('.fr-tile__title a');
    expect(anchorElt).not.toBeNull();
    expect(anchorElt.textContent).toEqual(TITRE);

    const detailElt = fixture.nativeElement.querySelector('.fr-tile__detail');
    expect(detailElt.textContent).toContain('PDF - 76 ko'); // 77,12 ko en mode auto ???
  });

  it('target attribute', () => {
    const anchorElt = fixture.nativeElement.querySelector('a');
    let attribute = anchorElt.getAttribute('target');
    expect(attribute).toBeNull();

    tileComponent.linkTarget = '_blank';
    fixture.detectChanges();
    attribute = anchorElt.getAttribute('target');
    expect(attribute).not.toBeNull();
    expect(attribute).toEqual('_blank');
  });
});
