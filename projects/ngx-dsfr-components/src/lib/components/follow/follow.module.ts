import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrFormEmailModule } from '../../forms/form-email';
import { InputGroupComponent } from '../../shared';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrFollowLinkComponent } from './follow-link/follow-link.component';
import { DsfrFollowComponent } from './follow.component';

@NgModule({
  declarations: [DsfrFollowComponent, DsfrFollowLinkComponent],
  exports: [DsfrFollowComponent, DsfrFollowLinkComponent],
  imports: [CommonModule, FormsModule, DsfrFormEmailModule, InputGroupComponent, DsfrI18nPipe],
})
export class DsfrFollowModule {}
