import {
  AfterViewInit,
  Component,
  ContentChildren,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
  QueryList,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { DsfrFormEmailComponent } from '../../forms/form-email';
import { DomUtils, DsfrHeadingLevel, DsfrHeadingLevelConst, DsfrI18nService } from '../../shared';
import { DsfrFollowLinkComponent } from './follow-link/follow-link.component';
import { DsfrFollowType, DsfrFollowTypeConst } from './follow.model';

@Component({
  selector: 'dsfr-follow',
  templateUrl: './follow.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrFollowComponent implements AfterViewInit, OnDestroy {
  /** @internal */
  @ViewChild(DsfrFormEmailComponent) emailComponent: DsfrFormEmailComponent;

  /**
   * Niveau du titre, h2 par défaut
   */
  @Input() headingLevel: DsfrHeadingLevel = DsfrHeadingLevelConst.H2;

  /** Description (pour certains types seulement) optionnelle. */
  @Input() description = '';

  /** Email par défaut. */
  @Input() email = '';

  /** Message d'erreur concernant l'email. */
  @Input() emailError = '';

  /** Indique que l'inscription a bien été prise en compte. */
  @Input() registered = false;

  /** Type du composant, réseaux sociaux seuls par défaut  */
  @Input() type: DsfrFollowType = DsfrFollowTypeConst.NETWORKS;

  /** Abonnement si l'adresse est valide. */
  @Output() subscribe = new EventEmitter<string>();

  /** @internal */
  @ContentChildren(DsfrFollowLinkComponent) followLinkComponent: QueryList<DsfrFollowLinkComponent>;

  /* Tous les types possibles du composant. */
  /** @internal */ followType = DsfrFollowTypeConst;

  private subscription = new Subscription();

  /** @internal */
  constructor(
    public i18n: DsfrI18nService,
    private _elementRef: ElementRef,
  ) {
    this.type = DsfrFollowTypeConst.NETWORKS;
  }

  ngAfterViewInit(): void {
    DomUtils.surroundChildWithli(this._elementRef, 'dsfr-follow-link');

    const followLinkChanges$ = this.followLinkComponent.changes.subscribe(() => {
      DomUtils.removeEmptyLi(this._elementRef);
      DomUtils.surroundChildWithli(this._elementRef, 'dsfr-follow-link');
    });

    this.subscription.add(followLinkChanges$);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /** @internal */
  onSubscribe(email: string) {
    if (this.emailComponent?.isValid()) {
      this.subscribe.emit(email);
    }
  }
}
