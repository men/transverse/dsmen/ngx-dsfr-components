import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { DsfrAccordionModule } from '../accordion';
import { DsfrAccordionsGroupComponent } from './accordions-group.component';

require('../../../../../../node_modules/@gouvfr/dsfr/dist/dsfr.module.js');
declare function dsfr(elem: HTMLElement): any;

@Component({
  selector: `edu-host-component`,
  template: `<dsfr-accordions-group>
    <dsfr-accordion heading="Intitulé accordéon">{{ data }}</dsfr-accordion>
    <dsfr-accordion heading="Intitulé accordéon">Données de tests ...</dsfr-accordion>
    <dsfr-accordion [expanded]="true" heading="Accordéon">Mon contenu</dsfr-accordion>
  </dsfr-accordions-group>`,
})
class TestHostComponent {
  @ViewChild(DsfrAccordionsGroupComponent)
  public accordions: DsfrAccordionsGroupComponent;

  data = `<div> <p> Test </p> </div>`;
}

describe('DsfrAccordionsGroupComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [DsfrAccordionModule],
      declarations: [DsfrAccordionsGroupComponent, TestHostComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    //@ts-ignore
    //dsfr.mode = 'manual';
    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should init accordions with section tag ', () => {
    fixture.detectChanges();
    const accordionGroup = fixture.nativeElement.querySelector('.fr-accordions-group');
    expect(accordionGroup.children.length).toEqual(3);
  });

  xit('should set first accordion on expanded', fakeAsync(() => {
    //@ts-ignore
    dsfr.start();
    fixture.detectChanges();
    const accordionsList = fixture.nativeElement.querySelector('.fr-accordions-group').children;
    const buttonEl = accordionsList[1].querySelector('.fr-accordion__btn');
    expect(buttonEl.getAttribute('aria-expanded')).toEqual('false');
    buttonEl.click();
    tick(100); // delay to let dsfr execute
    fixture.detectChanges();
    expect(buttonEl.getAttribute('aria-expanded')).toEqual('true');
    expect(accordionsList[0].querySelector('.fr-accordion__btn').getAttribute('aria-expanded')).toEqual('false');

    //@ts-ignore
    dsfr.stop();
  }));

  xit('should have accordions ungroup', fakeAsync(() => {
    testHostComponent.accordions.ungroup = true;
    //@ts-ignore
    dsfr.start();
    fixture.detectChanges();
    const accordionsList = fixture.nativeElement.querySelector('.fr-accordions-group').children;
    const buttonEl = accordionsList[1].querySelector('.fr-accordion__btn');
    buttonEl.click();
    tick(200); // delay to let dsfr execute
    fixture.detectChanges();
    expect(accordionsList[1].querySelector('.fr-accordion__btn').getAttribute('aria-expanded')).toEqual('false');
    expect(accordionsList[2].querySelector('.fr-accordion__btn').getAttribute('aria-expanded')).toEqual('true');
  }));
});
