import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'dsfr-accordions-group',
  templateUrl: './accordions-group.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrAccordionsGroupComponent {
  /** Dissocie le groupe d'accordéon pour en avoir plusieurs d'ouvert à la fois.*/
  @Input() ungroup = false;
}
