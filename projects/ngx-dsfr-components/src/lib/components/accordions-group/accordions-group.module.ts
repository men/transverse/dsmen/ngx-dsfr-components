import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DsfrAccordionsGroupComponent } from './accordions-group.component';

@NgModule({
  declarations: [DsfrAccordionsGroupComponent],
  exports: [DsfrAccordionsGroupComponent],
  imports: [CommonModule],
})
export class DsfrAccordionsGroupModule {}
