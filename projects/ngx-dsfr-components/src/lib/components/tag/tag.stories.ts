import { DSFR_TAG } from '.storybook/dsfr-themes';
import { argEventEmitter, dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrLinkModule } from '../link';
import { DsfrTooltipDirective } from '../tooltip';
import { DsfrTagComponent, DsfrTagModeConst } from './index';

const meta: Meta = {
  title: 'COMPONENTS/Tag',
  component: DsfrTagComponent,
  decorators: [moduleMetadata({ imports: [DsfrLinkModule, DsfrTooltipDirective, DsfrI18nPipe] })],
  parameters: { actions: false },
  argTypes: {
    mode: { control: { type: 'inline-radio' }, options: Object.values(DsfrTagModeConst) },
    customClass: { control: { type: 'select' }, options: [''].concat(Object.values(DSFR_TAG)) },
    icon: {
      control: { type: 'select' },
      options: ['', 'ri-home-4-line', 'fr-icon-arrow-left-line'],
    },
    tagSelect: { control: argEventEmitter },
  },
};
export default meta;
type Story = StoryObj<DsfrTagComponent>;

const label = 'Label tag';

/** Default */
export const Default: Story = {
  decorators: dsfrDecorator('Tag non cliquable'),
  args: {
    label: label,
  },
};

/** IconOnLeft */
export const IconOnLeft: Story = {
  decorators: dsfrDecorator('Tag non cliquable icône à gauche'),
  args: {
    label: label,
    icon: 'fr-icon-arrow-left-line',
  },
};

/** Small */
export const Small: Story = {
  decorators: dsfrDecorator('Tag non cliquable taille SM'),
  args: {
    label: label,
    small: true,
  },
};

/** Clickable */
export const Clickable: Story = {
  decorators: dsfrDecorator('Tag cliquable'),
  args: {
    label: label,
    link: '#',
  },
};

/** ClickableCustom */
export const ClickableCustom: Story = {
  decorators: dsfrDecorator('Tag cliquable accentué'),
  args: {
    label: label,
    route: '/path',
    customClass: 'fr-tag--green-emeraude',
  },
};

/** ClickableDisabled */
export const ClickableDisabled: Story = {
  decorators: dsfrDecorator('Tag cliquable désactivé'),
  args: {
    label: label,
    mode: 'clickable',
    disabled: true,
  },
};

/** Selectable */
export const Selectable: Story = {
  decorators: dsfrDecorator('Tag sélectionnable'),
  args: {
    id: 'selectable',
    label: label,
    mode: DsfrTagModeConst.SELECTABLE,
    selected: false,
  },
};

/** Selected */
export const Selected: Story = {
  decorators: dsfrDecorator('Tag sélectionné'),
  args: {
    id: 'selected',
    label: label,
    mode: DsfrTagModeConst.SELECTABLE,
    selected: true,
  },
};

/** SelectableDisabled */
export const SelectableDisabled: Story = {
  decorators: dsfrDecorator('Tag sélectionnable désactivé'),
  args: {
    label: label,
    mode: DsfrTagModeConst.SELECTABLE,
    disabled: true,
  },
};

/** Deletable */
export const Deletable: Story = {
  decorators: dsfrDecorator('Tag supprimable'),
  args: {
    label: label,
    mode: 'deletable',
  },
};

/** DeletableDisabled */
export const DeletableDisabled: Story = {
  decorators: dsfrDecorator('Tag supprimable désactivé'),
  args: {
    label: label,
    mode: 'deletable',
    disabled: true,
  },
};

/** Tooltip */
export const Tooltip: Story = {
  decorators: dsfrDecorator('Tooltip'),
  args: {
    label: label,
    tooltipMessage: 'Lorem ipsum dolor sit amet, consectetur adipiscing, incididunt, ut labore et dolore magna aliqua.',
  },
  render: (args) => ({
    props: args,
    template: `<dsfr-tag [label]="label"  [tooltip]="tooltipMessage"></dsfr-tag>`,
  }),
};
