import { expect, test } from '@playwright/test';
import { getSbFrame, goToComponentPage } from '../../shared/utils/e2e.utils';

test('tag.default', async ({ page }) => {
  await goToComponentPage('tag', page, 'default');
  const frame = getSbFrame(page);

  const paragraph = frame.locator('p.fr-tag');
  await expect(paragraph).toBeEnabled();
});

/* selectable */
test('tag.selectable', async ({ page }) => {
  await goToComponentPage('tag', page, 'selectable');
  const frame = getSbFrame(page);

  const button = frame.locator('button.fr-tag');
  await expect(button).toBeEnabled();
  await expect(button).toHaveAttribute('aria-pressed', 'false');
  await expect(button).toHaveAttribute('aria-label', 'Sélectionner le filtre Label tag');
});

test('tag.selected', async ({ page }) => {
  await goToComponentPage('tag', page, 'selected');
  const frame = getSbFrame(page);

  const button = frame.locator('button.fr-tag');
  await expect(button).toBeEnabled();
  await expect(button).toHaveAttribute('aria-pressed', 'true');
});

test('tag.clickable-with-path', async ({ page }) => {
  await goToComponentPage('tag', page, 'clickable');
  const frame = getSbFrame(page);

  const anchor = frame.locator('a.fr-tag');
  await expect(anchor).toBeEnabled();
});

/* custom */
test('tag.custom', async ({ page }) => {
  await goToComponentPage('tag', page, 'clickable-custom');
  const frame = getSbFrame(page);

  const anchor = frame.locator('a.fr-tag');
  await expect(anchor).toBeEnabled();
  await expect(anchor).toHaveClass('fr-tag fr-tag--green-emeraude');
});

/* deletable */
test('tag.deletable', async ({ page }) => {
  await goToComponentPage('tag', page, 'deletable');
  const frame = getSbFrame(page);

  const button = frame.locator('button.fr-tag');
  await expect(button).toBeEnabled();
  await expect(button).toHaveClass('fr-tag fr-tag--dismiss');
});

test('tag.with-icon', async ({ page }) => {
  await goToComponentPage('tag', page, 'icon-on-left');
  const frame = getSbFrame(page);

  const paragraph = frame.locator('p.fr-tag');
  await expect(paragraph).toBeEnabled();
  await expect(paragraph).toHaveClass('fr-tag fr-tag--icon-left fr-icon-arrow-left-line');
});

test('tag.small', async ({ page }) => {
  await goToComponentPage('tag', page, 'small');
  const frame = getSbFrame(page);

  const paragraph = frame.locator('p.fr-tag');
  await expect(paragraph).toBeEnabled();
  await expect(paragraph).toHaveClass('fr-tag fr-tag--sm');
});
