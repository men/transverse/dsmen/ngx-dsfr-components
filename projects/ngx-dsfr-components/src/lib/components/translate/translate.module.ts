import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrTranslateComponent } from './translate.component';

@NgModule({
  declarations: [DsfrTranslateComponent],
  exports: [DsfrTranslateComponent],
  imports: [CommonModule, FormsModule, DsfrI18nPipe],
})
export class DsfrTranslateModule {}
