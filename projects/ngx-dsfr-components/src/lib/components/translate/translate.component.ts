import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, EventEmitter, Inject, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { LangService, newUniqueId } from '../../shared';
import { DsfrLang } from './lang.model';

@Component({
  selector: 'dsfr-translate',
  templateUrl: './translate.component.html',
  encapsulation: ViewEncapsulation.None,
})
/** Composant de sélection de la langue. */
export class DsfrTranslateComponent implements OnInit {
  /** Liste des langues proposées à l'utilisateur. */
  @Input() languages: DsfrLang[] = [];

  /** Sans bordure si 'false' */
  @Input() outline = true;

  /** Événement émit uniquement si la langue sélectionnée ne contient pas de href. Il contient le code de la langue. */
  @Output() langChange = new EventEmitter<string>();

  /** @internal */
  collapseId: string;

  /** @internal */
  constructor(
    private langService: LangService,
    private elementRef: ElementRef,
    @Inject(DOCUMENT) private readonly document: Document,
  ) {}

  get currentLangCode(): string {
    return this.langService.lang;
  }

  get currentLabel(): string {
    let label = '';
    const language = this.getCurrentLang();

    if (language && language.label) {
      label = language.label;
    }

    return label;
  }

  /** Force la langue courante. */
  @Input() set currentLangCode(value: string) {
    this.langService.lang = value; // Informe le service
    this.setDocLang(value); // Positionne la langue dans le document html
  }

  /** @internal */
  ngOnInit() {
    this.collapseId = newUniqueId();
  }

  /** @internal */
  onClick(event: Event, lang: DsfrLang) {
    this.currentLangCode = lang.value;

    if (!lang.link) {
      event.preventDefault();
      // Si on ne déclenche pas le href, on est obligé de fermer manuellement le composant
      this.collapse();
      this.langChange.emit(lang.value);
    }
  }

  private getCurrentLang(): DsfrLang | undefined {
    return this.languages.find((l) => l.value === this.currentLangCode);
  }

  private collapse() {
    const nativeElt = this.elementRef.nativeElement;
    const buttonElt = nativeElt.querySelector('button');
    if (buttonElt) {
      buttonElt.setAttribute('aria-expanded', 'false');
      buttonElt.setAttribute('data-fr-js-collapse-button', true);
    }
  }

  /** Positionne l'attribut lang dans la balise html, ex : lang="fr" */
  private setDocLang(codeLang: string) {
    this.document?.documentElement.setAttribute('lang', codeLang);
  }
}
