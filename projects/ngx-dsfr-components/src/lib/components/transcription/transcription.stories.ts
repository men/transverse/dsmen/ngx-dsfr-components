import { Meta, StoryObj } from '@storybook/angular';
import { DsfrTranscriptionComponent } from './index';
import { gridDecoratorLG, titleDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'COMPONENTS/Transcription',
  component: DsfrTranscriptionComponent,
};
export default meta;
type Story = StoryObj<DsfrTranscriptionComponent>;

const heading = 'Titre de la vidéo';
const loremIpsum =
  'Lorem ipsum dolor sit amet, consectetur adipiscing, <a href="https://www.systeme-de-design.gouv.fr/" target="_blank">link test</a> incididunt, ut labore et dolore magna aliqua. Vitae sapien pellentesque habitant morbi tristique senectus et. Diam maecenas sed enim ut. Accumsan lacus vel facilisis volutpat est. Ut aliquam purus sit amet luctus. Lorem ipsum dolor sit amet consectetur adipiscing elit ut.';

export const Default: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Transcription avec zone d’action')],
  args: {
    heading: heading,
    content: loremIpsum,
  },
};

export const Slot: Story = {
  decorators: [gridDecoratorLG, titleDecorator('Transcription avec contenu HTML')],
  args: {
    heading: heading,
  },
  render: (args) => ({
    props: args,
    template: `
<dsfr-transcription [content]="content" [heading]="heading">
  <div>
    <p>${loremIpsum}</p>
    <ul>
      <li>list item</li>
      <li>list item</li>
      <li>list item</li>
    </ul>
  </div>
</dsfr-transcription>
`,
  }),
};
