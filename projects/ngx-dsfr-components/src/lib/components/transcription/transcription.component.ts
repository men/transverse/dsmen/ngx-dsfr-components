import { AfterContentInit, AfterViewInit, Component, ElementRef, Input, ViewEncapsulation } from '@angular/core';
import { newUniqueId } from '../../shared';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';

@Component({
  selector: 'dsfr-transcription',
  templateUrl: './transcription.component.html',
  styleUrls: ['./transcription.component.scss'],
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [DsfrI18nPipe],
})
export class DsfrTranscriptionComponent implements AfterContentInit, AfterViewInit {
  /**
   * Contenu de la transcription, prioritaire sur le slot <ng-content /> sans sélecteur.
   * Le slot permet d'avoir un contenu plus riche.
   */
  @Input() content: string;

  /** Titre du média de niveau h1. */
  @Input() heading: string;

  /** @internal */ collapseId: string;
  /** @internal */ dialogId: string;
  /** @internal */ titleId: string;
  /** @internal */ roleDialog = false;

  constructor(private elementRef: ElementRef) {}

  ngAfterContentInit(): void {
    this.collapseId = newUniqueId();
    this.dialogId = newUniqueId();
    this.titleId = newUniqueId();
  }

  /** @internal */
  ngAfterViewInit() {
    // Pour pallier `.fr-content-media + .fr-transcription`
    const previousElt = this.elementRef.nativeElement.previousElementSibling;
    if (previousElt && previousElt.classList.contains('fr-content-media')) {
      const transcriptionElt = this.elementRef.nativeElement.querySelector('.fr-transcription');
      transcriptionElt?.classList.add('follows-content');
    }
  }

  /** @internal */
  enlargeScript() {
    this.roleDialog = !this.roleDialog;
  }
}
