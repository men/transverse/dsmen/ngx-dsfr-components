export { DsfrTableLegacyComponent } from './table-legacy.component';
export { DsfrDataTable, DsfrTableColumn } from './table-legacy.model';
export { DsfrTableModule } from './table.module';
