import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Component } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DsfrMenu } from './menu.model';
import { DsfrSidemenuModule } from './sidemenu.module';

@Component({
  selector: `edu-host-component`,
  template: ` <dsfr-sidemenu [menu]="items" controlId="monId"></dsfr-sidemenu> `,
})
class TestHostComponent {
  public items: DsfrMenu = {
    items: [
      {
        label: 'Menu 1',
        route: 'menu1',
        subItems: [
          { label: 'Sub item 1-1', route: 'subitem1-1' },
          { label: 'Sub item 1-2', route: 'subitem1-2' },
        ],
      },
      {
        label: 'Menu 2',
        route: 'menu2',
        active: true,
        subItems: [
          { label: 'Sub item 2-1', route: 'subitem2-1' },
          { label: 'Sub item 2-2', route: 'subitem2-2', active: true },
        ],
      },
    ],
  };
}

describe('DsfrSideMenu', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterModule.forRoot([]), DsfrSidemenuModule],
      declarations: [TestHostComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should init menu without title ', () => {
    const tileElt = fixture.nativeElement.querySelector('.fr-sidemenu__title');
    const navEl = fixture.nativeElement.querySelector('nav');

    expect(tileElt).toBeNull();
    expect(navEl.getAttribute('aria-label')).toContain('Menu latéral');
  });

  it('should init menu with title and corrects ids', () => {
    testHostComponent.items.title = 'Mon titre';
    fixture.detectChanges();

    const tileElt = fixture.nativeElement.querySelector('.fr-sidemenu__title');
    const navEl = fixture.nativeElement.querySelector('nav');

    expect(tileElt.textContent).toContain('Mon titre');
    expect(tileElt.getAttribute('id')).toEqual('sidemenu-title-monId');
    expect(navEl.getAttribute('aria-label')).toBeNull();
    expect(navEl.getAttribute('aria-labelledby')).toEqual('sidemenu-title-monId');
  });
});
