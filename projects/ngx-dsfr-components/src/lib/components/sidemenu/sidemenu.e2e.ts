import { expect, test } from '@playwright/test';
import { getSbFrame, goToComponentPage } from '../../shared/utils/e2e.utils';

/** Default */
test('sidemenu.default', async ({ page }) => {
  const frame = getSbFrame(page);
  const nav = frame.locator('nav.fr-sidemenu');
  const menuInner = nav.locator('div.fr-sidemenu__inner');
  const menuCollapse = menuInner.locator('div.fr-collapse');
  const menuTitle = nav.getByText('Titre du menu');
  const item = menuCollapse.getByText('Niveau 2 actif + icône');

  await goToComponentPage('sidemenu', page);

  await expect(nav).toBeVisible();
  await expect(nav).toHaveAttribute('role', 'navigation');
  await expect(menuInner).toBeVisible();
  await expect(menuInner).toBeVisible();
  await expect(menuTitle).toBeVisible();
  await expect(item).not.toBeVisible();
});

test.describe('specific viewport mobile', () => {
  test.use({ viewport: { width: 350, height: 400 } });

  test('sidemenu.default', async ({ page }) => {
    const frame = getSbFrame(page);
    const nav = frame.locator('nav.fr-sidemenu');
    const btnMenu = frame.locator('.fr-sidemenu__btn').first();
    const menuInner = nav.locator('div.fr-sidemenu__inner');
    const firstLevelCollapse = menuInner.locator('div.fr-collapse').first();
    const titleMenu = nav.getByText('Titre du menu');

    await goToComponentPage('sidemenu', page);

    await expect(nav).toBeVisible();
    await expect(btnMenu).toHaveAttribute('aria-controls', 'sidemenu-default');
    await expect(firstLevelCollapse).toHaveAttribute('id', 'sidemenu-default');
    await expect(titleMenu).not.toBeVisible();

    await btnMenu.click();

    await expect(btnMenu).toHaveAttribute('aria-expanded', 'true');
    await expect(firstLevelCollapse).toHaveClass('fr-collapse fr-collapse--expanded');
    await expect(titleMenu).toBeVisible();
  });
});
