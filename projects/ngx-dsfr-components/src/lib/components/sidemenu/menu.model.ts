import { DsfrLink } from '../../shared';

/** Menu */
export interface DsfrMenu {
  /** Titre du menu (optionnel) */
  title?: string;
  /** Entrées du menu */
  items: DsfrMenuItem[];
}

/** Entrée de menu */
export interface DsfrMenuItem extends DsfrLink {
  /** Indique le titre de rubrique */
  title?: string;

  /** Indique l'identifiant de la zone de menu contrôlée par cet entête de menu, généré automatiquement par défaut */
  controlId?: string;

  /* Entrées du menu (liens directs ou sous-menus si plusieurs niveaux) */
  subItems?: DsfrMenuItem[];
}
