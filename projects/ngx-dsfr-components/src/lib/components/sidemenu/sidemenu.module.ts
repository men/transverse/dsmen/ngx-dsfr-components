import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DsfrDisableRouterLinkDirective } from '../../shared/directives/disable-router-link.directive';
import { ItemLinkComponent } from '../link/item-link.component';
import { DsfrSidemenuComponent } from './sidemenu.component';

@NgModule({
  declarations: [DsfrSidemenuComponent],
  exports: [DsfrSidemenuComponent],
  imports: [CommonModule, RouterModule, ItemLinkComponent, DsfrDisableRouterLinkDirective],
})
export class DsfrSidemenuModule {}
