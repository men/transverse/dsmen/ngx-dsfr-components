import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrLinkModule } from '../link';
import { DsfrConsentBannerComponent } from './consent-banner.component';
import { DsfrConsentManagerModule } from './consent-manager/consent-manager.module';

@NgModule({
  declarations: [DsfrConsentBannerComponent],
  exports: [DsfrConsentBannerComponent],
  imports: [CommonModule, FormsModule, DsfrConsentManagerModule, DsfrLinkModule, DsfrI18nPipe],
})
export class DsfrConsentBannerModule {}
