import { expect, test } from '@playwright/test';

test('consent modal attributes', async ({ page }) => {
  await page.goto('?path=/story/components-consent-banner--default');
  const frame = page.frameLocator('#storybook-preview-iframe');
  const dialog = await frame.locator('dialog');
  const openBtn = await frame.locator('button[data-fr-opened="false"]');

  await expect(openBtn).toHaveAttribute('data-fr-js-modal-button', 'true');
  await expect(openBtn).toHaveAttribute('aria-controls', 'consent-modal-id');

  await openBtn.press('Enter');
  await expect(dialog).toBeVisible();
  await expect(dialog).toHaveAttribute('id', 'consent-modal-id');
});

test('consent modal finalities status', async ({ page }) => {
  await page.goto('?path=/story/components-consent-banner--default');
  const frame = page.frameLocator('#storybook-preview-iframe');
  const dialog = await frame.locator('dialog');
  const openBtn = await frame.locator('button[data-fr-opened="false"]');

  await openBtn.press('Enter');

  await expect(dialog).toBeVisible();

  const labelAcceptAll = await dialog.locator('label[for="consent-all-accept"]');
  const acceptAll = await dialog.locator('#consent-all-accept');

  const finality1 = await dialog.locator('#consent-finality-0-accept');
  const subFinalities = await finality1.locator('#finality-0-collapse > input').all();

  const labelSubFinalityRefuse = await dialog.locator('label[for="consent-finality-0-sub-1-refuse"]');
  const labelSubFinalityAccept = await dialog.locator('label[for="consent-finality-0-sub-1-accept"]');

  const showSubFinalitiesButton = await dialog.locator('button[aria-controls="finality-0-collapse"]');

  // Enable all finalities
  await labelAcceptAll.click();

  await expect(finality1).toBeChecked();

  for (let el = 0; el < subFinalities.length; el++) {
    await expect(subFinalities[el]).toBeChecked();
  }

  // Disable subfinality 1
  await showSubFinalitiesButton.click(); // show subfinalites part
  await labelSubFinalityRefuse.click();

  await expect(finality1).not.toBeChecked();
  await expect(acceptAll).not.toBeChecked();

  // Enable subfinality 1
  await labelSubFinalityAccept.click();

  await expect(finality1).toBeChecked();
  await expect(acceptAll).toBeChecked();
});
