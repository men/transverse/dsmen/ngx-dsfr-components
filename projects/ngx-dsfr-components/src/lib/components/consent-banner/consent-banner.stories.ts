import { argEventEmitter, dsfrDecorator } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrLinkModule } from '../link';
import { DsfrConsentManagerModule } from './consent-manager/consent-manager.module';
import { DsfrConsentBannerComponent, DsfrFinality } from './index';

const meta: Meta = {
  title: 'COMPONENTS/Consent banner',
  component: DsfrConsentBannerComponent,
  decorators: [moduleMetadata({ imports: [FormsModule, DsfrConsentManagerModule, DsfrLinkModule, DsfrI18nPipe] })],
  argTypes: {
    acceptAllSelect: { control: argEventEmitter },
    refuseAllSelect: { control: argEventEmitter },
    customizeSelect: { control: argEventEmitter },
    confirmCustomizeSelect: { control: argEventEmitter },
    finalityChange: { control: argEventEmitter },
    rgpdRouteSelect: { control: argEventEmitter },
  },
};
export default meta;
type Story = StoryObj<DsfrConsentBannerComponent>;

const finalities: DsfrFinality[] = [
  {
    name: 'Fonctionnalité 1',
    subFinalities: [{ name: 'Fonction 1' }, { name: 'Fonction 2' }, { name: 'Fonction 3' }],
  },
  { name: 'Fonctionnalité 2', description: ' Ce service peut déposer 6 cookies.' },
  { name: 'Fonctionnalité 3' },
  { name: 'Fonctionnalité 4 - Exemption CNIL', exempt: true },
  {
    name: 'Fonctionnalité 5',
    disabled: true,
    description:
      ' Ce site utilise des cookies nécessaires à son bon fonctionnement qui ne peuvent pas être désactivés. ',
  },
];

export const Default: Story = {
  decorators: dsfrDecorator('Gestionnaire de consentement'),
  args: {
    heading: 'A propos des cookies sur nomdusite.fr',
    finalities: finalities,
    rgpdLink: 'https://www.transformation.gouv.fr/donnees-personnelles-et-cookies',
    rgpdLinkTarget: '_blank',
  },
  parameters: { docs: { story: { inline: false, height: 400 } } },
};
