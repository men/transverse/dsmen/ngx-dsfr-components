import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { NavigationExtras } from '@angular/router';
import { DsfrLinkTarget } from '../../shared';
import { DsfrFinality } from './consent-banner.model';
import { CONSENT_MODAL_ID } from './consent-manager/consent-manager.component';

@Component({
  selector: 'dsfr-consent-banner',
  templateUrl: './consent-banner.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrConsentBannerComponent {
  /**
   * URL de présentation des données personnelles. Utilisé si welcome n'est pas renseigné
   * RGPD : Règlement Général sur la Protection des Données
   */
  @Input() rgpdLink = 'https://www.transformation.gouv.fr/donnees-personnelles-et-cookies';

  /** @since 1.7 Target du lien. Target par défaut de l'application si la propriété est non renseignée. */
  @Input() rgpdLinkTarget?: DsfrLinkTarget | undefined;

  /** @since 1.7 Path interne. Exclusif avec link et routerLink */
  @Input() rgpdRoute?: string;

  /** @since 1.7 Path angular géré en tant que directive routerLink. Exclusif avec link et route. */
  @Input() rgpdRouterLink?: string | string[];

  /** @since 1.7 RouterLink : classe utilisée pour la directive routerLink active. */
  @Input() rgpdRouterLinkActive?: string | string[];

  /** @since 1.7 RouterLink : valeurs additionnelles de navigation pour le routerLink (queryParams, state etc.) */
  @Input() rgpdRouterLinkExtras?: NavigationExtras;

  /** Titre de la bannière */
  @Input() heading: string;

  /**
   * Modèle de présentation des finalités.
   */
  @Input() finalities: DsfrFinality[];

  /** Sélection bouton Tout accepter*/
  @Output() acceptAllSelect = new EventEmitter<void>();

  /** Sélection bouton Tout refuser */
  @Output() refuseAllSelect = new EventEmitter<void>();

  /** Sélection bouton Personnaliser */
  @Output() customizeSelect = new EventEmitter<void>();

  /** Validation de la personnalisation */
  @Output() confirmCustomizeSelect = new EventEmitter<DsfrFinality[]>();

  /** Sélection de Accepter ou Refuser pour une finalité */
  @Output() finalityChange = new EventEmitter<DsfrFinality>();

  /** @since 1.7 Signale quand la route rgdp est sélectionnée. */
  @Output() rgpdRouteSelect = new EventEmitter<string>();

  /** Description de bienvenue. Utilisation du texte par défaut si non renseigné */
  @Input() welcome: string;

  /** Identifiant de la modale ('consent-modal' par défaut) */
  @Input() modalId: string = CONSENT_MODAL_ID;

  /** @deprecated (@since 1.7) utiliser `rgdpLink` à la place. */
  get rgpdUrl(): string {
    return this.rgpdLink;
  }

  /** @deprecated (@since 1.7) utiliser 'heading' à la place. */
  get title(): string {
    return this.heading;
  }

  /** @deprecated (@since 1.7) utiliser `rgdpLink` à la place. */
  @Input() set rgpdUrl(value: string) {
    this.rgpdLink = value;
  }

  /** @deprecated (@since 1.7) utiliser `heading` à la place. */
  @Input() set title(value: string) {
    this.heading = value;
  }

  /**
   * Accepter tout
   * @internal
   */
  onAcceptAll(): void {
    this.acceptAllSelect.emit();
  }

  /**
   * Refuser tout
   * @internal
   */
  onRefuseAll(): void {
    this.refuseAllSelect.emit();
  }

  /**
   * Changement de sélection accept/refuse d'une finalité ou sous-finalité.
   * @internal
   */
  onFinalityChange(finality: DsfrFinality): void {
    this.finalityChange.emit(finality);
  }

  /**
   * Ouverture du panneau de personnalisation des finalités.
   * @internal
   */
  onCustomizeSelect(): void {
    this.customizeSelect.emit();
  }

  /**
   * Confirmation de la personnalisation des finalités.
   * @param finalities liste des finalités
   * @internal
   */
  onConfirmCustomize(finalities: DsfrFinality[]): void {
    this.confirmCustomizeSelect.emit(finalities);
  }

  /** @since 1.7 Emet un événement lors de la sélection du lien RGPD si l'accès aux RGPD est fait via la propriété 'route'. */
  /** @internal */
  onRgpdSelect(): void {
    // Relais. Signale quand la route rgdp est sélectionnée
    if (this.rgpdRoute) this.rgpdRouteSelect.emit(this.rgpdRoute);
  }
}
