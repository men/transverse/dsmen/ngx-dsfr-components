import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrI18nPipe } from '../../../shared/i18n/i18n.pipe';
import { DsfrLinkModule } from '../../link';
import { ConsentManagerComponent } from './consent-manager.component';
import { ConsentServiceComponent } from './consent-service/consent-service.component';

@NgModule({
  declarations: [ConsentManagerComponent, ConsentServiceComponent],
  exports: [ConsentManagerComponent],
  imports: [CommonModule, FormsModule, DsfrLinkModule, DsfrI18nPipe],
})
export class DsfrConsentManagerModule {}
