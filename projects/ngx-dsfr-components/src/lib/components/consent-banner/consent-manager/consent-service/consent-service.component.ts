import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { DsfrFinality, DsfrSubFinality } from '../../consent-banner.model';

@Component({
  selector: 'edu-consent-service',
  templateUrl: './consent-service.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class ConsentServiceComponent implements OnInit {
  /**
   * Le modèle de présentation.
   */
  @Input() finality: DsfrFinality;
  /**
   * L'index de la finalité au sein du panneau de gestion.
   */
  @Input() idxFinality: number;
  /**
   * Émis lors l'utilisateur accepte/refuse la finalité.
   */
  @Output() finalityChange = new EventEmitter<DsfrFinality>(); // changement accept/refuse d'une finalité ou sous-finalité

  _acceptAll: boolean | undefined;
  fieldsetLabelledBy: string;
  buttonDescribedBy = 'finality-{{idxFinality}}-legend';
  buttonControls = 'finality-{{idxFinality}}-collapse';

  /**
   * Au changement utilisateur accepter tout / refuser tout, mettre a jour la finalité et finalités enfants
   * Pas de mise à jour si la finalité est en disabled ou exempt (toujours accept=true)
   * @param value true (accept all), false (refuse all), undefined (pas de sélection)
   */
  @Input() set acceptAll(value: boolean | undefined) {
    this._acceptAll = value;
    if (this._acceptAll !== undefined && !this.finality.disabled && !this.finality.exempt) {
      this.finality.accept = this._acceptAll;
      this.finality.subFinalities?.map((f) => (f.accept = this._acceptAll));
    }
  }

  ngOnInit() {
    const index = this.idxFinality;
    this.fieldsetLabelledBy = `finality-${index}-legend finality-${index}}-desc`;
    this.buttonDescribedBy = `finality-${index}-legend`;
    this.buttonControls = `finality-${index}-collapse`;
    if (this.finality.exempt || this.finality.disabled) {
      this.finality.accept = true;
    }
  }

  /**
   * Changement accept/refuse de la finalité, mettre a jour les sous-finalités enfant
   * @param subFinality sous-finalité concernée
   */
  onFinalityRadioChange() {
    if (this.finality.subFinalities) {
      this.finality.subFinalities.map((f: DsfrSubFinality) => (f.accept = this.finality.accept));
    }
    this.finalityChange.emit(this.finality);
  }

  /**
   * Changement accept/refuse d'une sous finalité, remettre la finalité parente en accept undefined si necessaire
   * @param subFinality sous-finalité concernée
   */
  onSubFinalityRadioChange(subFinality: DsfrSubFinality) {
    if (
      (this.finality.accept && subFinality.accept === false) ||
      (this.finality.accept === false && subFinality.accept)
    ) {
      this.finality.accept = undefined;
    }

    this.finalityChange.emit(this.finality);
  }
}
