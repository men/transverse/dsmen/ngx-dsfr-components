import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { DsfrNavigation } from '../../../shared';
import { DsfrFinality } from '../consent-banner.model';

export const CONSENT_MODAL_ID = 'consent-modal-id';

@Component({
  selector: 'edu-consent-manager',
  templateUrl: './consent-manager.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class ConsentManagerComponent implements OnInit {
  /** URL de présentation des données personnelles. */
  @Input() rgpdNavigation: DsfrNavigation;

  /** Modèle de présentation : liste de finalités. */
  @Input() finalities: DsfrFinality[];

  /** Confirmation de la personnalisation des finalités. */
  @Output() confirmSelect = new EventEmitter<DsfrFinality[]>();

  /** Changement selection accept/refuse d'une finalité ou sous-finalité. */
  @Output() finalityChange = new EventEmitter<DsfrFinality>();

  /** @since 1.7 Signale quand la route rgdp est sélectionnée. */
  @Output() rgpdRouteSelect = new EventEmitter<string>();

  /*Identifiant de la modale ('consent-modal-id' par défaut) */
  @Input() modalId: string = CONSENT_MODAL_ID;

  /** internal */ titleId: string;

  acceptAll: boolean | undefined;

  ngOnInit(): void {
    this.acceptAll = undefined;
    this.titleId = this.modalId + '-title';
  }

  /**
   * Changement selection accept/refuse d'une finalité ou sous-finalité
   * @param finality
   */
  onFinalityChange(finality: DsfrFinality): void {
    // update selon l'état des sous-finalités
    if (finality.subFinalities) {
      if (finality.subFinalities.every((f) => f.accept === true)) {
        finality.accept = true;
      } else if (finality.subFinalities.every((f) => f.accept === false)) {
        finality.accept = false;
      }
    }

    // update état global
    this.acceptAll = this.finalities.every((f) => f.accept === finality.accept || f.exempt || f.disabled)
      ? finality.accept
      : undefined;

    this.finalityChange.emit(finality);
  }

  /**
   * Confirmation de la sélection
   */
  onConfirm(): void {
    this.confirmSelect.emit(this.finalities);
  }

  /** @since 1.7 */
  onRgpdSelect(): void {
    // Signale quand la route rgdp est sélectionnée
    const route = this.rgpdNavigation.route;
    if (route) this.rgpdRouteSelect.emit(route);
  }
}
