import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'dsfr-backtotop',
  templateUrl: './backtotop.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrBackToTopComponent {
  constructor() {}
}
