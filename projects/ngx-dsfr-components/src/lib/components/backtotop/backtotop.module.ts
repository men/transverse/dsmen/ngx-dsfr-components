import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrLinkModule } from '../link';
import { DsfrBackToTopComponent } from './backtotop.component';

@NgModule({
  declarations: [DsfrBackToTopComponent],
  exports: [DsfrBackToTopComponent],
  imports: [CommonModule, DsfrLinkModule, DsfrI18nPipe],
})
export class DsfrBackToTopModule {}
