import { RouterTestingModule } from '@angular/router/testing';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrLinkModule } from '../link';
import { DsfrBackToTopComponent } from './backtotop.component';

const meta: Meta = {
  title: 'COMPONENTS/Back to Top',
  component: DsfrBackToTopComponent,
  decorators: [moduleMetadata({ imports: [DsfrLinkModule, RouterTestingModule, DsfrI18nPipe] })],
};
export default meta;
type Story = StoryObj<DsfrBackToTopComponent>;

export const Default: Story = {
  args: {},
};
