import { DsfrLinkTarget } from '../../shared';

export interface DsfrDownload {
  /**
   * Obligatoire si le document n'est pas du même langage que la page courante.
   * Attribut hreflang, ex: hreflang="en".
   */
  langCode?: string;
  /**
   * Equivalent à l'attribut html natif 'download'.
   * Si == 'true', télécharge directement le fichier sans l'ouvrir, 'false' par défaut.
   * Peut prendre le nom du fichier à télécharger si on souhaite renommer ce fichier.
   */
  directDownload?: boolean | string;

  /**
   * Lien de téléchargement du fichier.
   */
  link?: string;

  /** Surcharger le label 'Télécharger [ nom de fichier ]' */
  label?: string;

  /**
   * Lien interne
   * Si cette propriété est renseignée, un bouton remplace l'ancre et l'événement (linkSelect) au clic du bouton doit être intercepté.
   */
  route?: string;

  /** Format du fichier - obligatoire. Cf. DsfrMimeType */
  mimeType: string;

  /** Nom du ficher - obligatoire. */
  fileName: string;

  /** Poids du fichier en octets. Obligatoire dans le Dsfr mais peut être renseigné ultérieurement. */
  sizeBytes?: number;

  /** Attribut aria-label  */
  ariaLabel?: string;

  /** Target du lien. Target par défaut de l'application si la propriété est non renseignée. */
  linkTarget?: DsfrLinkTarget;

  /**
   * Indique que les métadonnées du fichier seront positionnées automatiquement par le script DSFR. Les propriétés
   * fileMimeType, fileSizeBytes, hreflang sont alors ignorées. Si la propriété est positionnée à false alors ce sont
   * les valeurs des propriétés fileMimeType, fileSizeBytes, hreflang qui seront utlisées.
   */
  assessFile?: boolean;
}
