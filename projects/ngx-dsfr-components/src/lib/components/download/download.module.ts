import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LinkDownloadComponent } from '../../shared';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrDownloadComponent } from './download.component';

@NgModule({
  declarations: [DsfrDownloadComponent],
  exports: [DsfrDownloadComponent],
  imports: [CommonModule, LinkDownloadComponent, DsfrI18nPipe],
})
export class DsfrDownloadModule {}
