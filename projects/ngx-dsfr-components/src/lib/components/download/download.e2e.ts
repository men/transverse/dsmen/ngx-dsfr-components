import { expect, test } from '@playwright/test';
import { getSbFrame, goToComponentPage } from '../../shared/utils/e2e.utils';

test.describe('Download link', () => {
  /** Default */
  test('download.default', async ({ page }) => {
    const frame = getSbFrame(page);
    const span = frame.locator('span.fr-link__detail');

    await goToComponentPage('download', page);

    await expect(span).toBeEnabled();
    await expect(span).toContainText('PDF - 77,12 ko');
  });

  /** DocEN */
  test('download.doc-en', async ({ page }) => {
    const frame = getSbFrame(page);
    const span = frame.locator('span.fr-link__detail');

    await goToComponentPage('download', page, 'doc-en');

    await expect(span).toBeEnabled();
    await expect(span).toContainText('PDF - 77,12 ko');
    // cf. https://github.com/microsoft/playwright/issues/13919
    await expect(span).toContainText(/\b(Anglais|English)\b/);
  });

  /** NoFileSize */
  test('download.NoFileSize', async ({ page }) => {
    const frame = getSbFrame(page);
    const anchor = frame.locator('a.fr-link--download');

    await goToComponentPage('download', page, 'no-file-size');

    // Les métadatas sont systématiques
    await expect(anchor).toBeEnabled();
    await expect(anchor).not.toHaveAttribute('data-fr-assess-file');
  });

  /** Direct */
  test('download.direct', async ({ page }) => {
    const frame = getSbFrame(page);
    const anchor = frame.locator('a.fr-link--download');

    await goToComponentPage('download', page, 'direct');

    await expect(anchor).toBeEnabled();
    await expect(anchor).toHaveAttribute('download', '');
  });

  /** NewFileName */
  test('download.newFileName', async ({ page }) => {
    const frame = getSbFrame(page);
    const anchor = frame.locator('a.fr-link--download');

    await goToComponentPage('download', page, 'new-file-name');

    await expect(anchor).toBeEnabled();
    await expect(anchor).toHaveAttribute('download', 'Nouveau nom');
  });

  /** Bloc */
  test('download.bloc', async ({ page }) => {
    const frame = getSbFrame(page);
    const div = frame.locator('div:has(.fr-download--card)');

    await goToComponentPage('download', page, 'bloc');

    await expect(div).toBeEnabled();
  });

  /** BlocButton */
  test('download.BlocButton', async ({ page }) => {
    const frame = getSbFrame(page);
    const button = frame.locator('.fr-download--card.edu-cardWithButton button.fr-download__link');

    await goToComponentPage('download', page, 'bloc-button');

    await expect(button).toBeVisible();
  });
});
