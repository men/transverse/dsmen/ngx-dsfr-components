import { componentWrapperDecorator, Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrFileSizeUnitConst, LinkDownloadComponent } from '../../shared';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrTranslateModule } from '../translate';
import { DsfrDownloadVariantConst } from './download-variant';
import { DsfrDownloadComponent } from './download.component';
import { DsfrMimeTypeConst } from './dsfr-mime.type';

const meta: Meta = {
  title: 'COMPONENTS/Download',
  component: DsfrDownloadComponent,
  decorators: [
    moduleMetadata({
      imports: [DsfrTranslateModule, LinkDownloadComponent, DsfrI18nPipe],
    }),
  ],
  argTypes: {
    mimeType: { control: { type: 'select' }, options: Object.values(DsfrMimeTypeConst) },
    sizeUnit: { control: { type: 'inline-radio' }, options: Object.values(DsfrFileSizeUnitConst) },
    variant: { control: { type: 'inline-radio' }, options: Object.values(DsfrDownloadVariantConst) },
  },
};
export default meta;

type Story = StoryObj<DsfrDownloadComponent>;

const componentLang = `<dsfr-translate [languages]="[{ value: 'fr', label: 'Français' },{ value: 'en', label: 'English' }]" currentCode="en"></dsfr-translate>`;
//const decoratorLang = componentWrapperDecorator((story) => `${componentLang}${story}`);
const decoratorLang = componentWrapperDecorator((story: any) => `${story}`);

export const Default: Story = {
  args: {
    link: 'files/lorem-ipsum.pdf',
    fileName: 'lorem-ipsum',
  },

  decorators: [decoratorLang],
};

export const DocEN: Story = {
  args: {
    link: 'files/lorem-ipsum.pdf',
    fileName: 'lorem-ipsum',
    langCode: 'en',
  },

  decorators: [decoratorLang],
};

export const NoFileSize: Story = {
  args: {
    link: '#',
    fileName: 'lorem-ipsum',
    mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
    assessFile: false,
  },
};

export const Direct: Story = {
  args: {
    link: 'files/lorem-ipsum.pdf',
    fileName: 'lorem-ipsum',
    directDownload: true,
  },
};

export const Button: Story = {
  args: {
    fileName: 'lorem-ipsum',
    mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
    sizeBytes: 77 * 1024,
    route: 'my-download-route',
  },

  decorators: [decoratorLang],
};

export const NewFileName: Story = {
  args: {
    link: 'files/lorem-ipsum.pdf',
    fileName: 'lorem-ipsum',
    mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
    sizeBytes: 76 * 1024,
    directDownload: 'Nouveau nom',
  },
};

export const CustomLabel: Story = {
  args: {
    label: 'Mon label de téléchargement',
    fileName: 'lorem-ipsum',
    mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
    link: 'files/lorem-ipsum.pdf',
    sizeBytes: 76 * 1024,
  },
};

export const Bloc: Story = {
  name: 'Bloc (deprecated)',
  args: {
    link: 'files/lorem-ipsum.pdf',
    fileName: 'lorem-ipsum',
    mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
    sizeBytes: 76 * 1024,
    variant: DsfrDownloadVariantConst.BLOCK,
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla est purus, ultrices in porttitor\n' +
      'in, accumsan non quam. Nam consectetur porttitor rhoncus. Curabitur eu est et leo feugiat\n' +
      'auctor vel quis lorem.',
    langCode: 'la', // attr.data-fr-assess-file
  },
};

export const BlocButton: Story = {
  name: 'Bloc button (deprecated)',
  args: {
    fileName: 'lorem-ipsum',
    mimeType: DsfrMimeTypeConst.APPLICATION_PDF,
    sizeBytes: 76 * 1024 + 1224,
    variant: DsfrDownloadVariantConst.BLOCK,
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla est purus, ultrices in porttitor\n' +
      'in, accumsan non quam. Nam consectetur porttitor rhoncus. Curabitur eu est et leo feugiat\n' +
      'auctor vel quis lorem.',
    langCode: 'la', // attr.data-fr-assess-file
  },
};
