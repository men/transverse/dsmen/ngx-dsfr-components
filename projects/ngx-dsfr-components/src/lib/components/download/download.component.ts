import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { DsfrFileSizeUnit, DsfrI18nService, DsfrLink, DsfrLinkTarget, LangService, downloadDetail } from '../../shared';
import { DsfrDownloadVariant, DsfrDownloadVariantConst } from './download-variant';
import { DsfrDownload } from './download.model';

@Component({
  selector: 'dsfr-download',
  templateUrl: './download.component.html',
  styleUrls: ['./download.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class DsfrDownloadComponent implements DsfrDownload {
  /** Label du lien. Par défaut indique 'Télécharger [nom du fichier]' */
  @Input() label: string | undefined;

  /** La propriété ariaLabel définit une valeur de chaîne qui étiquette un élément interactif. */
  @Input() ariaLabel: string | undefined;

  /**
   * Lien de téléchargement (href) du fichier.
   * Si cette propriété n'est pas renseignée, un bouton remplace l'ancre (voir route)
   */
  @Input() link: string | undefined;

  /** Format du fichier - obligatoire. Cf. DsfrMimeType */
  @Input() mimeType: string;

  /** Nom du ficher - obligatoire. */
  @Input() fileName: string;

  /** Poids du fichier en octets. Obligatoire dans le DSDFR mais peut être renseigné ultérieurement. */
  @Input() sizeBytes: number | undefined;

  /**
   * Obligatoire si le document n'est pas du même langage que la page courante.
   *
   * Ex : langCode="en".
   */
  @Input() langCode: string | undefined;

  /** @deprecated Permet de basculer la présentation en card. */
  @Input() variant: DsfrDownloadVariant = DsfrDownloadVariantConst.LINK;

  /** @deprecated Description du fichier (uniquement en mode bloc). */
  @Input() description: string;

  /**
   * Indique que les métadonnées du fichier seront positionnées automatiquement par le script DSFR. Les propriétés
   * `mimeType`, `sizeBytes`, `langCode` sont alors ignorées. Si la propriété est positionnée à false alors ce sont
   * les valeurs des propriétés `mimeType`, `sizeBytes`, `langCode` qui seront utlisées.
   */
  @Input() assessFile = true;

  /**
   * Equivalent à l'attribut html natif 'download'.
   * Si == 'true', télécharge directement le fichier sans l'ouvrir, 'false' par défaut.
   * Peut prendre le nom du fichier à télécharger si on souhaite renommer ce fichier.
   */
  @Input() directDownload: boolean | string | undefined = false;

  /**
   * Si cette propriété est renseignée, un bouton remplace l'ancre et l'évènement linkSelect doit être intercepté.
   * Son activation est exclusive avec link et prime sur ce dernier.
   * Attention, en mode 'route' les métadonnées du fichier à télécharger ne peuvent être connues a priori, vous
   * devez donc obligatoirement renseigner les propriétés `mimeType`, `sizeBytes` et langCode` car la propriété
   * `assessFile` sera ignorée.
   */
  @Input() route: string | undefined;

  /**
   * Attribut target du lien.
   */
  @Input() linkTarget: DsfrLinkTarget | undefined;

  /**
   * Propage la valeur de 'route' lors du clic sur le lien.
   */
  @Output() downloadSelect = new EventEmitter<string>();

  /** cf. accesseurs. */
  private _sizeUnit: DsfrFileSizeUnit;

  constructor(
    private langService: LangService,
    /** @internal */ public i18n: DsfrI18nService,
  ) {}

  get currentLang(): string {
    return this.langService.lang;
  }

  get itemLink(): DsfrLink {
    let label = this.label ? this.label : `${this.i18n.t('commons.download')} ${this.fileName}`;
    label += `<span class="fr-link__detail">${this.detail}</span>`;

    const itemLink: DsfrLink = { label: label, ariaLabel: this.ariaLabel ?? '' };
    if (this.link) {
      itemLink.link = this.link;
    } else if (this.route) {
      itemLink.route = this.route;
    }

    return itemLink;
  }

  /**
   * Retourne l'affichage des détails si assetFile est positionné et que l'utilisateur n'est pas en mode action
   * (activation via l'input route), sinon retourne '' ce qui aura pour effet de laisser calculer l'affichage par
   * le script DSFR.
   */
  get detail(): string {
    return this.assessFile && !this.route ? '' : downloadDetail(this.mimeType, this.sizeBytes, this.sizeUnit);
  }

  get sizeUnit(): DsfrFileSizeUnit {
    return this._sizeUnit ? this._sizeUnit : this.langService.lang === 'fr' ? 'octets' : 'bytes';
  }

  /**
   * Permet d'afficher la taille soit en bytes (KB, MB, ...) soit en octets (Ko, Mo, ...). Par défaut, l'unité est
   * en octets lorsque la langue courante est 'fr', 'bytes' dans les autres cas.
   */
  @Input() set sizeUnit(value: DsfrFileSizeUnit) {
    this._sizeUnit = value;
  }

  /** @internal */
  onLinkSelect(): void {
    if (this.route) {
      this.downloadSelect.emit(this.route);
    }
  }

  /**
   * Retourne vrai si on affiche un bouton à la place d'une ancre.
   * @internal
   */
  hasButtonMarkup(): boolean {
    return !!this.route || !this.link;
  }

  /**
   * Si 'true' ou avec un nom de fichier, télécharge directement le fichier sans l'ouvrir. False par défaut.
   * @internal
   */
  isDirectDownload(): boolean {
    return !!this.directDownload;
  }

  /** @internal */
  getNewFileName(): string {
    return typeof this.directDownload === 'string' ? this.directDownload : '';
  }

  /** @internal */
  isBlockMode() {
    return this.variant === DsfrDownloadVariantConst.BLOCK;
  }
}
