import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrDisplayComponent } from './display.component';

@NgModule({
  declarations: [DsfrDisplayComponent],
  exports: [DsfrDisplayComponent],
  imports: [CommonModule, FormsModule, DsfrI18nPipe],
})
export class DsfrDisplayModule {}
