import { DsfrLink } from '../../shared';
/** Pied de page complet intégrant des listes de liens en rebond de la navigation **/
export interface DsfrFooterReboundLinks {
  /** Titre des catégories */
  title: string;
  /** Liste de liens */
  links: DsfrLink[];
}
