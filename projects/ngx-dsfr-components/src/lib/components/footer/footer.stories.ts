import { argEventEmitter, dsfrDecorator } from '.storybook/storybook-utils';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Meta, StoryObj, componentWrapperDecorator, moduleMetadata } from '@storybook/angular';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrDisplayModule } from '../display';
import { DsfrLinkModule } from '../link';
import { ItemLinkComponent } from '../link/item-link.component';
import { DsfrFooterReboundLinks } from './footer-rebound-links.model';
import { DsfrFooterComponent } from './footer.component';
import { DsfrPartner } from './partner.model';

const meta: Meta = {
  title: 'COMPONENTS/Footer',
  component: DsfrFooterComponent,
  decorators: [
    moduleMetadata({
      imports: [FormsModule, RouterTestingModule, ItemLinkComponent, DsfrDisplayModule, DsfrI18nPipe, DsfrLinkModule],
    }),
  ],
  parameters: {
    actions: { argTypesRegex: '^on.*' },
  },
  argTypes: {
    accessibility: { control: { type: 'inline-radio' } },
    mandatoryLinkSelect: { control: argEventEmitter },
  },
};
export default meta;

type Story = StoryObj<DsfrFooterComponent>;

const loremIpsum =
  '<strong>Lorem ipsum dolor sit amet</strong>, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <em>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur</em>.';
const loremIpsumShort = 'Lorem ipsum dolor [...] fugiat nulla pariatur.';

const partnerMainTest: DsfrPartner = {
  imagePath: 'img/placeholder.16x9.png',
  imageAlt: 'logo operator',
  link: '#',
};

const partnersSubTest: Array<DsfrPartner> = [
  {
    imagePath: 'img/placeholder.16x9.png',
    imageAlt: 'logo operator',
    link: '/',
  },
  {
    imagePath: 'img/placeholder.16x9.png',
    imageAlt: 'logo operator',
    link: '/',
  },
  {
    imagePath: 'img/placeholder.16x9.png',
    imageAlt: 'logo operator',
    link: '#',
    customHeight: '2rem',
  },
];

const reboundLinkTest: Array<DsfrFooterReboundLinks> = [
  {
    title: 'Catégorie 1',
    links: [
      { label: 'Lien de navigation', link: '#' },
      { label: 'Lien de navigation', link: '#' },
    ],
  },
  {
    title: 'Exemples',
    links: [
      { label: 'legifrance.gouv.fr', link: 'https://legifrance.gouv.fr' },
      { label: 'info.gouv.fr', link: 'https://info.gouv.fr' },
      { label: 'service-public.fr', link: 'https://service-public.fr' },
    ],
  },
  {
    title: 'Catégorie 3',
    links: [{ label: 'Lien de navigation', link: '#' }],
  },
];

export const Default: Story = {
  args: {
    presentation: loremIpsum,
    institutionalLinks: DsfrFooterComponent.DEF_INSTITUTIONAL_LINKS,
    mandatoryLinks: DsfrFooterComponent.DEF_MANDATORY_LINKS,
    partnersSub: [],
    reboundLinks: [],
    logo: {
      label: 'Intitulé<br>Officiel',
      tooltipMessage: `Retour à l'accueil du site - [À MODIFIER - texte alternatif de l'image : nom de l'opérateur ou du site serviciel] - République Française`,
      navigation: {
        link: '/',
      },
    },
  },
};

export const WithLogo: Story = {
  args: {
    presentation: loremIpsum,
    logo: {
      imagePath: 'img/placeholder.9x16.png',
      label: 'République<br>Française',
      imageAlt: 'République française logo',
      tooltipMessage: '',
      navigation: {
        routerLink: '/',
      },
    },
    institutionalLinks: DsfrFooterComponent.DEF_INSTITUTIONAL_LINKS,
    mandatoryLinks: DsfrFooterComponent.DEF_MANDATORY_LINKS,
    partnersSub: [],
    reboundLinks: [],
  },
};

export const Full: Story = {
  args: {
    presentation: loremIpsum,
    logo: {
      imagePath: 'img/placeholder.9x16.png',
      label: 'République<br>Française',
      link: '',
      tooltipMessage: '',
    },
    reboundLinks: reboundLinkTest,
    license: `ici c\'est une mention de la licence <a href=\"#\">et là un lien</a>`,
    institutionalLinks: DsfrFooterComponent.DEF_INSTITUTIONAL_LINKS,
    mandatoryLinks: DsfrFooterComponent.DEF_MANDATORY_LINKS,
    partnersSub: [],
    accessibility: 'none',
  },
};

export const WithPartners: Story = {
  args: {
    presentation: loremIpsum,
    logo: {
      imagePath: 'img/placeholder.9x16.png',
      label: 'République<br>Française',
      link: '',
      tooltipMessage: '',
    },
    institutionalLinks: DsfrFooterComponent.DEF_INSTITUTIONAL_LINKS,
    mandatoryLinks: DsfrFooterComponent.DEF_MANDATORY_LINKS,
    partnerMain: partnerMainTest,
    partnersSub: partnersSubTest,
    reboundLinks: [],
    accessibility: 'partially',
  },
};

export const Display: Story = {
  decorators: dsfrDecorator("Avec lien d'accès aux paramètres d'affichage"),
  args: {
    display: true,
  },
  parameters: {
    docs: {
      story: {
        inline: false,
        height: 600,
      },
    },
  },
};

export const DisplayArtworkDirPath: Story = {
  render: (args) => ({
    props: args,
  }),
  args: {
    display: true,
    artworkDirPath: 'artwork',
  },
  decorators: [
    componentWrapperDecorator(
      (story) =>
        `<div class="sb-title">Avec lien d'accès aux paramètres d'affichage et <code>artworkDirPath</code> custom</div>${story}`,
    ),
  ],
};
