import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrDisplayModule } from '../display';
import { DsfrLinkModule } from '../link';
import { ItemLinkComponent } from '../link/item-link.component';
import { DsfrFooterComponent } from './footer.component';

@NgModule({
  declarations: [DsfrFooterComponent],
  exports: [DsfrFooterComponent],
  imports: [CommonModule, RouterModule, DsfrLinkModule, DsfrDisplayModule, DsfrI18nPipe, ItemLinkComponent],
})
export class DsfrFooterModule {}
