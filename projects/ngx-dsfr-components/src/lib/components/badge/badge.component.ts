import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DsfrSizeConst } from '../../shared';
import { DsfrBadgeSeverity, DsfrBadgeSize } from './badges.model';

@Component({
  selector: 'dsfr-badge',
  templateUrl: './badge.component.html',
  encapsulation: ViewEncapsulation.None,
})
export class DsfrBadgeComponent {
  /** Label du badge, obligatoire. */
  @Input() label: string;

  /**
   * L'icône de sévérité n'est pas affiché si `true` (`false` par défaut).
   * Attention seuls les badges dits "système" peuvent avoir une icône, les badges "custom" ne le peuvent pas.
   */
  @Input() noIcon = false;

  /** Taille du badge, optionnel, `MD` par défaut. */
  @Input() size: DsfrBadgeSize = 'MD';

  /** Classe personnalisée pour la couleur du badge. Correspond à une classe du type `fr-badge--blue-cumulus`. */
  @Input() customClass: string | undefined;

  /** @internal */
  private _severity: DsfrBadgeSeverity | undefined = undefined;

  get severity(): DsfrBadgeSeverity | undefined {
    return this._severity;
  }

  /** Niveau d'alerte, optionnel. */
  @Input() set severity(value: DsfrBadgeSeverity | undefined) {
    this._severity = !value ? undefined : <DsfrBadgeSeverity>value?.toLowerCase();
  }

  /** @internal */
  getClasses() {
    const classes: Record<string, boolean> = {
      'fr-badge--sm': this.size === DsfrSizeConst.SM && !!this.label,
      'fr-badge--no-icon': this.noIcon,
    };

    if (this.severity) {
      classes['fr-badge--new'] = this.severity === 'new';
      classes['fr-badge--success'] = this.severity === 'success';
      classes['fr-badge--info'] = this.severity == 'info';
      classes['fr-badge--warning'] = this.severity == 'warning';
      classes['fr-badge--error'] = this.severity === 'error';
    }

    return classes;
  }
}
