/**
 * Définition d'un badge dans un groupe de badges
 */
export interface DsfrBadge {
  /** Label du badge **/
  label: string;

  /** Niveau d'alerte. Optionnel. */
  severity?: DsfrBadgeSeverity;

  /** L'icône de sévérité n'est pas affiché si 'true' (il est affiché par défaut). */
  noIcon?: boolean;

  /** @deprecated since 1.9. Ignoré, La taille des badges dans un groupe est liée au groupe. */
  size?: DsfrBadgeSize;

  /** Classe personnalisée pour la couleur du badge.  */
  customClass?: string;
}

// -- Severity ---------------------------------------------------------------------------------------------------------

export namespace DsfrBadgeSeverityConst {
  export const NEW = 'new';
  export const INFO = 'info';
  export const SUCCESS = 'success';
  export const WARNING = 'warning';
  export const ERROR = 'error';
}

/** Sévérité des badges, (minuscules recommandées) */
type Type = typeof DsfrBadgeSeverityConst;
export type DsfrBadgeSeverity = Type[keyof Type] | 'NEW' | 'SUCCESS' | 'INFO' | 'WARNING' | 'ERROR';

// -- Size ---------------------------------------------------------------------------------------------------------

export type DsfrBadgeSize = 'SM' | 'MD';
