import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DsfrTabComponent } from './tab.component';
import { DsfrTabsComponent } from './tabs.component';

@NgModule({
  imports: [CommonModule, RouterModule],
  declarations: [DsfrTabComponent, DsfrTabsComponent],
  exports: [DsfrTabComponent, DsfrTabsComponent],
})
export class DsfrTabsModule {}
