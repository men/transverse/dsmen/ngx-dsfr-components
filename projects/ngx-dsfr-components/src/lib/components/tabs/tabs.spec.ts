import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { Component, ViewChild } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { DsfrTabComponent } from './tab.component';
import { DsfrTabsComponent } from './tabs.component';
import { DsfrTabsModule } from './tabs.module';
require('../../../../../../node_modules/@gouvfr/dsfr/dist/dsfr.module.js');
declare function dsfr(elem: HTMLElement): any;

@Component({
  selector: `edu-host-component`,
  template: `
    <dsfr-tabs [selectedTabIndex]="selectedTabIndex" [tabsAriaLabel]="tabsAriaLabel" [fullViewport]="fullViewport">
      <dsfr-tab tabId="tab1" label="Label Tab 1" icon="fr-icon-checkbox-circle-line">
        <h4 class="fr-h4">Contenu 1</h4>
        <p>Vitae sapien pellentesque habitant morbi tristique senectus.</p>
        <ul>
          <li>list item</li>
          <li>list item</li>
        </ul>
      </dsfr-tab>
      <dsfr-tab-panel tabId="tab2" label="Label Tab 2" icon="fr-icon-checkbox-circle-line">
        <h4 class="fr-h4">Contenu 2</h4>
        <p>Accumsan lacus vel facilisis volutpat est.</p>
      </dsfr-tab-panel>
      <dsfr-tab tabId="tab3" label="Label Tab 3" icon="fr-icon-checkbox-circle-line" [disabled]="true">
        <h4 class="fr-h4">Contenu 3</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing.</p>
        <ul>
          <li>list item</li>
          <li>list item</li>
        </ul>
      </dsfr-tab>
    </dsfr-tabs>
  `,
})
class TestHostComponent {
  @ViewChild(DsfrTabsComponent) public tabs: DsfrTabsComponent;
}

describe('DsfrTabsComponent', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule, DsfrTabsModule],
      declarations: [DsfrTabsComponent, DsfrTabComponent, TestHostComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should select tab#2', fakeAsync(() => {
    //@ts-ignore
    dsfr.start();

    fixture.detectChanges();
    const btnTab2 = fixture.nativeElement.querySelector('button[aria-controls="tab2"]');
    expect(btnTab2.getAttribute('aria-selected')).toEqual('false');
    btnTab2.click();
    tick(100); // delay to let dsfr execute
    fixture.detectChanges();
    expect(btnTab2.getAttribute('aria-selected')).toEqual('true');
    //@ts-ignore
    dsfr.stop();
  }));

  it('tab 3 should be disabled', () => {
    const tab3 = fixture.nativeElement.querySelector('#buttontab-tab3');
    expect(tab3).not.toBeNull();
    expect(tab3.getAttribute('disabled')).not.toBeNull();
  });
});
