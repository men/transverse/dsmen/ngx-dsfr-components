import { Component, Input } from '@angular/core';
import { DsfrSize } from '../../shared';

@Component({
  selector: 'dsfr-logo',
  templateUrl: './logo.component.html',
})
export class DsfrLogoComponent {
  /** Taille du logo, MD par défaut si non spécifié. */
  @Input() size: DsfrSize;

  /** Intitulé officiel. Utiliser un slot pour du contenu personnalisé. */
  @Input() label: string;
}
