import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';

@Component({
  selector: 'dsfr-franceconnect',
  templateUrl: './franceconnect.component.html',
  encapsulation: ViewEncapsulation.None,
  standalone: true,
  imports: [CommonModule, DsfrI18nPipe],
})
export class DsfrFranceConnectComponent {
  /** Utilisation de FranceConnect+ (qui est plus sécurisé). */
  @Input() secure: boolean;

  /** Demande de connexion par FranceConnect */
  @Output() franceConnectSelect = new EventEmitter<boolean>();

  /** @internal */
  onConnect() {
    this.franceConnectSelect.emit(this.secure);
  }
}
