import { argEventEmitter, dsfrDecorator } from '.storybook/storybook-utils';
import { Meta, StoryObj } from '@storybook/angular';
import { DsfrFranceConnectComponent } from './index';

const meta: Meta = {
  title: 'COMPONENTS/France Connect',
  component: DsfrFranceConnectComponent,
  argTypes: {
    franceConnectSelect: { control: argEventEmitter },
  },
};
export default meta;

type Story = StoryObj<DsfrFranceConnectComponent>;

export const Default: Story = {
  decorators: dsfrDecorator('Bouton FranceConnect'),
  args: {},
};

export const Plus: Story = {
  decorators: dsfrDecorator('Bouton FranceConnect+'),

  args: {
    secure: true,
  },
};
