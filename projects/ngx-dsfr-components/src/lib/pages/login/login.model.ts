import { DsfrFormPasswordValidationRule } from '../../forms/form-password';

export interface DsfrLogin {
  login: string;
  password: string;
  rememberMe?: boolean;
}

export type DsfrFnValidatePassword = (rules: DsfrFormPasswordValidationRule[], password: string) => void;
