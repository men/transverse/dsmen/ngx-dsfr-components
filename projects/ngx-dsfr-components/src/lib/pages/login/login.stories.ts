import { argEventEmitter } from '.storybook/storybook-utils';
import { Meta, StoryObj } from '@storybook/angular';
import { DsfrLoginComponent } from './login.component';

const meta: Meta = {
  title: 'PAGES/Login',
  component: DsfrLoginComponent,
  argTypes: {
    signupSelect: { control: argEventEmitter },
    signinSelect: { control: argEventEmitter },
    recoveryRouteSelect: { control: argEventEmitter },
    passwordChange: { control: argEventEmitter },
    franceConnectSelect: { control: argEventEmitter },
  },
};
export default meta;
type Story = StoryObj<DsfrLoginComponent>;

const service = '[Nom de service-site]';
const recoveryLink = 'https://my-recovery-link';

const DefaultValues: Story = {
  args: {
    // Valeurs par défaut
    rememberMe: false,
    secure: false,
  },
};

export const Default: Story = {
  args: {
    ...DefaultValues,
    service: service,
    recoveryLink: recoveryLink,
  },
};

/** Error */
export const Error: Story = {
  args: {
    ...DefaultValues,
    service: service,
    recoveryLink: recoveryLink,
    error: 'Identifiant/mot de passe incorrect',
  },
};
