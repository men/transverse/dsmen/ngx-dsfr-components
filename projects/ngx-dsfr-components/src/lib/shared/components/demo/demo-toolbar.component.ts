import { Component, EventEmitter, Output } from '@angular/core';
import { DISPLAY_MODAL_ID, DsfrDisplayModule, DsfrTranslateModule } from '../../../components';

//FIXME: RPA: ce composant dépend du répertoire src/lib/components/ > il faut le relocaliser pour éviter la circularité
@Component({
  selector: 'demo-toolbar',
  templateUrl: './demo-toolbar.component.html',
  standalone: true,
  imports: [DsfrTranslateModule, DsfrDisplayModule],
})
export class DemoToolbarComponent {
  @Output() langChange = new EventEmitter<string>();

  protected readonly displayModalId = DISPLAY_MODAL_ID;

  protected readonly languages = [
    { value: 'fr', label: 'Français' },
    { value: 'en', label: 'English' },
  ];

  /** @Internal */
  onLangChange(codeLang: string) {
    this.langChange.emit(codeLang);
  }
}
