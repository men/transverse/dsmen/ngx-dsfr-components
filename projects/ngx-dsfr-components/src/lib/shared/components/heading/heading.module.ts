import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HeadingComponent } from './heading.component';

@NgModule({
  declarations: [HeadingComponent],
  exports: [HeadingComponent],
  imports: [CommonModule],
})
export class HeadingModule {}
