import { Component, Input, OnInit } from '@angular/core';
import { newUniqueId } from '../utils';
import { DefaultValueAccessorComponent } from './default-value-accessor.component';

/**
 * Ce composant est le contrôle abstrait des contrôles Dsfr possédant un id, un name et un label.
 */
@Component({
  template: '',
})
// Renamed from AbstractControlComponent
export abstract class DefaultControlComponent<T> extends DefaultValueAccessorComponent<T> implements OnInit {
  @Input() ariaControls: string;

  /**
   * Attribut `id` du champ, généré automatiquement par défaut.
   */
  @Input() inputId: string;

  /**
   * Texte additionnel décrivant le champ.
   */
  @Input() hint: string | undefined; // Le composant upload utilise la valeur undefined

  /**
   *  Libellé du champ.
   */
  @Input() label: string;

  /**
   * Sera utilisé pour positionner un attribut `name` sur le champ de formulaire.
   */
  @Input() name: string;

  /**
   * @deprecated since 1.11, l'id sur un label ne sera plus utilisé en 2.0
   * @internal
   */
  labelId: string;

  private _id: string;

  /**
   * Cet attribut doit être utilisé en tant que propriété et non en attribut, ex. `[id]="'monid'"`.
   *
   * @deprecated since 1.5, utiliser `inputId` à la place.
   */
  @Input() set id(value: string) {
    if (value) {
      this._id = value;
      this.inputId ??= this._id;
    }
  }

  /** @internal */
  ngOnInit() {
    this.inputId = this.inputId || newUniqueId(); // même si this.inputId = '', l'id sera valorisé (contrairement à '')
    this.labelId = `${this.inputId}-label`;
  }
}
