import { Component, ViewChild } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DsfrSeverity, DsfrSeverityConst } from '../../models';
import { InputGroupComponent } from './input-group.component';

@Component({
  selector: `edu-host-component`,
  template: `<edu-input-group
    [inputId]="inputId"
    [label]="label"
    [hint]="hint"
    [messagesGroupId]="messagesGroupId"
    [message]="message"
    [severity]="severity"
    ><input
  /></edu-input-group>`,
})
class TestHostComponent {
  @ViewChild(InputGroupComponent)
  public inputGroupComponent: InputGroupComponent;
  inputId = '1';
  label: string;
  hint: string;
  messagesGroupId = '2';
  message: string;
  severity: DsfrSeverity;
}

describe('InputGroupComponentTest', () => {
  let testHostComponent: TestHostComponent;
  let fixture: ComponentFixture<TestHostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [InputGroupComponent],
      declarations: [TestHostComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent);
    testHostComponent = fixture.componentInstance;
    fixture.detectChanges();
  });

  // -- Label ----------------------------------------------------------------------------------------------------------

  xit('should have label', () => {
    const label = 'input label';
    testHostComponent.inputGroupComponent.label = label;
    fixture.detectChanges();

    const labelElt = fixture.nativeElement.querySelector('.fr-label');
    expect(labelElt).toBeDefined();
    expect(labelElt.textContent).toEqual(label);
  });

  xit('should have hint', () => {
    const hint = 'texte description';
    testHostComponent.inputGroupComponent.hint = hint;
    fixture.detectChanges();

    const hintElt = fixture.nativeElement.querySelector('.fr-hint-text');
    expect(hintElt).toBeDefined();
    expect(hintElt.className).toContain('fr-hint-text');
    expect(hintElt.textContent).toEqual(hint);
  });

  // -- Messages -------------------------------------------------------------------------------------------------------

  it('should have messages element', () => {
    const message = 'Ceci est un message';
    testHostComponent.inputGroupComponent.message = message;
    fixture.detectChanges();

    const inputGroupElt = fixture.nativeElement.querySelector('.fr-input-group');
    expect(inputGroupElt).toBeDefined();
    const messagesGroupElt = fixture.nativeElement.querySelector('.fr-messages-group');
    expect(messagesGroupElt).toBeDefined();
    const messageElt = messagesGroupElt.querySelector('.fr-message');
    expect(messageElt).toBeDefined();
  });

  it('should disabled component', () => {
    testHostComponent.inputGroupComponent.disabled = true;
    fixture.detectChanges();

    const inputGroupElt = fixture.nativeElement.querySelector('.fr-input-group');
    expect(inputGroupElt.className).toContain('fr-input-group--disabled');
  });

  it('should display an error', () => {
    const error = 'Une erreur sur ce champ';
    testHostComponent.inputGroupComponent.message = error;
    testHostComponent.inputGroupComponent.severity = DsfrSeverityConst.ERROR;
    fixture.detectChanges();

    const inputGroupElt = fixture.nativeElement.querySelector('.fr-input-group');
    expect(inputGroupElt.className).toContain('fr-input-group--error');
    const messageElt = fixture.nativeElement.querySelector('.fr-message');
    expect(messageElt.className).toContain('fr-message--error');
    expect(messageElt.textContent).toEqual(error);
  });

  it('should display a validation message', () => {
    const valid = 'Texte validation';
    testHostComponent.inputGroupComponent.message = valid;
    testHostComponent.inputGroupComponent.severity = DsfrSeverityConst.SUCCESS;
    fixture.detectChanges();

    const inputGroupElt = fixture.nativeElement.querySelector('.fr-input-group');
    expect(inputGroupElt.className).toContain('fr-input-group--valid');
    const messageElt = fixture.nativeElement.querySelector('.fr-message');
    expect(messageElt.className).toContain('fr-message--valid');
    expect(messageElt.textContent).toEqual(valid);
  });

  it('should display an info message', () => {
    const info = "Texte d'information";
    testHostComponent.inputGroupComponent.message = info;
    testHostComponent.inputGroupComponent.severity = DsfrSeverityConst.INFO;
    fixture.detectChanges();

    const inputGroupElt = fixture.nativeElement.querySelector('.fr-input-group');
    expect(inputGroupElt.className).toContain('fr-input-group--info');
    const messageElt = fixture.nativeElement.querySelector('.fr-message');
    expect(messageElt.className).toContain('fr-message--info');
    expect(messageElt.textContent).toEqual(info);
  });
});
