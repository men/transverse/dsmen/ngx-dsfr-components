import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DsfrLink } from '../../models';
import { LinkDownloadComponent } from './link-download.component';

describe('LinkDownloadComponentTest', () => {
  const item: DsfrLink = { label: "Titre de l'item" };

  let fixture: ComponentFixture<LinkDownloadComponent>;
  let linkDownloadComponent: LinkDownloadComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [LinkDownloadComponent],
    }).compileComponents();
    fixture = TestBed.createComponent(LinkDownloadComponent);
    linkDownloadComponent = fixture.componentInstance;
    linkDownloadComponent.item = item;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(linkDownloadComponent).toBeDefined();
  });

  it('target attribute', () => {
    const anchorElt = fixture.nativeElement.querySelector('a');
    expect(anchorElt).not.toBeNull();
    let attribute = anchorElt.getAttribute('target');
    expect(attribute).toBeNull();

    linkDownloadComponent.linkTarget = '_blank';
    fixture.detectChanges();
    attribute = anchorElt.getAttribute('target');
    expect(attribute).not.toBeNull();
    expect(attribute).toEqual('_blank');
  });
});
