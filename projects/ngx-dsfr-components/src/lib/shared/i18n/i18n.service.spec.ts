import { describe, test } from '@jest/globals';
import { DsfrConfig } from '../config/config.model';
import { LangService, Language, LoggerService } from '../services';
import { DsfrI18nService } from './i18n.service';

// L'argument de 'expect' doit être la valeur que votre code produit, et le paramètre du comparateur doit être la valeur correcte.
describe('I18nServiceTest', () => {
  const config: DsfrConfig = { artworkDirPath: '' };
  const logger: LoggerService = new LoggerService();
  const langService: LangService = new LangService(logger);
  const i18n: DsfrI18nService = new DsfrI18nService(config, logger, langService);

  test('should return French message', () => {
    (<any>i18n).onLangChange('fr');
    const msg = i18n.t('follow.newsletter.button.label');
    expect(msg).toEqual('S’abonner');
  });

  test('should return English message', () => {
    (<any>i18n).onLangChange('en');
    const msg = i18n.t('follow.newsletter.button.label');
    expect(msg).toEqual('Subscribe');
  });

  test('should fallback to English', () => {
    (<any>i18n).onLangChange('es');
    const msg = i18n.t('follow.newsletter.button.label');
    expect(msg).toEqual('Subscribe');
  });

  test('should fallback to jsonPath', () => {
    (<any>i18n).onLangChange('es');
    const msg = i18n.t('foo.bar.quz');
    expect(msg).toEqual('foo.bar.quz');
  });

  test('should extend fr/en bundles', () => {
    i18n.extendsLabelsBundle(Language.FR, { myExtensionComp: { myCustomLabel: 'Message en français' } });
    i18n.extendsLabelsBundle(Language.EN, { myExtensionComp: { myCustomLabel: 'English label' } });
    (<any>i18n).onLangChange('fr');
    let msg = i18n.t('myExtensionComp.myCustomLabel');
    expect(msg).toEqual('Message en français');
    (<any>i18n).onLangChange('en');
    msg = i18n.t('myExtensionComp.myCustomLabel');
    expect(msg).toEqual('English label');
  });
});
