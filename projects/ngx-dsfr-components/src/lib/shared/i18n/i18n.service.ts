import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, merge, Observable } from 'rxjs';
import { fromFetch } from 'rxjs/internal/observable/dom/fetch';

import { DSFR_CONFIG_TOKEN } from '../config/config-token';
import { DsfrConfig } from '../config/config.model';
import { DsfrI18nConfig } from '../config/i18n-config';
import { LangService, Language, LoggerService } from '../services';
import { jsonPath2Value } from '../utils';
import { LABELS_BUNDLE_EN } from './i18n-labels-bundle-en';
import { LABELS_BUNDLE_FR } from './i18n-labels-bundle-fr';
import { DsfrI18nBundle, DsfrI18nExtensionBundle } from './i18n-types';

/**
 * Maintient le code langue de l'application et signale le changement de langue.
 */
@Injectable({ providedIn: 'root' })
export class DsfrI18nService {
  /** Observable au changement de la langue (fin du chargement des nouveaux bundles) */
  completeLangChange$: Observable<string>;

  private completeLangChange: BehaviorSubject<string>;

  /**
   * L'ensemble des bundles de libellés gérés par le service I18N.
   */
  private bundles = new Map<string, DsfrI18nBundle>();

  /**
   * Messages (JSON) de la langue courante.
   */
  private currentBundle: DsfrI18nBundle;

  constructor(
    @Inject(DSFR_CONFIG_TOKEN) config: DsfrConfig,
    private readonly logger: LoggerService,
    private readonly langService: LangService,
  ) {
    this.completeLangChange = new BehaviorSubject<string>(this.langService.lang);
    this.completeLangChange$ = this.completeLangChange.asObservable();

    this.bundles.set(Language.FR, LABELS_BUNDLE_FR);
    this.bundles.set(Language.EN, LABELS_BUNDLE_EN);
    this.onLangChange(this.langService.lang); // défaut
    this.readMessagesFiles(config?.i18n).subscribe({
      complete: () => this.startSubscribe(),
    });
  }

  /**
   * Retourne la valeur de traduction dans `messages` partir d'un path json.
   * @param jsonPath le chemin dans le fichier de messages json de la langue courante
   * @return le message et si non trouvé le path json
   */
  t(jsonPath: string): string {
    return jsonPath2Value(this.currentBundle, jsonPath) ?? this.messageNotFound(this.langService.lang, jsonPath);
  }

  /**
   * Permet d'étendre les ressources d'internationalisation sur un périmètre de langue défini.
   *
   * @param lang Le code langue sur deux caractères en minuscules.
   * @param extension Le bundle contenant l'ensemble des libellés traduits dans la langue indiquée.
   */
  extendsLabelsBundle(lang: string, extension: DsfrI18nExtensionBundle): void {
    const locale = lang.toLowerCase();
    if (locale.length !== 2) {
      this.logger.warn("Le code langue n'est pas indiqué sur deux caractères, l'extension est ignorée");

      return;
    }

    const existing: DsfrI18nBundle | undefined = this.bundles.get(lang);
    if (existing) {
      //RPA: un jour viendra peut-être où la ligne ci-dessous ne compilera plus et ce serait une bonne chose
      // on aimerait en effet qu'une erreur soit levée par le compilateur TS car le type I18nBundle est actuellement
      // défini strictement (c'est juste un alias sur le type I18nBaseBundle)
      // ce jour là ajuster la définition du type I18nBundle
      const extendedBundle: DsfrI18nBundle = { ...existing, ...extension };
      this.bundles.set(lang, extendedBundle);
      // si on modifie le bundle de la langue courante, il faut mettre à jour le bundle courant
      if (this.langService.lang === lang) {
        this.currentBundle = extendedBundle;
      }
    } else {
      this.logger.warn(`Aucun bundle à étendre pour le code langue <${lang}>`);
    }
  }

  /**
   * Démarre l'abonnement au changement de langue.
   */
  protected startSubscribe() {
    this.langService.langChange$.subscribe((code) => this.onLangChange(code));
  }

  /**
   * Charge le bon fichier de messages selon le code langue, messages en anglais si non trouvé.
   */
  protected onLangChange(code: string): void {
    this.currentBundle = this.bundles?.get(code) ?? this.bundles?.get(Language.EN) ?? LABELS_BUNDLE_EN;
    this.completeLangChange.next(code); // indique la fin du chargement des bundles corrects
  }

  /**
   * Si message non trouvé, retourne le message en anglais pour les langues autres que les langues par défaut.
   */
  protected messageNotFound(codeLang: string, jsonPath: string): string {
    this.logger.warn(`Aucune traduction '${codeLang}' pour '${jsonPath}'`);
    // try fallback to english
    const englishFallback = codeLang !== Language.EN ? jsonPath2Value(this.bundles.get(Language.EN), jsonPath) : null;

    return englishFallback ?? jsonPath;
  }

  /**
   * Lit tous les fichiers de la configuration
   * @param i18nconfig Configuration i18n
   * @return événement complet sur l'observable quand tous les fichiers ont été lus, erreur ou non
   */
  private readMessagesFiles(i18nConfig: DsfrI18nConfig | undefined): Observable<void> {
    const all: Observable<string>[] = [];
    i18nConfig?.messagesFiles?.forEach((mf) => {
      const observable = fetchFile(mf.filePath);
      observable.subscribe({
        next: (data: any) => {
          console.info(`Lecture du fichier ${mf.filePath}`);
          this.bundles.set(mf.code, data);
        },
        error: (err) => {
          console.error(`Erreur ${err} dans la lecture du fichier ${mf.filePath}`);
        },
      });
      all.push(observable);
    });

    return new Observable<void>((subscriber) => {
      merge(...all).subscribe({
        complete: () => subscriber.complete(),
      });
    });
  }
}

/**
 * Lecture d'un fichier sur le serveur
 * @param filePath Chemin partiel du fichier
 * @param contentType text par défaut
 * @return Observable
 */
function fetchFile(filePath: string, contentType = 'text/plain'): Observable<any> {
  const myHeaders = new Headers();
  myHeaders.append('Content-Type', contentType);
  const myConfig = { headers: myHeaders };

  return new Observable<any>((subscriber) => {
    const fetchObs = fromFetch(filePath, myConfig).subscribe({
      next: (response) => {
        if (response.ok) {
          response.json().then((data) => {
            subscriber.next(data);
            subscriber.complete();
          });
        } else {
          subscriber.error(response.status);
          subscriber.complete();
        }
      },
      error: (err) => subscriber.error(err),
    });
  });
}
