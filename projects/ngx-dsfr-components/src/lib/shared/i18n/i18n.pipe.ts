import { ChangeDetectorRef, OnDestroy, Pipe, PipeTransform } from '@angular/core';
import { Subscription } from 'rxjs';
import { DsfrI18nService } from './i18n.service';

@Pipe({
  name: 'dsfrI18n',
  standalone: true,
  pure: false,
})
export class DsfrI18nPipe implements PipeTransform, OnDestroy {
  value: string = '';
  lastKey: string | null = null;
  langChangeSub$: Subscription | null = null;

  constructor(
    private i18nService: DsfrI18nService,
    private cdr: ChangeDetectorRef,
  ) {}

  transform(jsonPath: string): string {
    if (!jsonPath || jsonPath.length === 0) {
      return jsonPath;
    }

    // Utiliser le cache si pas de changement de langue
    if (this.lastKey === jsonPath) {
      return this.value;
    }

    this.lastKey = jsonPath; // Maj du cache

    this.dispose(); // necessaire dans le cas de pipe impur

    this.langChangeSub$ = this.i18nService.completeLangChange$.subscribe(() => {
      if (this.lastKey) {
        this.value = this.i18nService.t(this.lastKey);
        this.cdr.markForCheck(); // pour composants OnPush
      }
    });

    return this.value;
  }

  ngOnDestroy(): void {
    this.dispose();
  }

  private dispose(): void {
    this.langChangeSub$?.unsubscribe();
    // Note : Il est important de nettoyer la référence fournit dans langChangeSub$
    // RxJS garde une référence au callback dans sa gestion interne, ce qui peut empêcher le nettoyage en mémoire.
    this.langChangeSub$ = null;
  }
}
