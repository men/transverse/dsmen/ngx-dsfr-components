export interface DsfrInputText {
  /**Label du lien.*/
  label: string;
  /**Attribut name de l'input.*/
  name: string;
  /**La valeur gérée par le champ de formulaire.*/
  value: string;
  /**Attribut ariaLabel pour legende non visible.*/
  ariaLabel?: string;
  /**Message d'erreur de l'input.*/
  error?: string;
  /**Ajout un icon à droite dans le champ de saisie.*/
  icon?: string;
  /**Message de validation de l'input.*/
  valid?: string;
  /**Text additionel décrivant le champs.*/
  hint?: string;
  /**Indique que le champs est obligatoite.*/
  required?: boolean;
  /**Attribut 'id' du champ, généré automatiquement par défaut.*/
  id?: string;
  /**Permer de désactiver le champs.*/
  disabled?: boolean;
  /**Valeur de l'attribut auto-complete.*/
  autoComplete?: string;
}
