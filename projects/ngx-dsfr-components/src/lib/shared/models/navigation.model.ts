import { IsActiveMatchOptions, NavigationExtras } from '@angular/router';
import { DsfrLinkTarget } from './link-target.model';

/** Modèle pour les interfaces permettant une navigation. */
export interface DsfrNavigation {
  /** Lien href externe, exclusif avec route et routerLink. */
  link?: string;

  /** Target du lien. Target par défaut de l'application si la propriété est non renseignée. */
  linkTarget?: DsfrLinkTarget;

  /** Path interne. Exclusif avec link et routerLink */
  route?: string;

  /** Path angular géré en tant que directive routerLink. Exclusif avec link et route. */
  routerLink?: string | string[];

  /** RouterLink : classe utilisée pour la directive routerLink active. */
  routerLinkActive?: string | string[];

  /** RouterLink : options qui détermine si le lien est actif en cas de directive routerLink. */
  routerLinkActiveOptions?: { exact: boolean } | IsActiveMatchOptions;

  /** RouterLink : valeurs additionnelles de navigation pour le routerLink (queryParams, state etc.) */
  routerLinkExtras?: NavigationExtras;
}
