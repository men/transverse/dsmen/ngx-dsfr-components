/**
 * Les constantes des niveaux de sévérité DSFR.
 *
 * La valeur SUCCESS est déprécié depuis la version 1.13.0
 */
export namespace DsfrSeverityConst {
  export const INFO = 'info';
  export const SUCCESS = 'success';
  export const WARNING = 'warning';
  export const ERROR = 'error';
  export const VALID = 'valid';
}

/**
 * Les niveaux de sévérité DSFR exposées sous forme de type.
 */
export type DsfrSeverity = (typeof DsfrSeverityConst)[keyof typeof DsfrSeverityConst];
