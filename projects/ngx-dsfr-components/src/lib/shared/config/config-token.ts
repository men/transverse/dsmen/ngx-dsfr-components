import { InjectionToken } from '@angular/core';
import { DsfrConfig } from './config.model';

export const DEFAULT_CONFIG = {
  artworkDirPath: 'artwork',
};

/*
 * Étant donné que ce jeton est à usage interne uniquement, nous n'exporterons pas ce fichier en dehors de
 * la bibliothèque. La création du jeton DI dans un fichier séparé permet d'éviter dépendances circulaires.
 */
export const DSFR_CONFIG_TOKEN = new InjectionToken<DsfrConfig>('ngx-dsfr-config', {
  providedIn: 'root',
  factory: () => DEFAULT_CONFIG,
});
