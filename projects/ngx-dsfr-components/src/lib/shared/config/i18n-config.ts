/**
 * Ce modèle permet de configurer le service i18n.
 */
export interface DsfrI18nConfig {
  /**
   * Répertoire et noms des fichiers pour chaque langue.
   */
  messagesFiles?: { code: string; filePath: string }[];
}
