import { DsfrLogLevel } from '../services/logger.service';

export interface DsfrLogConfig {
  level?: DsfrLogLevel;
}
