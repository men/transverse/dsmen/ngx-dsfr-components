import { describe, test } from '@jest/globals';
import { isStringEmptyOrNull } from './string-utils';

describe('String', () => {
  test('isEmptyOrNull', () => {
    expect(isStringEmptyOrNull(null)).toBe(true);
    expect(isStringEmptyOrNull(undefined)).toBe(true);
    expect(isStringEmptyOrNull('')).toBe(true);
    expect(isStringEmptyOrNull('abc')).toBe(false);
  });
});
