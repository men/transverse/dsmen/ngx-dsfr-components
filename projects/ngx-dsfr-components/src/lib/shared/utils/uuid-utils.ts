import { v4 as uuidv4 } from 'uuid';

/**
 * Fournit un identifiant unique de type string basé, actuellement, sur uuidv4.
 */
export function newUniqueId(): string {
  return uuidv4();
}
