import { ElementRef } from '@angular/core';

export class DomUtils {
  private constructor() {}

  static surroundChildWithli({ nativeElement }: ElementRef<HTMLElement>, childTagName: string): void {
    const children = nativeElement.querySelectorAll(childTagName);

    const childrenToWrap = Array.from(children).filter((child) => {
      return child.parentElement!.tagName !== 'LI';
    });

    childrenToWrap.forEach((child) => {
      this.insertNodeInLi(child);
    });
  }

  static removeEmptyLi({ nativeElement }: ElementRef<HTMLElement>): void {
    const nonProgrammaticLiElements = nativeElement.querySelectorAll('li[data-ngx-dsfr-li]');

    nonProgrammaticLiElements.forEach((li) => {
      if (li.childElementCount === 0) {
        li.remove();
      }
    });
  }

  private static insertNodeInLi(child: Element): void {
    const li = document.createElement('li');
    li.setAttribute('data-ngx-dsfr-li', 'true');
    child.replaceWith(li); // Ne remplace pas l'élément en lui même mais modifie la Node parent de l'élément: https://developer.mozilla.org/fr/docs/Web/API/Element/replaceWith
    li.appendChild(child);
  }
}
