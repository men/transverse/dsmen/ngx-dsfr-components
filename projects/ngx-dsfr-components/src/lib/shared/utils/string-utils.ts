/**
 * Retourne vrai si la string 's' est null, undefined ou vide.
 * @param s la chaïne à considére
 */
export function isStringEmptyOrNull(s: string | null | undefined): boolean {
  return !s || s.trim() === '';
}
