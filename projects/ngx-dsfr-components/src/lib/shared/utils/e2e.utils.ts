import { FrameLocator, Locator, Page, Response, expect } from '@playwright/test';

/**
 * Permet de charger la story faisant l'objet des tests.
 *
 * @param componentName Le nom canonique du composant tel qu'il est défini dans les métadonnées
 *                      (en minuscules et espaces remplacés par des tirets du milieu)
 *
 * @param page La référence à une page Playwright (c'est à dire à un onglet unique du navigateur)
 *
 * @param variant Le nom canonique de la story (en minuscules et espaces remplacés par des tirets du milieu)
 *
 * @returns une promesse sur la réponse HTTP reçue par l'onglet du navigateur
 */
export async function goToComponentPage(
  componentName: string,
  page: Page,
  variant: string = 'default',
): Promise<Response | null> {
  return page.goto(`?path=/story/components-${componentName}--${variant}`);
}

/**
 * Permet de charger la story faisant l'objet des tests.
 *
 * @param componentName Le nom canonique du composant tel qu'il est défini dans les métadonnées
 *                      (en minuscules et espaces remplacés par des tirets du milieu)
 *
 * @param page La référence à une page Playwright (c'est à dire à un onglet unique du navigateur)
 *
 * @param variant Le nom canonique de la story (en minuscules et espaces remplacés par des tirets du milieu)
 *
 * @returns une promesse sur la réponse HTTP reçue par l'onglet du navigateur
 */
export async function goToFormsPage(
  componentName: string,
  page: Page,
  variant: string = 'default',
): Promise<Response | null> {
  return page.goto(`?path=/story/forms-${componentName}--${variant}`);
}

/**
 * Permet de récupérer l'iframe générée par Storybook pour charger la story.
 *
 * @param page La référence à une page Playwright (c'est à dire à un onglet unique du navigateur)
 *
 * @returns un locator sur l'iframe dont l'attribut id est égal à "storybook-preview-iframe"
 */
export function getSbFrame(page: Page): FrameLocator {
  return page.frameLocator('#storybook-preview-iframe');
}

/**
 * Actionne la combinaison clavier spécifiée et assure que le focus est positionné sur le bon élément en séquence.
 *
 * @param tabNavigationSequence Décrit la séquence de navigation attendue.
 * @param keyPressContext Fournit le locator pointant vers l'élément qui doit être actionné.
 * @param withShift
 */
export async function testTabNavigation(
  tabNavigationSequence: Locator[],
  keyPressContext: Locator,
  withShift: boolean = false,
): Promise<void> {
  const keyToPress = withShift ? 'Shift+Tab' : 'Tab';

  for (let navigationStepIndex in tabNavigationSequence) {
    await keyPressContext.press(keyToPress);
    await expect(tabNavigationSequence[navigationStepIndex]).toBeFocused();
  }
}
