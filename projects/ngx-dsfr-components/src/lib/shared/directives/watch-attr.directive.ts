import { Directive, ElementRef, EventEmitter, Input, OnDestroy, Output } from '@angular/core';

@Directive({
  selector: '[watchAttr]',
  standalone: true,
})
export class WatchAttrDirective implements OnDestroy {
  // /**
  //  * Obligatoire : permet d'indiquer quel attribut on souhaite observer.
  //  */
  // @Input() attrObserver: string;

  @Input()
  public watchAttrName: string | undefined = undefined;

  @Output()
  public watchAttr = new EventEmitter<MutationRecord>();

  private _changes: MutationObserver;

  constructor(private elementRef: ElementRef) {
    const element = this.elementRef.nativeElement;

    this._changes = new MutationObserver((mutations: MutationRecord[]) => {
      mutations.forEach((mutation: MutationRecord) => {
        if (
          this.watchAttrName === undefined ||
          (this.watchAttrName.trim().length > 0 && this.watchAttrName.trim() === mutation.attributeName)
        ) {
          this.watchAttr.emit(mutation);
        }
      });
    });

    this._changes.observe(element, {
      attributes: true,
      childList: false,
      characterData: false,
    });
  }

  ngOnDestroy(): void {
    this._changes.disconnect();
  }
}
