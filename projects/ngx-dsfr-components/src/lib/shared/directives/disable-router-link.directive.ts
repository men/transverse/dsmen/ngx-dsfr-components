import { Directive, Input, Optional } from '@angular/core';
import { RouterLink } from '@angular/router';

/**
 * Désactiver le lien du routerLink
 * Cas particulier des entrées de menu : profiter de la directive routerLink mais ne pas effectuer la navigation
 * Le preventDefault classique non fonctionnel sur les routerLink
 */
@Directive({
  selector: 'button[routerLink][disableNavigation]',
  standalone: true,
})
export class DsfrDisableRouterLinkDirective {
  @Input() disableNavigation: boolean;

  constructor(
    // Inject routerLink
    @Optional() routerLink: RouterLink,
  ) {
    const link = routerLink;

    // Replace method
    link.onClick = () => {
      return true;
    };
  }
}
