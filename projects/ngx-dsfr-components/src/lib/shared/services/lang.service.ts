import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { LocalStorage } from '../utils';
import { LoggerService } from './logger.service';

/**
 * Le service est chargé de maintenir le code langue de l'application et de signaler le changement de langue aux observateurs.
 */
@Injectable({ providedIn: 'root' })
export class LangService {
  private static readonly DEF_LANG = 'fr';

  /** Observable changement de la langue. */
  langChange$: Observable<string>;

  private langChange: BehaviorSubject<string>;

  /** Par défaut, le code de la langue est stocké dans le localStorage.  */
  private hasLocalStorage = true;

  /** Nom par défaut du local storage. */
  private localStorageName = 'lang';

  constructor(private logger: LoggerService) {
    this.langChange = new BehaviorSubject<string>(LangService.DEF_LANG);
    this.langChange$ = this.langChange.asObservable();

    if (this.hasLocalStorage) {
      const lang = LocalStorage.get(this.localStorageName);
      if (lang) this.lang = <string>LocalStorage.get(this.localStorageName);
    }
  }

  get lang(): string {
    return this.langChange.value;
  }

  set lang(code: string) {
    if (!code || code === this.lang) return;
    this.logger.info(`Changement de langue courante: '${code}'`);

    if (this.hasLocalStorage) LocalStorage.set(this.localStorageName, code);
    this.langChange.next(code);
  }
}
