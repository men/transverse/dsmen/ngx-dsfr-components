import { Inject, Injectable, Optional } from '@angular/core';
import { DSFR_CONFIG_TOKEN } from '../config/config-token';
import { DsfrConfig } from '../config/config.model';

@Injectable({
  providedIn: 'root',
})
export class ConfigService {
  /**
   * Puisqu'il est possible que les utilisateurs de la bibliothèque oublient d'appeler forRoot(), nous marquons
   * le jeton DI comme optionnel à l'aide du décorateur intégré (pour éviter une erreur "Aucun fournisseur pour ...").
   * Dans ces cas, le paramètre constructeur sera nul, mais nous fournirons des valeurs par défaut.
   */
  constructor(
    @Optional()
    @Inject(DSFR_CONFIG_TOKEN)
    public readonly params: DsfrConfig | null,
  ) {}
}
