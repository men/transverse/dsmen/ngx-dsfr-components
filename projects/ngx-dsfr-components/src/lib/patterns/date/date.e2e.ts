import { expect, FrameLocator, Locator, Page, Response, test } from '@playwright/test';
import { getSbFrame } from '../../shared/utils/e2e.utils';

test.describe('Date Pattern', () => {
  test.describe('should have a value corresponding to', () => {
    test.use({ timezoneId: 'Europe/Brussels' });

    test('the value typed in its inputs', async ({ page }) => {
      const frame = getSbFrame(page);
      const isoInput = getIsoInput(frame);

      await goToDatePatternPage(page, 'iso-string');
      await typeDateInInputs(frame, '14', '3', '2024');
      await expect(isoInput).toHaveValue('2024-03-13T23:00:00.000Z');
    });

    test('the value typed is the Iso Input', async ({ page }) => {
      const frame = getSbFrame(page);
      const dayInput = getDayInput(frame);
      const monthInput = getMonthInput(frame);
      const yearInput = getYearInput(frame);

      await goToDatePatternPage(page, 'iso-string');
      await typeDateInIsoInput(frame, '2024-03-13T23:00:00.000Z');
      await dayInput.click(); // Focus out

      await expect(dayInput).toHaveValue('14');
      await expect(monthInput).toHaveValue('3');
      await expect(yearInput).toHaveValue('2024');
    });

    test('the value typed is the Iso Input with an offset', async ({ page }) => {
      const frame = getSbFrame(page);
      const dayInput = getDayInput(frame);
      const monthInput = getMonthInput(frame);
      const yearInput = getYearInput(frame);

      await goToDatePatternPage(page, 'iso-string');
      await typeDateInIsoInput(frame, '2024-03-13T23:00:00.000+05:00');
      await dayInput.click(); // Focus out

      await expect(dayInput).toHaveValue('14');
      await expect(monthInput).toHaveValue('3');
      await expect(yearInput).toHaveValue('2024');
    });
  });

  test.describe('when given an exotic timezone, should have a value corresponding to', () => {
    test.use({ timezoneId: 'Antarctica/Palmer' });

    test('the value typed in its inputs', async ({ page }) => {
      const frame = getSbFrame(page);
      const isoInput = getIsoInput(frame);

      await goToDatePatternPage(page, 'iso-string');
      await typeDateInInputs(frame, '14', '3', '2024');
      await expect(isoInput).toHaveValue('2024-03-14T03:00:00.000Z');
    });

    test('the value typed is the Iso Input', async ({ page }) => {
      const frame = getSbFrame(page);
      const dayInput = getDayInput(frame);
      const monthInput = getMonthInput(frame);
      const yearInput = getYearInput(frame);

      await goToDatePatternPage(page, 'iso-string');
      await typeDateInIsoInput(frame, '2024-03-14T03:00:00.000Z');
      await dayInput.click(); // Focus out

      await expect(dayInput).toHaveValue('14');
      await expect(monthInput).toHaveValue('3');
      await expect(yearInput).toHaveValue('2024');
    });
  });
});

/** LOCATORS */

function getDayInput(frame: FrameLocator): Locator {
  return frame.getByLabel('Jour');
}

function getMonthInput(frame: FrameLocator): Locator {
  return frame.getByLabel('Mois');
}

function getYearInput(frame: FrameLocator): Locator {
  return frame.getByLabel('Année');
}

function getIsoInput(frame: FrameLocator): Locator {
  return frame.getByLabel('ISO String :');
}

/** ACTIONS */

async function goToDatePatternPage(page: Page, variant: string = 'default'): Promise<Response | null> {
  return page.goto(`?path=/story/patterns-date--${variant}`);
}

async function typeDateInInputs(frame: FrameLocator, day: string, month: string, year: string): Promise<void> {
  const dayInput = getDayInput(frame);
  const monthInput = getMonthInput(frame);
  const yearInput = getYearInput(frame);

  await dayInput.fill(day);
  await monthInput.fill(month);
  return await yearInput.fill(year);
}

async function typeDateInIsoInput(frame: FrameLocator, isoDate: string): Promise<void> {
  const isoInput = getIsoInput(frame);

  return await isoInput.fill(isoDate);
}
