import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrFormInputModule } from '../../forms';
import { DsfrFormFieldsetModule } from '../../forms/fieldset';
import { DsfrI18nPipe } from '../../shared/i18n/i18n.pipe';
import { DsfrDateComponent } from './date.component';

@NgModule({
  declarations: [DsfrDateComponent],
  exports: [DsfrDateComponent],
  imports: [CommonModule, FormsModule, DsfrFormFieldsetModule, DsfrFormInputModule, DsfrI18nPipe],
})
export class DsfrDateModule {}
