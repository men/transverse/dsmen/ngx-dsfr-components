import { describe, test } from '@jest/globals';
import { DATE_ERROR, DateModel } from './date.model';

// L'argument de 'expect' doit être la valeur que votre code produit, et le paramètre du comparateur doit être la valeur correcte.
describe('DateModel', () => {
  // constructor
  test('constructor', () => {
    // Transformation de date
    const dateModel = new DateModel(1995, 12, 17);
    const dateJs = new Date(Date.UTC(1995, 11, 17));
    expect(dateModel.toDate()).toEqual(dateJs);

    expect(new DateModel(95).fullYear).toEqual(1995);
  });

  // isValid
  test('isValid', () => {
    expect(new DateModel().isValid()).toBeFalsy();
    expect(new DateModel(2024).isValid()).toBeFalsy();
    expect(new DateModel(2024, 12).isValid()).toBeFalsy();
    expect(new DateModel(2024, 12, 31).isValid()).toBeTruthy();
  });

  // toDate
  test('toDate', () => {
    let utcDate = new Date(Date.UTC(96, 1, 2));
    expect(utcDate.toUTCString()).toEqual('Fri, 02 Feb 1996 00:00:00 GMT');

    // Date locale avec décalage d'1 heure
    const dateModel = new DateModel(96, 2, 2);
    expect(dateModel.toDate()?.toUTCString()).toEqual('Fri, 02 Feb 1996 00:00:00 GMT');
    expect(dateModel?.toDate()).toEqual(utcDate);

    expect(new DateModel().toDate()).toBeUndefined();
    expect(new DateModel(2024).toDate()).toBeUndefined();
    expect(new DateModel(2024, 12).toDate()).toBeUndefined();
    expect(new DateModel(2024, 12, 31).toDate()).toBeDefined();
  });

  test('toDate with invalid dates', () => {
    // Date inexistante
    expect(new DateModel(<never>'2024', <never>'2', <never>'29').toDate()).toBeDefined();
    expect(new DateModel(<never>'2024', <never>'2', <never>'30').toDate()).toBeUndefined();
  });

  test('validate', () => {
    // `Les champs doivent être au format numérique.` : si au moins un champ contient une autre valeur que des chiffres,
    // @ts-ignore
    let dateModel = new DateModel('a');
    expect(dateModel.validate()).toEqual([DATE_ERROR.INVALID_FORMAT_YEAR]);

    // `Le jour doit être compris entre 1 et 31.`,
    dateModel = new DateModel(2024, 1, 32);
    expect(dateModel.validate()).toEqual([DATE_ERROR.INVALID_DAY]);

    // `Le mois doit être compris entre 1 et 12.`,
    dateModel = new DateModel(2024, 13, 32);
    expect(dateModel.validate()).toEqual([DATE_ERROR.INVALID_DAY, DATE_ERROR.INVALID_MONTH]);

    // `La date est invalide.` : si la date n'existe pas, ex : `31/04/2024`,
    dateModel = new DateModel(2024, 4, 31);
    expect(dateModel.validate()).toEqual([DATE_ERROR.INVALID_DATE]);

    // `Tous les champs sont requis.` : si une valeur a été saisie puis effacée.
    // @ts-ignore
    dateModel = new DateModel(2024, 4, '');
    expect(dateModel.validate()).toEqual([]);
    expect(dateModel.validate(true)).toEqual([DATE_ERROR.REQUIRED]);
  });

  describe('of', () => {
    const dateReference = new Date('2023-12-05T05:00:00.000Z');

    const testCases = [
      {
        testCaseName: 'with iso string',
        testValue: DateModel.of('2023-12-05T05:00:00.000Z', window.console),
      },
      {
        testCaseName: 'with Date.now()',
        testValue: DateModel.of(1701752400000, window.console),
      },
      {
        testCaseName: 'with Date constructor',
        testValue: DateModel.of(new Date(2023, 11, 5, 5), window.console),
      },
      {
        testCaseName: 'with Date constructor and GMT string',
        testValue: DateModel.of(new Date('Tue Dec 05 2023 01:00:00 GMT-0300'), window.console),
      },
    ];

    for (let testCase of testCases) {
      test(testCase.testCaseName, () => {
        expect(testCase.testValue.toDate(5)).toEqual(dateReference);
      });
    }
  });
});
