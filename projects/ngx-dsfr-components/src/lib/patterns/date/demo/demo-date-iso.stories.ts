import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrDateModule } from '../date.module';
import { DemoDateIsoComponent } from './demo-date-iso.component';
import { controlDisable, dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'PATTERNS/Date',
  component: DemoDateIsoComponent,
  decorators: [moduleMetadata({ imports: [FormsModule, DsfrDateModule] })],
  argTypes: {
    value: controlDisable,
  },
};
export default meta;
type Story = StoryObj<DemoDateIsoComponent>;

export const IsoString: Story = {
  decorators: dsfrDecorator('Valeur initiale sous forme de chaîne ISO'),
  argTypes: { value: { control: { type: 'text' } } },
  args: {
    legend: "Date de l'événement",
    hint: '',
    required: true,
  },
};
