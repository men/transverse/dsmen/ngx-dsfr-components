import { FormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrDateModule } from '../date.module';
import { DemoDateErrorComponent } from './demo-date-error.component';
import { dsfrDecorator } from '.storybook/storybook-utils';

const meta: Meta = {
  title: 'PATTERNS/Date',
  component: DemoDateErrorComponent,
  decorators: [moduleMetadata({ imports: [FormsModule, DsfrDateModule] })],
  argTypes: {
    value: { control: { type: 'date' } },
  },
};
export default meta;
type Story = StoryObj<DemoDateErrorComponent>;

const now = new Date(Date.now());
const demain = new Date(Date.UTC(now.getFullYear(), now.getMonth(), now.getDate() + 1));

export const Error: Story = {
  decorators: dsfrDecorator('Error'),
  args: {
    value: demain,
    hint: 'La date doit être inférieure ou égale à la date du jour',
    legend: "Date de l'événement",
    required: false,
  },
};
