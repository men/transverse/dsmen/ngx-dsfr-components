import { ReactiveFormsModule } from '@angular/forms';
import { Meta, moduleMetadata, StoryObj } from '@storybook/angular';
import { DsfrDateModule } from '../date.module';
import { DemoDateReactiveComponent } from './demo-date-reactive.component';

const meta: Meta = {
  title: 'PATTERNS/Date',
  component: DemoDateReactiveComponent,
  decorators: [moduleMetadata({ imports: [ReactiveFormsModule, DsfrDateModule] })],
  argTypes: {
    value: { control: { type: 'date' } },
  },
};
export default meta;
type Story = StoryObj<DemoDateReactiveComponent>;

export const ReactiveForm: Story = {
  args: {
    value: Date.now(),
    required: false,
  },
};

export const ReactiveFormUndefined: Story = {
  args: {
    value: undefined,
    required: true,
  },
};
