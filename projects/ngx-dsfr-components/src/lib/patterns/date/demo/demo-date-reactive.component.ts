import { CommonModule } from '@angular/common';
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  ValidationErrors,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { DemoToolbarComponent } from '../../../shared/components/demo/demo-toolbar.component';
import { DsfrDateModule } from '../date.module';

@Component({
  selector: 'demo-date-reactive',
  templateUrl: './demo-date-reactive.component.html',
  standalone: true,
  imports: [CommonModule, ReactiveFormsModule, DsfrDateModule, DemoToolbarComponent],
})
export class DemoDateReactiveComponent implements OnChanges {
  @Input() value: any;
  @Input() required: boolean;

  /** @internal */ legend = "Date de l'événement";
  /** @internal */ hint = 'La date doit être inférieure ou égale à la date du jour';
  /** @internal */ error = '';

  /** @internal */ formGroup: FormGroup;

  constructor(private fb: FormBuilder) {
    this.formGroup = this.fb.group({
      date: ['', this.myDateValidator()],
    });
  }

  // Changement provenant de StoryBook
  ngOnChanges(changes: SimpleChanges) {
    if (changes['value']) {
      const date = this.value ? new Date(this.value) : this.value;
      this.formGroup.controls['date'].setValue(date);
    }
    if (changes['required']) {
      const control = this.formGroup.controls['date'];
      if (this.required) control.addValidators(Validators.required);
      else control.removeValidators(Validators.required);
    }
  }

  /** @internal */
  // Si on gère un validator
  myDateValidator(): ValidatorFn {
    this.error = '';
    return (control: AbstractControl): ValidationErrors | null => {
      console.log('myDateValidator');
      const value = control?.value;
      if (!value) return null;

      let errors = null;
      this.error = '';
      if (value > new Date(Date.now())) {
        this.error = 'La date doit être inférieure ou égale à la date du jour';
        errors = [{ err_date: this.error }];
      }
      return errors;
    };
  }

  /** @internal */
  getControlValue() {
    return this.formGroup.controls['date']?.value;
  }

  /** @internal */
  getTypeValue() {
    return typeof this.getControlValue();
  }

  /** @internal */
  toIsoString() {
    const value = this.getControlValue();
    if (!value) return '';
    return typeof value === 'object' ? value.toISOString() : value?.toString();
  }
}
