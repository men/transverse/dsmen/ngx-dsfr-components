import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrFormInputModule } from '../../forms';
import { DsfrFormFieldsetModule } from '../../forms/fieldset';
import { DsfrAddressComponent } from './address.component';

@NgModule({
  declarations: [DsfrAddressComponent],
  exports: [DsfrAddressComponent],
  imports: [CommonModule, DsfrFormFieldsetModule, DsfrFormInputModule, FormsModule],
})
export class DsfrAddressModule {}
