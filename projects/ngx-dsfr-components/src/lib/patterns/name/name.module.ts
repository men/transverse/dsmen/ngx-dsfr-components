import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DsfrButtonModule } from '../../components/button';
import { DsfrFormCheckboxModule, DsfrFormInputModule } from '../../forms';
import { DsfrFormFieldsetModule } from '../../forms/fieldset';
import { DsfrFormSelectModule } from '../../forms/form-select';
import { DsfrNameComponent } from './name.component';

@NgModule({
  declarations: [DsfrNameComponent],
  exports: [DsfrNameComponent],
  imports: [
    CommonModule,
    DsfrFormFieldsetModule,
    DsfrFormInputModule,
    DsfrFormCheckboxModule,
    DsfrButtonModule,
    DsfrFormSelectModule,
    FormsModule,
  ],
})
export class DsfrNameModule {}
