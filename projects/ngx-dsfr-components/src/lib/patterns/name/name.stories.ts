import { FormsModule } from '@angular/forms';
import { Meta, StoryObj, moduleMetadata } from '@storybook/angular';
import { DsfrButtonModule } from '../../components/button';
import { DsfrFormCheckboxModule, DsfrFormInputModule } from '../../forms';
import { DsfrFormFieldsetModule } from '../../forms/fieldset';
import { DsfrFormSelectModule } from '../../forms/form-select';
import { DsfrInputText } from '../../shared/models/input.model';
import { DsfrSelect } from '../../shared/models/select.model';
import { DsfrNameComponent } from './name.component';

const meta: Meta = {
  title: 'PATTERNS/Name',
  component: DsfrNameComponent,
  decorators: [
    moduleMetadata({
      imports: [
        DsfrFormFieldsetModule,
        DsfrFormInputModule,
        FormsModule,
        DsfrFormCheckboxModule,
        DsfrButtonModule,
        DsfrFormSelectModule,
      ],
    }),
  ],
};
export default meta;

let myModelName: DsfrInputText = { name: 'name', label: 'Nom', value: 'Doe' };
let myModelFirstName: DsfrInputText[] = [{ name: 'firstname', label: 'Prénom', value: 'John' }];

export const Default: StoryObj<DsfrNameComponent> = {
  args: {
    legend: '',
    noFirstName: false,
    lastName: undefined,
    usualName: undefined,
    addFirstName: false,
    country: undefined,
  },
  render: (args) => ({
    props: { ...args, myModelName, myModelFirstName },
    template: `
      <dsfr-name [lastName]="myModelName" 
                 [firstNames]="myModelFirstName"
                 [legend]="legend"
                 [noFirstName]="noFirstName"
                 [usualName]="usualName"
                 [addFirstName]="addFirstName"
                 [country]="country">
      </dsfr-name>
      <div>myModelName.value : <code>{{ myModelName.value }}</code></div>
      <div>myModelFirstName[0].value : <code>{{ myModelFirstName[0].value  }}</code></div>
      `,
  }),
};

let myModelFirstNameDisabled: Array<DsfrInputText> = [
  {
    name: 'firstname',
    label: 'Prénom',
    value: 'John',
  },
];
export const FirstNameDisabled: StoryObj<DsfrNameComponent> = {
  render: (args) => ({
    props: { ...args, myModelName, myModelFirstNameDisabled },
    template: `
    <dsfr-name noFirstName="true" [lastName]="myModelName" [firstNames]="myModelFirstNameDisabled"></dsfr-name>
          <span>myModelName.value : {{myModelName.value}}</span>
          `,
  }),
};

let myModelUsualName: DsfrInputText = {
  name: 'UsualName',
  label: "Nom d'usage",
  value: '',
};
export const UsualName: StoryObj<DsfrNameComponent> = {
  render: (args) => ({
    props: { ...args, myModelName, myModelFirstNameDisabled, myModelUsualName },
    template: `
    <dsfr-name noFirstName="true" [lastName]="myModelName" [usualName]="myModelUsualName" [firstNames]="myModelFirstNameDisabled"></dsfr-name>
          <span>myModelName.value : {{myModelName.value}}</span>
          `,
  }),
};

let myModelMultipleFirstName: Array<DsfrInputText> = [
  {
    name: 'firstname',
    label: 'Prénom',
    value: 'John',
  },
  {
    name: 'firstname',
    label: 'Prénom',
    value: 'John2',
  },
];
export const AddFirstname: StoryObj<DsfrNameComponent> = {
  render: (args) => ({
    props: { ...args, myModelName, myModelMultipleFirstName, addNewFirstName, deleteFirstName },
    template: `
    <dsfr-name (addFirstNameSelect)="addNewFirstName()" (deleteFirstNameSelect)="deleteFirstName($event)" noFirstName="true" [addFirstName]="true" [lastName]="myModelName" [firstNames]="myModelMultipleFirstName"></dsfr-name>`,
  }),
};

function addNewFirstName() {
  myModelMultipleFirstName.push({ name: 'given-name', label: 'Prénom', value: '' });
}

function deleteFirstName(index: number) {
  myModelMultipleFirstName.splice(index, 1);
}

let myModelCountry: DsfrSelect = {
  name: 'country',
  label: 'Pays',
  value: '',
  options: [
    { label: 'France', value: 'FR' },
    { label: 'Allemagne', value: 'DE' },
    { label: 'Italie', value: 'IT' },
    { label: 'Espagne', value: 'ES' },
    { label: 'Royaume-Uni', value: 'GB' },
  ],
};
export const International: StoryObj<DsfrNameComponent> = {
  render: (args) => ({
    props: { ...args, myModelName, myModelFirstName, myModelCountry },
    template: `
    <dsfr-name noFirstName="true" [country]="myModelCountry" [lastName]="myModelName" [firstNames]="myModelFirstName"></dsfr-name>
          `,
  }),
};
