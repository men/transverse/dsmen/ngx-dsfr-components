module.exports = { Upgrader };

const { readFile, readdir, writeFile } = require('fs/promises');
const { HtmlUpgrader } = require('./html-upgrader');
const { TypescriptUpgrader } = require('./typescript-upgrader');

function Upgrader() {
  const EXTENSIONS = { HTML: 'html', TS: 'ts' };
  const htmlUpgrader = new HtmlUpgrader();
  const tsUpgrader = new TypescriptUpgrader();

  /**
   *
   * @param path
   * @return {Promise<unknown>}
   */
  this.upgradeDir = (path /*: string*/) /*: Promise<void>*/ => {
    return new Promise((resolve, reject) => {
      readdir(path, { withFileTypes: true }).then(
        (entries) => {
          const allPromises /*: Promise<void>[]*/ = [];
          entries.forEach((entry) => {
            const entryPath = path + '\\' + entry.name;
            if (entry.isFile()) {
              const extension = getExtension(entryPath);
              if (extension === EXTENSIONS.HTML || extension === EXTENSIONS.TS)
                allPromises.push(upgradeFile(entryPath));
            } else if (entry.isDirectory()) {
              allPromises.push(this.upgradeDir(entryPath));
            }
          });
          Promise.all(allPromises).then(
            () => resolve(),
            (err) => reject(err),
          );
        },
        (err) => reject(err),
      );
    });
  };

  /**
   *
   * @param path
   * @param outputPath
   * @return {Promise<unknown>}
   */
  function upgradeFile(path /*: string*/, outputPath = undefined /*?: string*/) /*: Promise<void>*/ {
    return new Promise((resolve, reject) => {
      const extension = getExtension(path);
      readFile(path).then(
        (content) => {
          console.info("- Fichier '" + path + "'");
          const result =
            extension === EXTENSIONS.HTML
              ? htmlUpgrader.upgradeContent(content.toString())
              : tsUpgrader.upgradeContent(content.toString());
          const resultPath = outputPath ? outputPath : path;
          writeFile(resultPath, result).then(
            (ok) => resolve(ok),
            (err) => reject(err),
          );
        },
        (err) => reject(err),
      );
    });
  }

  function getVersion() {
    return '0.0.2';
  }
}

function getExtension(fileName /*: string*/) {
  return fileName.split('.').pop();
}
