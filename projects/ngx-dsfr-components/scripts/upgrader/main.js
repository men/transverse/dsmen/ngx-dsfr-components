module.exports = { upgradeMain };

const readline = require('readline');
const { Upgrader } = require('./upgrader');
const { version } = require('../../package.json');

function upgradeMain(dir /*: string*/) {
  const conversation = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  console.info('version ' + version);
  conversation.question(`Voulez-vous mettre à jour le répertoire '${dir}' ? (Y/n) `, (response) => {
    conversation.close();
    if (response === 'y' || response === 'Y' || !response) {
      console.log(`Mise à niveau des fichiers du répertoire '${dir}'`);
      new Upgrader().upgradeDir(dir).then(
        () => console.info('Le traitement est terminé'),
        (err) => console.error(err),
      );
    } else console.info('quit');
  });
}
