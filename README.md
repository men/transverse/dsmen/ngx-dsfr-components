# @edugouvfr/ngx-dsfr - Readme

`NgxDsfr` est un portage Angular des éléments d'interface du
[🇫🇷 Système de Design de l'État Français (DSFR)](https://www.systeme-de-design.gouv.fr/).

Cette bibliothèque est développée, maintenue et gérée par une équipe de développement du Ministère de l'Éducation
Nationale basée à Grenoble et pilotée par les sous-directions "socle numérique" (SOCLE1) et "services numériques" (SN2)
de la Direction du Numérique pour l'Éducation (DNE).

## Dépendances

Notre package s'appuie actuellement sur la [version 1.13.0 du DSFR](https://github.com/GouvernementFR/dsfr/releases/tag/v1.13.0).
Nous développons avec Angular 17 mais notre bibliothèque est compatible Angular 17 à 19.

## Composants additionnels

Nous proposons des [composants d'extension](https://foad.phm.education.gouv.fr/edugouvfr/ngx-dsfr-ext/) aux éléments officiels du DSFR : multiselect, autocomplete, etc.
Ce package [ngx-dsfr-ext](https://foad.phm.education.gouv.fr/edugouvfr/ngx-dsfr-ext/) a pour but de couvrir les besoins fonctionnels qui ne sont pas encore couverts par le DSFR. Elle a été construite en cohérence avec les composants DSFR existants, dans le respect des fondamentaux techniques et des principes de design du DSFR.

## Installation

- Installer la dépendance vers le module NPM dans votre projet Angular

  ```bash
  npm install @edugouvfr/ngx-dsfr
  ```

  _La dépendance transitive vers `@gouvfr/dsfr` sera installée automatiquement._

- Référencer les feuilles de style et le script DSFR dans votre fichier `angular.json` ou `project.json` :

  ```json
  "styles": [
    "projects/app-demo/src/styles.scss",
    "./node_modules/@gouvfr/dsfr/dist/dsfr/dsfr.main.min.css",
    "./node_modules/@gouvfr/dsfr/dist/utility/utility.main.min.css",
  ],
  "scripts": [
    "./node_modules/@gouvfr/dsfr/dist/dsfr/dsfr.module.js"
  ]
  ```

- Ajouter les meta tags du dsfr dans le fichier `index.html`.

_Exemple: [Prise en main](https://www.systeme-de-design.gouv.fr/utilisation-et-organisation/developpeurs/prise-en-main)_

## Utilisation

- Dans votre module, importer le module du composant que vous souhaitez utiliser :

  ```typescript
  import { DsfrButtonModule } from "@edugouvfr/ngx-dsfr"

  @NgModule({
    imports: [
      CommonModule,
      DsfrButtonModule,
      ...
    ]
  })
  export class MonModule {}
  ```

- Puis utiliser le composant dans votre template html :

  ```html
  <dsfr-button>...</dsfr-button>
  ```

- Idem pour vos composants `standalone`. Les nouveaux composants créés dans la librairie sont également créés en standalone.

  ```typescript
  @Component({
    selector: 'app-component',
    imports: [DsfrModalModule, DsfrButtonModule],
  })
  export class MonComposant implements OnInit {}
  ```

## Documentation

Se référer au Storybook de la version que vous utilisez : https://foad.phm.education.gouv.fr/edugouvfr/ngx-dsfr/

Icônes de la documentation :

- 👆 Remarque
- 📌 Note
- 🔥 Point d'attention
- 👓 Accessibilité

## Code source

Le code source de cette bibliothèque de composants est publié sur mim-libre :

https://gitlab.mim-libre.fr/men/transverse/dsmen/ngx-dsfr-components.git

## Fonctionnement

### Configuration globale

Il est possible de définir certains paramètres de configuration de manière globale en utilisant la méthode statique
`forRoot` du module `DsfrConfigModule`.

Par exemple, pour indiquer de manière globale le chemin sur lequel est exposé le répertoire des pictogrammes
illustratifs DSFR, procédez comme suit :

```typescript
@NgModule({
imports: [
    DsfrConfigModule.forRoot({
      artworkDirPath: 'custom/path/to/artwork',
    }),
]
})
```

### Internationalisation

Les libellés des composants sont nativement disponibles en français (par défaut) et en anglais.
Le changement de langue des composants est géré à travers l'usage du composant `dsfr-translate` et le pipe `dsfrI18n`.

Il est possible d'étendre les traductions ou d'ajouter des nouvelles langues,
[Cf. documentation du composant Translate](/docs/components-translate--docs)

### Gestion des icônes

Une propriété `icon` est une classe du DSFR, exemple `fr-icon-home-4-fill`.
Les icônes du DSFR sont incluses dans le projet et donc disponibles automatiquement. La liste des icônes du DSFR est disponible sur leur documentation [DSFR Icônes](https://www.systeme-de-design.gouv.fr/fondamentaux/icone/)

En cas de nécessité d'ajouter d'autres icônes, il est conseillé de se limiter aux icônes de la librairie [Remix Icons](https://remixicon.com/)

Il n'existe pas pour l'instant de manière d'ajouter toute la librairie d'icônes en une fois, pour conserver le style `DSFR` appliqué aux icpones il est nécessaire de les ajouter une à une dans votre projet de la façon suivante :

- ajouter le fichier `.svg` correspondant à l'icône (fonction `Copy SVG` sur RemixIcon)
- création de la classe `CSS` correspondante, en commençant le nom de classe par `fr-icon-*`.
  ex:

```CSS
.fr-icon-play::before,
.fr-icon-play::after {
  -webkit-mask-image: url('../shared/icons/carousel/play.svg');
  mask-image: url('../shared/icons/carousel/play.svg');
}
```

### Gestion des liens

Certains composants permettent de naviguer vers d'autres pages web ou d'autres parties de votre site/application web.
Pour obtenir un hyperlien "classique" (navigation externe) avec une URL (absolue ou relative) utilisez la propriété `link` du composant.

Si vous souhaitez plutôt exécuter une navigation Angular (via le Router) voici comment procéder :

#### Navigation gérée via la directive `routerLink` (si supporté par le composant)

- Renseigner la propriété `routePath` au niveau du composant (ou `routerLink` au niveau du modèle)
- Cela va intégrer une directive `routerLink` au niveau du lien de navigation interne au composant
- `routerLinkExtras` vous permet d'ajouter les paramètres optionnels tels les queryParams
- Ou de laisser la gestion de façon externe au composant en utilisant l'attribut `route`.

#### Navigation gérée programmatiquement via `router#navigate`

- Renseigner la propriété `route` en input du composant
- Mettez-vous à l'écoute de l'output `routeSelect`
- L'événement `(routeSelect)` est émis avec la valeur de `route` et l'événement natif n'est pas propagé
- Vous pouvez alors exécuter `router#navigate`

[Cf. documentation du composant Lien](/docs/link--docs)

### Propriétés et slots

Dans de nombreux composants, il vous est possible de fournir certains contenus de deux façons :

- soit via une propriété (input) :

Ex.: `<dsfr-link link="#bottom" label="Bas de page"></dsfr-link>`

- soit via un point d'injection (ng-content) prévu à cet effet :

Ex.: `<dsfr-link link="#bottom">Bas de page</dsfr-link>`

🔥 Si les deux (slot + input) sont renseignés c'est l'input qui sera utilisé en priorité.

🔥 L'input ne supporte pas le format HTML, si vous avez besoin de formater votre contenu en utilisant des balises HTML
vous devez obligatoirement passer par le slot

Exemples de composants supportant cette possibilité :

- forms : checkbox, input, select, toggle, radio(legend)
- accordion (content, heading)
- table (caption)
- tile (description)

## Licence et droit d'utilisation

Le contenu de ce projet est placé sous licence [EUPL 1.2](https://eupl.eu/1.2/fr/).

Pour rappel, l'usage du DSFR est réservé aux sites Internet de l'État, se référer aux cf. [CGU du DSFR](https://github.com/GouvernementFR/dsfr/blob/main/doc/legal/cgu.md).

## Demander une évolution ou signaler une anomalie

#### Équipes internes Éduc. Nat.

- Passer par les [issues du Gitlab Forge](https://gitlab.forge.education.gouv.fr/dsmen/components/ngx-dsfr-components/-/issues)

#### Autres ministères ou délégations de service public

- Passer par les [issues Gitlab Mim-Libre](https://gitlab.mim-libre.fr/publication-codes-men/transverse/ngx-dsfr-components/-/issues)

## Contact

- Liste de diffusion de l'équipe de développement [dsmen-core@ldiff.forge.education.gouv.fr](mailto:dsmen-core@ldiff.forge.education.gouv.fr)
- [Canal Mattermost](https://mattermost.forge.education.gouv.fr/tyforge/channels/ngx-dsfr-components) (interne forge Éduc. Nat.)

## Feuille de route

- T1 2025 : possibilité d'un menu déroulant dans le header (utilisateur connecté)
- T2 2025 : ajout de fonctionnalités sur les tableaux complexes
- 2025 : implémentation de la V2 du DSFR

## Contribuer

Voir le [guide de contribution](CONTRIBUTING.md).
