import { StorybookConfig } from '@storybook/angular';

const config: StorybookConfig = {
  stories: [
    '../projects/ngx-dsfr-components/**/*.mdx',
    '../projects/ngx-dsfr-components/**/*.stories.@(js|jsx|mjs|ts|tsx)',
  ],
  addons: [
    '@storybook/addon-essentials',
    '@storybook/addon-links',
    '@storybook/addon-actions',
    '@storybook/addon-a11y',
    //'storybook-addon-angular-router', not compatible with SB 8 for now
    '@storybook/addon-themes',
  ],
  babel: async (options: any) => ({
    ...options,
    compact: true,
  }),
  framework: {
    name: '@storybook/angular',
    options: {},
  },
  staticDirs: [
    './assets',
    '../node_modules/@gouvfr/dsfr/dist',
    { from: '../node_modules/@gouvfr/dsfr/dist/artwork/pictograms/system', to: '/artwork/pictograms/environment' },
  ],
  docs: {},
};
export default config;
